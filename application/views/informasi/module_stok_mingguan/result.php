<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data Stok Mingguan</a>
                    </div>
					<span class="tools">
                      <i class="fa fa-list-ol"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      <div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									<?php echo anchor('/stok/insert_mingguan', 
									'<button type="button" class="btn btn-info 
										" data-toggle="tooltip" data-placement="top" title="Tambah Data Stok ">
											<i class="fa fa-plus"></i> Tambah</button>'); ?>
									<!--<?php echo anchor('/stok/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Refresh Data Stok ">
											<i class="fa fa-refresh"></i> Refresh</button>'); ?>-->
								</div>
							</div>
							
						</div>
					</div><br><br><br>
					
					
					 <div class="col-sm-12">
						<div class=row>
							
							<?php echo form_open("stok/result_mingguan");?>
							<div class="col-lg-1 col-md-2 col-sm-6 col-sx-12"></div>
							<div class="col-md-3 col-sm-4 col-xs-4">
								<div class="form-group">
									  <select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" required>
										<?php
										if($group_id=="group1000"){?>
											<option value="">
											- Kabupaten -
											</option>
											<?php
											foreach ($kabupaten as $cb) {
											echo "<option value='$cb->id_kabupaten'>
											$cb->nama_kabupaten</option>";
											}
										} else {
											foreach ($kabupaten as $cb) {
											echo "<option value='$cb->id_kabupaten' selected>
											$cb->nama_kabupaten</option>";
											}
										}
										
										?>
                              </select>
								  </div>
							</div>
								<div class="col-md-2 col-sm-4 col-xs-4">
								<div class="form-group">
									  <select class="form-control selectpicker" data-live-search="true" name="bulan" required>
											<option value="">- Bulan -</option>
											<option value="Januari">Januari</option>
											<option value="Februari">Februari</option>
											<option value="Maret">Maret</option>
											<option value="April">April</option>
											<option value="Mei">Mei</option>
											<option value="Juni">Juni</option>
											<option value="Juli">Juli</option>
											<option value="Agustus">Agustus</option>
											<option value="September">September</option>
											<option value="Oktober">Oktober</option>
											<option value="November">November</option>
											<option value="Desember">Desember</option>
									   </select>
								  </div>
							</div>
							<div class="col-md-2 col-sm-4 col-xs-4">
								<div class="form-group">
									  <select class="form-control selectpicker" data-live-search="true" name="minggu" required>
											<option value="">- Minggu -</option>
											<option value="Minggu 1">Minggu 1</option>
											<option value="Minggu 2">Minggu 2</option>
											<option value="Minggu 3">Minggu 3</option>
											<option value="Minggu 4">Minggu 4</option>
									   </select>
								  </div>
							</div>
							<div class="col-md-2 col-sm-4 col-xs-4">
								<div class="form-group">
								  <select class="form-control selectpicker" data-live-search="true" name="tahun" required>
										<option value="">- Tahun -</option>
										<?php
										$thn = date("Y")+ 2;
											for($i=2016;$i<=$thn;$i++){
											  echo "<option value='".$i."'>".$i."</option>";
											}
										?>
								   </select>
								</div>
							</div>
							<div class="btn-group">
								<button type="submit" class="btn btn-info" title="Cari Data User">
									<i class="fa fa-refresh"></i> Proses
								</button>
							</div>
							<?php echo form_close();?>
							
						</div>

					</div>
					
					
					
                    <div class="clearfix">
                    </div> 
                      
                    <div class="container">
                        <br>
						<h4><center>Kabupaten <?php echo $tempat2->nama_kabupaten?> Bulan <?php echo $bulan;?> <?php echo $minggu;?> Tahun <?php echo $tahun;?></center></h4>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left">
                        
                        <thead>
                          <tr>
                            <th style="width:2%">
                              No
                            </th>
							<th style="width:25%">
                              Nama Komoditi
                            </th>
							<th style="width:15%">
                              Satuan
                            </th>
                            <th style="width:15%">
                              Stok/Pasokan
                            </th>
							<th style="width:15%">
                              Ketahanan Stok (Minggu/Bulan)
                            </th>
                            <th style="width:1%">
                              Pilihan
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no = $number + 1;
                            foreach ($stok as $v) {
                        ?>    
                          <tr class="gradeA success">
                            <td>
                              <?php echo $no ?>
                            </td> 
							<td>
                              <?php echo $v->nama_komoditi ?>
                            </td>							
                            <td>
                              <?php echo $v->satuan ?>
                            </td>
							<td>
                              <?php echo $v->stok ?>
                            </td>
							<td>
                              <?php echo $v->ketahanan ?>
                            </td>
                            <td class="hidden-xs">
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Pilihan 
                                  <span class="caret"></span></button>
                              <ul class="dropdown-menu pull-right" role="menu">
                                <li><?php echo anchor("/stok/detail_mingguan?id=".$v->id_stok, "<i class='fa fa-check-square-o'></i> Detail"); ?></li>
                                <li><?php echo anchor("/stok/update_mingguan?id=".$v->id_stok, "<i class='fa fa-pencil-square-o'></i> Edit"); ?></li>
                                <li class="divider"></li>
                                <li><?php echo anchor("/stok/delete_mingguan?id=".$v->id_stok."&&id_kabupaten=".$id_kabupaten."&&bulan=".$bulan."&&minggu=".$minggu."&&tahun=".$tahun,"
                                    <i class='fa fa-trash-o'></i> Hapus
                                ", "onclick=\"return confirm('Anda Yakin Akan Menghapus?')\"");
                                ?></li>
                              </ul>
                            </div>
                          </td>
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
					  <div align="right"><?php echo $links?> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   