<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail Stok</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      	<?php echo form_open("stok/result_mingguan", "class='form-horizontal' row-border");?>
							
							
                      <div class="form-group">
                        <label class="col-md-2 control-label">Komoditi</label>
                           <div class="col-md-4">
                          <input class="form-control" type="text" name="satuan" value="<?php echo $entry->nama_komoditi;?>" maxlength="100" readonly>
                        </div>
                      </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Satuan</label>
                        <div class="col-md-2">
                          <input class="form-control" type="text" name="satuan" value="<?php echo $entry->satuan;?>" maxlength="100" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Stok</label>
                        <div class="col-md-2">
                          <input class="form-control" type="text" name="satuan" value="<?php echo $entry->stok;?>" maxlength="100" readonly>
                        </div>
                      </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Ketahanan Stok (Minggu/Bulan)</label>
                        <div class="col-md-2">
                          <input class="form-control" type="text" name="satuan" value="<?php echo $entry->ketahanan;?>" maxlength="100" readonly>
                        </div>
                      </div>
					  
                   
					   
					 
                      <div class="form-group">
							<input class="form-control" type="hidden" name="id_kabupaten" value="<?php echo $entry->id_kabupaten;?>" maxlength="100" readonly>
							<input class="form-control" type="hidden" name="bulan" value="<?php echo $entry->bulan;?>" maxlength="100" readonly>
							<input class="form-control" type="hidden" name="minggu" value="<?php echo $entry->minggu;?>" maxlength="100" readonly>
							<input class="form-control" type="hidden" name="tahun" value="<?php echo $entry->tahun;?>" maxlength="100" readonly>
							
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-success" title="Cari Data User">
									Kembali
								</button>
							</div>
                      
					
						
                      </div>
					   <?php form_close();?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   