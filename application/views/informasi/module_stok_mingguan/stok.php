<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data Stok Mingguan</a>
                    </div>
					<span class="tools">
                      <i class="fa fa-list-ol"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      <div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									<?php echo anchor('/stok/insert_mingguan', 
									'<button type="button" class="btn btn-info 
										" data-toggle="tooltip" data-placement="top" title="Tambah Data Stok Bahan Pokok">
											<i class="fa fa-plus"></i> Tambah</button>'); ?>
									<!--<?php echo anchor('/stok/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Refresh Data Stok Bahan Pokok">
											<i class="fa fa-refresh"></i> Refresh</button>'); ?>-->
								</div>
							</div>
							
						</div>

					</div><br><br><br>
					
					<?php
									$message = $this->session->flashdata('notif');
									$message2 = $this->session->flashdata('notif2');
									
									if($message){
										echo '<p class="alert alert-success text-center">'.$message .'</p>';
									}else if($message2){
										echo '<p class="alert alert-danger text-center">'.$message2 .'</p>';
									}
							?>
					
					
					 <div class="col-sm-12">
						<div class=row>
							
							<?php echo form_open("stok/result_mingguan");?>
							<div class="col-lg-1 col-md-2 col-sm-6 col-sx-12"></div>
							<div class="col-md-3 col-sm-4 col-xs-4">
								<div class="form-group">
									  <select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" required>
										<?php
										if($group_id=="group1000"){?>
											<option value="">
											- Kabupaten -
											</option>
											<?php
											foreach ($kabupaten as $cb) {
											echo "<option value='$cb->id_kabupaten'>
											$cb->nama_kabupaten</option>";
											}
										} else {
											foreach ($kabupaten as $cb) {
											echo "<option value='$cb->id_kabupaten' selected>
											$cb->nama_kabupaten</option>";
											}
										}
										
										?>
                              </select>
								  </div>
							</div>
							<div class="col-md-2 col-sm-4 col-xs-4">
								<div class="form-group">
									  <select class="form-control selectpicker" data-live-search="true" name="bulan" required>
											<option value="">- Bulan -</option>
											<option value="Januari">Januari</option>
											<option value="Februari">Februari</option>
											<option value="Maret">Maret</option>
											<option value="April">April</option>
											<option value="Mei">Mei</option>
											<option value="Juni">Juni</option>
											<option value="Juli">Juli</option>
											<option value="Agustus">Agustus</option>
											<option value="September">September</option>
											<option value="Oktober">Oktober</option>
											<option value="November">November</option>
											<option value="Desember">Desember</option>
									   </select>
								  </div>
							</div>
							<div class="col-md-2 col-sm-4 col-xs-4">
								<div class="form-group">
									  <select class="form-control selectpicker" data-live-search="true" name="minggu" required>
											<option value="">- Minggu -</option>
											<option value="Minggu 1">Minggu 1</option>
											<option value="Minggu 2">Minggu 2</option>
											<option value="Minggu 3">Minggu 3</option>
											<option value="Minggu 4">Minggu 4</option>
									   </select>
								  </div>
							</div>
							<div class="col-md-2 col-sm-4 col-xs-4">
								<div class="form-group">
								  <select class="form-control selectpicker" data-live-search="true" name="tahun" required>
										<option value="">- Tahun -</option>
										<?php
										$tahun = date("Y")+ 2;
										echo $tahun;
											for($i=2016;$i<=$tahun;$i++){
											  echo "<option value='".$i."'>".$i."</option>";
											}
										?>
								   </select>
								</div>
							</div>
							<div class="btn-group">
								<button type="submit" class="btn btn-info" title="Cari Data User">
									<i class="fa fa-refresh"></i> Proses
								</button>
							</div>
							<?php echo form_close();?>
							
						</div>

					</div>
					
					
					
                    <div class="clearfix">
                    </div> 
                      
                    <div class="container">
                        <br>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   