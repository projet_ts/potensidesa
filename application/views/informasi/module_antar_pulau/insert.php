<script>
        $(document).ready(function(){
			var id_kabupaten = $("#id_kabupaten").val();
			var id_pelabuhan = $("#id_pelabuhan").val();
            $.ajax({
				type : "POST",
				url  : "<?php echo base_url(); ?>antar_pulau/get",
				data : {id_kabupaten: id_kabupaten, id_pelabuhan: id_pelabuhan}, 
				success: function (data){
					$("#id_pelabuhan").html(data)
				}
            })
            $("#id_kabupaten").change(function (){
                var id_kabupaten = $("#id_kabupaten").val();
                $.ajax({
                 type : "POST",
                 url  : "<?php echo base_url(); ?>antar_pulau/get",
                 data : "id_kabupaten=" + id_kabupaten,
                 success: function (data){
                       $("#id_pelabuhan").html(data)
                 }
                })
            })	
        });
</script>
<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Tambah Perdagangan Antar Pulau</a>
                    </div>
					<span class="tools">
                      <i class="fa fa-list-ol"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                     
						<?php echo form_open("antar_pulau/create_data", 
                            "class='form-horizontal' row-border")?> 
					<div class="clearfix">
                    </div> 
					<div class="form-group">
					</div>
					
					  <div class="clearfix">
					  </div>
					<div class="form-group">
                        <label class="col-md-1 control-label">Kabupaten</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" id="id_kabupaten" required>
								<?php
								if($group_id=="group1000"){?>
									<option value="">
									- Kabupaten -
									</option>
									<?php
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten'>
									$cb->nama_kabupaten</option>";
									}
								} else {
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten' selected>
									$cb->nama_kabupaten</option>";
									}
								}
								
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
					  <div class="form-group">
					  <label class="col-md-1 control-label">Pelabuhan</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control" name="id_pelabuhan" id="id_pelabuhan" required>
									<option value="" hidden>- Pelabuhan -</option>
									<?php
										foreach ($pelabuhan as $l) {
												echo "<option value='$l->id_pelabuhan'>$l->nama_pelabuhan</option>";
										}
									?>
								  </select>
                                </div>
                            </div>  
                        </div>
						 
					  </div>
					  <div class="form-group">
                        <label class="col-md-1 control-label">Tanggal</label>
                        <div class="col-sm-10">
						<div class="row">
						<div class="col-md-2 col-sm-4 col-xs-4">
                          <div class='input-group date' id='datetimepicker1'>
										<input class="form-control" type="text"  name="tanggal" placeholder="Tanggal" id="datetimepicker1" maxlength="100" required>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
                        </div>
                        </div>
                        </div>
                      </div>
                    <div class="clearfix">
					</div>
                    <div class="container">
                        <br>
						
						
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left">
                      
                        <thead>
                          <tr>
                            <th style="width:2%">
                              No
                            </th>
							<th style="width:25%">
                              Nama Komoditi Tetap
                            </th>
							<th style="width:15%">
                              Nama Satuan
                            </th>
							<th style="width:15%">
                              Volume
                            </th>
							<th style="width:15%">
                              Harga Per Satuan
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no = $number + 1;
                            foreach ($komoditi_tetap as $v) {
                        ?>    
						<input type="hidden" name="id_komoditi_tetap[]" value="<?php echo $v->id_komoditi_tetap?>">
                          <tr class="gradeA success">
                            <td>
							<div class="form-group">
							<div class="col-md-6">
                              <?php echo $no ?>
							 </div>
							 </div>
                            </td> 
							
							<td>
                             <div class="form-group">
								<div class="col-md-6">
								  <input class="form-control" type="text" name="id_komoditi2" 
									value="<?php echo $v->nama_komoditi_tetap?>" maxlength="100" readonly>
								</div>
							</div>
                            </td>
							
                            <td>
                              <?php echo $v->satuan ?>
                            </td>
							
							<td>
								<div class="form-group">
								<div class="col-md-6">
								  <input class="form-control" type="number" min=0 name="volume[]" 
								 placeholder="Volume" maxlength="100" required>
								</div>
							  </div>
                            </td>
							
							<td>
								<div class="form-group">
								<div class="col-md-6">
								  <input class="form-control" type="number" min=0 name="harga[]" 
								 placeholder="Harga" maxlength="100" required>
								</div>
							  </div>
                            </td>
                            
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" >Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/antar_pulau/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Bahan Strategis">
											Kembali</button>'); ?>
                        </div>
                      </div>
					  
                    <?php form_close()?>
					  <?php echo $links?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   