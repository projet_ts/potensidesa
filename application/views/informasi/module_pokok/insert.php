<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Tambah Bahan pokok</a>
                    </div>
					<span class="tools">
                      <i class="fa fa-list-ol"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                     
						<?php echo form_open("pokok/create_data", 
                            "class='form-horizontal' row-border")?> 
					<div class="clearfix">
                    </div> 
					<div class="form-group">
					</div>
					<div class="form-group">
                        <label class="col-md-1 control-label">Tanggal</label>
                        <div class="col-sm-6">
						<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<div class='input-group date' id='datetimepicker1'>
										<input class="form-control" type="text"  name="tanggal" placeholder="Tanggal" id="datetimepicker1" maxlength="100" required>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
							</div>
                        </div>
                        </div>
                        </div>
                      </div>
					  <div class="clearfix">
					  </div>
					<div class="form-group">
					</div>
					<div class="form-group">
                        <label class="col-md-1 control-label">Pasar</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control selectpicker" data-live-search="true" name="id_pasar" required>
								<option value="">
									- Pasar -
								</option>
                                <?php
								foreach ($pasar as $cb) {
                                    echo "<option value='$cb->id_pasar'>
                                  $cb->nama_pasar</option>";
                                }
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
                    <div class="clearfix">
					</div>
                    <div class="container">
                        <br>
						
						
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left">
                      
                        <thead>
                          <tr>
                            <th style="width:2%">
                              No
                            </th>
							<th style="width:25%">
                              Nama Komoditi
                            </th>
							<th style="width:15%">
                              Nama Satuan
                            </th>
							<th style="width:15%">
                              Harga
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no = $number + 1;
                            foreach ($pokok as $v) {
                        ?>    
						<input type="hidden" name="id_komoditi[]" value="<?php echo $v->id_komoditi?>">
                          <tr class="gradeA success">
                            <td>
							<div class="form-group">
							<div class="col-md-6">
                              <?php echo $no ?>
							 </div>
							 </div>
                            </td> 
							<td>
                             <div class="form-group">
								<div class="col-md-6">
								  <input class="form-control" type="text" name="id_komoditi2" 
									value="<?php echo $v->nama_komoditi?>" maxlength="100" readonly>
								</div>
							</div>
                            </td>							
                            <td>
                              <?php echo $v->satuan ?>
                            </td>
							<td>
								<div class="form-group">
								<div class="col-md-6">
								 <!-- <input class="form-control" type="number" min="0" name="harga[]" placeholder="Harga" maxlength="100" required> -->
								 <input class="form-control" type="text" name="harga[]" placeholder="Harga" style="text-align: right;" onkeyup="formatRupiah(this, '.')" required/>
								</div>
							  </div>
                            </td>
                            
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" >Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/pokok/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Bahan pokok">
											Kembali</button>'); ?>
                        </div>
                      </div>
					  
                    <?php form_close()?>
					  <?php echo $links?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   