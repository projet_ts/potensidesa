<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Tambah Stok</a>
                    </div>
					<span class="tools">
                      <i class="fa fa-list-ol"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                     
						<?php echo form_open("stok/create_data", 
                            "class='form-horizontal' row-border")?> 
					<div class="clearfix">
                    </div> 
					<div class="form-group">
                        <label class="col-md-1 control-label">Kabupaten</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" required>
								<?php
								if($group_id=="group1000"){?>
									<option value="">
									- Kabupaten -
									</option>
									<?php
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten'>
									$cb->nama_kabupaten</option>";
									}
								} else {
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten' selected>
									$cb->nama_kabupaten</option>";
									}
								}
								
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
					  <div class="clearfix">
					  </div>
					<div class="form-group">
                        <label class="col-md-1 control-label">Tahun</label>
                        <div class="col-sm-10">
						<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">
                          <select class="form-control selectpicker" data-live-search="true" name="tahun" required>
								<option value="">- Tahun -</option>
										<?php
										$thn = date("Y")+ 2;
											for($i=2016;$i<=$thn;$i++){
											  echo "<option value='".$i."'>".$i."</option>";
											}
										?>
                           </select>
                        </div>
                        </div>
                        </div>
                      </div>
					
                    <div class="clearfix">
					</div>
                    <div class="container">
                        <br>
						
						
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left">
                      
                        <thead>
                          <tr>
                            <th style="width:2%">
                              No
                            </th>
							<th style="width:25%">
                              Nama Komoditi
                            </th>
							<th style="width:15%">
                              Nama Satuan
                            </th>
							<th style="width:15%">
                              Stok/Pasokan
                            </th>
							<th style="width:15%">
                              Ketahanan Stok (Minggu / Bulan)
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no = $number + 1;
                            foreach ($komoditi as $v) {
                        ?>    
						<input type="hidden" name="id_komoditi_stok[]" value="<?php echo $v->id_komoditi_stok?>">
                          <tr class="gradeA success">
                            <td>
							<div class="form-group">
							<div class="col-md-6">
                              <?php echo $no ?>
							 </div>
							 </div>
                            </td> 
							<td>
                             <div class="form-group">
								<div class="col-md-6">
								  <input class="form-control" type="text" name="id_komoditi2" 
									value="<?php echo $v->nama_komoditi?>" maxlength="100" readonly>
								</div>
							</div>
                            </td>							
                            <td>
                              <?php echo $v->satuan ?>
                            </td>
							<td>
								<div class="form-group">
								<div class="col-md-6">
								  <input class="form-control" type="number" min="0" name="stok[]" 
								 placeholder="Stok" maxlength="100" required>
								</div>
							  </div>
                            </td>
							<td>
								<div class="form-group">
								<div class="col-md-10">
								  <input class="form-control" type="text" min="0" name="ketahanan[]" 
								 placeholder="Ketahanan Stok" maxlength="100" required>
								</div>
							  </div>
                            </td>
                            
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" >Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/stok/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Bahan pokok">
											Kembali</button>'); ?>
                        </div>
                      </div>
					  
                    <?php form_close()?>
					  <?php echo $links?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   