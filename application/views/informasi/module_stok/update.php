<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Stok</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("stok/update_data", 
                            "class='form-horizontal' row-border")?> 
                      <input type="hidden" name="id_stok" value="<?php echo $entry->id_stok?>">
					  <input class="form-control" type="hidden" name="id_kabupaten" value="<?php echo $entry->id_kabupaten;?>" maxlength="100" readonly>
					  <input class="form-control" type="hidden" name="bulan" value="<?php echo $entry->bulan;?>" maxlength="100" readonly>
					  <input class="form-control" type="hidden" name="tahun" value="<?php echo $entry->tahun;?>" maxlength="100" readonly>
						<div class="form-group">
                        <label class="col-md-2 control-label">Komoditi</label>
                           <div class="col-md-4">
                          <input class="form-control" type="text" name="nama_komoditi" value="<?php echo $entry->nama_komoditi;?>" maxlength="100" readonly>
                        </div>
                      </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Satuan</label>
                        <div class="col-md-2">
                          <input class="form-control" type="text" name="satuan" value="<?php echo $entry->satuan;?>" maxlength="100" readonly>
                        </div>
						</div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Stok</label>
                        <div class="col-md-2">
                          <input class="form-control" type="text" name="stok" value="<?php echo $entry->stok;?>" maxlength="100" >
                        </div>
                      </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Ketahanan Stok (Minggu/Bulan)</label>
                        <div class="col-md-2">
                          <input class="form-control" type="text" name="ketahanan" value="<?php echo $entry->ketahanan;?>" maxlength="100" >
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" title="Update Data">Update</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  
					 	</div>
                      </div>  
                    <?php echo form_close();?>
					 <?php echo form_open("stok/result", 
                            "class='form-horizontal' row-border")?> 
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <input class="form-control" type="hidden" name="id_kabupaten" value="<?php echo $entry->id_kabupaten;?>" maxlength="100" readonly>
						  <input class="form-control" type="hidden" name="bulan" value="<?php echo $entry->bulan;?>" maxlength="100" readonly>
						  <input class="form-control" type="hidden" name="tahun" value="<?php echo $entry->tahun;?>" maxlength="100" readonly>
						  <button type="submit" class="btn btn-success" title="Kembali Data Stok Bahan pokok">Kembali</button>
					 	</div>
                      </div>  
						  
					 <?php echo form_close();?>
                        
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   