<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah Agenda</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open_multipart("agenda/create_data", 
                            "class='form-horizontal' row-border")?> 
					       <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis agenda</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="id_jenisagenda">
								<?php
                                foreach ($jenisagenda as $cb) {
                                    echo "<option value='$cb->id_jenisagenda'>
                                  $cb->nama_jenisagenda</option>"; 
                                }
                                ?>
								</select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  
                       <div class="form-group">
                        <label class="col-md-2 control-label">Judul</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="judul" 
							placeholder="judul" maxlength="100">
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal :</label>
                        <div class="col-md-2">
							<div class='input-group date' id='datetimepicker1'>
								<input class="form-control" type="text"  name="tanggal" placeholder="Tanggal" id="datetimepicker1" maxlength="100" required>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
					  </div>	
					   <div class="form-group">
                        <label class="col-md-2 control-label">Tempat</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="tempat" 
							placeholder="tempat" maxlength="100">
                        </div>
					   </div>
					  
					 <div class="form-group">
                        <label class="col-md-2 control-label">Kegiatan</label>
                        <div class="col-sm-10">
                            <textarea name="kegiatan" id="content" ></textarea><?php echo display_ckeditor($editor['ckeditor1']);?>
                        </div>
                      </div>					  
					<div class="form-group">
					  <label class="col-md-2 control-label" >Upload File</label>
					  <div class="col-md-6">
						  <input class="form-control" type="file" name="userfile">
					  </div>
					</div>
					  
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" >Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/agenda/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali ke Data agenda">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   