<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data Desa</a>
                    </div>
                  </div>
                  <div class="widget-body">
                      <div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									<?php echo anchor('/desa/insert',
									'<button type="button" class="btn btn-info
										" data-toggle="tooltip" data-placement="top" title="Tambah Data Desa">
											Tambah</button>'); ?>
									<?php echo anchor('/desa/data',
									'<button type="button" class="btn btn-success
										" data-toggle="tooltip" data-placement="top" title="Refresh Data Desa">
											Refresh</button>'); ?>
								</div>
							</div>

							<?php echo form_open("desa/result");?>

							<div class="col-md-3 col-sm-4 col-xs-4">
								<input type="text" class="form-control" name="key" placeholder="Cari">
							</div>

							<div class="btn-group">
								<button type="submit" class="btn btn-info" title="Cari Data Menu">
									<i class="fa fa-search"></i>
								</button>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
                    <div class="clearfix">
                    </div>

                    <div id="dt_example" class="example_alt_pagination">
                        <br>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left">

                        <thead>
                          <tr>
                            <th style="width:5%">
                              No
                            </th>
                            <th style="width:20%">
                              Nama Desa
                            </th>
                            <th style="width:20%">
                              Jenis
                            </th>
                            <th style="width:20%">
                              Nama Kecamatan
                            </th>
							              <th style="width:15%">
                              Luas
                            </th>
                            <th style="width:20%">
                              Pilihan
                            </th>

                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no = $number + 1;
                            foreach ($desa as $v) {


                        ?>
                          <tr class="gradeA success">
                            <td>
                              <?php echo $no ?>
                            </td>
                            <td>
                              <?php echo $v->nama_desa ?>
                            </td>
                            <td>
                              <?php echo $v->jenis ?>
                            </td>
                            <td>
                              <?php echo $v->nama_kecamatan ?>
                            </td>
							<td>
                              <?php echo $v->luas ?>
                            </td>
                            <td class="hidden-xs">
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Pilihan
                                  <span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu">
								<li><?php echo anchor("/desageografi/data?id=".$v->desa_id, "Data Geografi"); ?></li>
                <li><?php echo anchor("/pertanian/data?id=".$v->desa_id, "Data Pertanian"); ?></li>
                <li><?php echo anchor("/kehutanan/data?id=".$v->desa_id, "Data Kehutanan"); ?></li>
                <li><?php echo anchor("/peternakan/data?id=".$v->desa_id, "Data Peternakan"); ?></li>
                <li><?php echo anchor("/perkebunan/data?id=".$v->desa_id, "Data Perkebunan"); ?></li>
                <li><?php echo anchor("/pertambangan/data?id=".$v->desa_id, "Data Pertambangan"); ?></li>
                <li><?php echo anchor("/perikanan/data?id=".$v->desa_id, "Data Perikanan"); ?></li>
                <li><?php echo anchor("/sarana/data?id=".$v->desa_id, "Data Sarana"); ?></li>
                <li><?php echo anchor("/wisata/data?id=".$v->desa_id, "Data Wisata"); ?></li>
                <li class="divider"></li>
                <li><?php echo anchor("/penduduk/data?id=".$v->desa_id, "Data Penduduk"); ?></li>
                <li><?php echo anchor("/pendidikan/data?id=".$v->desa_id, "Data Pendidikan"); ?></li>
                <li><?php echo anchor("/pekerjaan/data?id=".$v->desa_id, "Data Pekerjaan"); ?></li>
                <li><?php echo anchor("/agama/data?id=".$v->desa_id, "Data Agama"); ?></li>
                <li><?php echo anchor("/kelembagaan/data?id=".$v->desa_id, "Data Kelembagaan"); ?></li>
                <li class="divider"></li>
                                <li><?php echo anchor("/desa/detail?id=".$v->desa_id, "Detail"); ?></li>
                                <li><?php echo anchor("/desa/update?id=".$v->desa_id, "Edit"); ?></li>
                                <li class="divider"></li>
                                <li><?php echo anchor("/desa/delete?id=".$v->desa_id, "
                                    Hapus
                                ", "onclick=\"return confirm('Anda Yakin Akan Menghapus?')\"");
                                ?></li>
                              </ul>
                            </div>
                          </td>
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
					  <div align="right"><?php echo $links?></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
