<script type="text/javascript" src="<?php echo base_url('/asset/highcharts/exporting.js'); ?>"></script>
<script type="text/javascript">
jQuery.noConflict();

var example = 'line-ajax', 
theme = 'default';

(function($){ // encapsulate jQuery
	$(function () {

    // Get the CSV and create the chart
   

        $('#container').highcharts({
			
		/*	chart: {
            type: 'areaspline'
        },*/
		chart: {
            type: 'area',
            spacingBottom: 30
        },
            title: {
                text: 'Harga <?php echo $nama_komoditi->nama_komoditi?> di <?php echo $nama_pasar->nama_pasar?> Bulan <?php echo $nama_bulan?> Tahun <?php echo $tahun->tahun?> ',
            },

            xAxis: {
                categories: [' 1 ',
							 ' 2 ',
							 ' 3 ',
							 ' 4 ',
							 ' 5 ',
							 ' 6 ',
							 ' 7 ',
							 ' 8 ',
							 ' 9 ',
							 ' 10 ',
							 ' 11 ',
							 ' 12 ',
							 ' 13 ',
							 ' 14 ',
							 ' 15 ',
							 ' 16 ',
							 ' 17 ',
							 ' 18 ',
							 ' 19 ',
							 ' 20 ',
							 ' 21 ',
							 ' 22 ',
							 ' 23 ',
							 ' 24 ',
							 ' 25 ',
							 ' 26 ',
							 ' 27 ',
							 ' 28 ',
							 ' 29 ',
							 ' 30 ',
							 ' 31 ']
            },

            /*yAxis: [{ // left y axis
                title: {
                    text: "Harga (Rp.)"
                },
                labels: {
                    align: 'left',
                    x: 3,
                    y: 16,
                    format: '{value:.,0f}'
                },
                showFirstLabel: false
            }, { // right y axis
                linkedTo: 0,
                gridLineWidth: 0,
                opposite: true,
                title: {
                    text: null
                },
                labels: {
                    align: 'right',
                    x: -3,
                    y: 16,
                    format: '{value:.,0f}'
                },
                showFirstLabel: false
            }],*/
			
			yAxis: {
            min: 0,
            title: {
                text: 'Rupiah (Rp)'
            }
			},

            /*legend: {
                align: 'left',
                verticalAlign: 'top',
                y: 20,
                floating: true,
                borderWidth: 0
            },

            tooltip: {
                shared: true,
                crosshairs: true
            },

            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                hs.htmlExpand(null, {
                                    pageOrigin: {
                                        x: e.pageX || e.clientX,
                                        y: e.pageY || e.clientY
                                    },
                                    headingText: this.series.name,
                                    maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) + ':<br/> ' +
                                        this.y + ' visits',
                                    width: 200
                                });
                            }
                        }
                    },
                    marker: {
                        lineWidth: 1
                    }
                }
            },*/
			
			legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
			},

           series: [{
            name: '<?php echo $nama_komoditi->nama_komoditi?>',
            data: <?php echo json_encode($grafik); ?>
        }]
        });

});
})(jQuery);
jQuery(document).ready(function(){jQuery("#view-menu").click(function(e){jQuery("#wrap").toggleClass("toggled")}),jQuery("#sidebar-close").click(function(e){jQuery("#wrap").removeClass("toggled")}),jQuery(document).keydown(function(e){var t;"INPUT"!=e.target.tagName&&(39==e.keyCode?t=document.getElementById("next-example"):37==e.keyCode&&(t=document.getElementById("previous-example")),t&&(location.href=t.href))}),jQuery("#switcher-selector").bind("change",function(){var e=jQuery(this).val();return e&&(window.location=e),!1})});
  </script>
  
 <!-- Page Content -->
	    <div class="container blog singlepost">
			<div class="row">
				<article class="col-md-12"><br>
			        <h1 align="center" class="page-header sidebar-title">Informasi Harga Komoditi Per Barang</h1>
			        
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>Informasi !</strong> silahkan mengisi data untuk melihat harga komoditi.
					</div>
				</div>

				 <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-body">
                      
                      <?php echo form_open("Menu_harga_komoditi/harga_komoditi_barang_result", 
                            "class='form-horizontal' row-border")?> 
                      
					  <div class="form-group">
                        <label class="col-md-2 control-label">Pasar</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                 <select  class="form-control selectpicker" data-live-search="true" name="id_pasar" required>
								<option value="">
									- Pasar -
									</option>
                                <?php
									foreach ($pasar as $cb) {
                                    echo "<option value='$cb->id_pasar'>
									$cb->nama_pasar</option>";
									}
								
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Nama Komoditi</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                 <select class="form-control selectpicker" data-live-search="true" name="id_komoditi" required>
								<option value="">
									- Nama Komoditi -
									</option>
                                <?php
									foreach ($komoditi as $cb) {
                                    echo "<option value='$cb->id_komoditi'>
									$cb->nama_komoditi</option>";
									}
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Bulan</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                 <select class="form-control selectpicker" data-live-search="true" name="bulan" required>
											<option value="">- Bulan -</option>
											<option value="1">Januari</option>
											<option value="2">Februari</option>
											<option value="3">Maret</option>
											<option value="4">April</option>
											<option value="5">Mei</option>
											<option value="6">Juni</option>
											<option value="7">Juli</option>
											<option value="8">Agustus</option>
											<option value="9">September</option>
											<option value="10">Oktober</option>
											<option value="11">November</option>
											<option value="12">Desember</option>
									   </select>
                                </div>
                            </div>  
                        </div>
                      </div>
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary" >Cetak</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
			
			<div class="span10">
			<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div><br><br><br>
	</div>

			    </article>
				<!-- Blog Sidebar Column -->
			</div>
	    </div>


<section id="contact">
			<div class="container"> 
				<div class="row">
					<div class="col-md-12">
					<center>	<div class="col-lg-12">
							<div class="text-center"><h2><img src="<?php echo base_url()?>asset/template/images/kontak2.png" alt="company logo" /></h2>
							</div>
						</div>
						
						<div class="col-lg-13">
							<form >
									<div class="col-sm-6 height-contact-element">
										<div class="col-md-12 height-contact-element">
											<div class="form-group">
												<i class="fa fa-2x fa-map-marker"></i>
												<span class="text">Alamat : Jl. Drs. H. Abdullah Silondae, No. 116, Kendari, Sulawesi Tenggara</span>
											</div>
										</div>
									</div>
									<div class="col-sm-6 height-contact-element">
										<div class="col-md-12 height-contact-element">
											<div class="form-group">
												<i class="fa fa-2x fa-phone"></i>
												<span class="text">Telp/Fax : +62 401 3128339</span>
											 </div>
										</div>
									</div>
									
							</form>
						</div></center>
						<!--<div class="col-lg-5 col-md-3 col-lg-offset-1 col-md-offset-1">
							<div class="row">
								<div class="col-md-12 height-contact-element">
									<div class="form-group">
										<i class="fa fa-2x fa-map-marker"></i>
										<span class="text">LOCATION</span>
									</div>
								</div>
								<div class="col-md-12 height-contact-element">
									<div class="form-group">
										<i class="fa fa-2x fa-phone"></i>
										<span class="text">0051 768622115</span>
									 </div>
								 </div>
								<div class="col-md-12 height-contact-element">    
									<div class="form-group">
										<i class="fa fa-2x fa-envelope"></i>
										<span class="text">info@company.com</span>
									</div>
								</div>
							</div>
						</div>-->
					</div>
				</div>
			</div>
		</section>

			<section id="follow-us">
			<div class="container"> 
				<div class="text-center height-contact-element">
					<h2>FOLLOW US</h2>
				</div>
				<img class="img-responsive displayed" src="<?php echo base_url();?>asset/template/images/short.png" alt="short" />
				<div class="text-center height-contact-element">
					<ul class="list-unstyled list-inline list-social-icons">
						<li class="active"><a href="#"><i class="fa fa-facebook social-icons"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter social-icons"></i></a></li>
						<!--<li><a href="#"><i class="fa fa-google-plus social-icons"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin social-icons"></i></a></li>-->
					</ul>
				</div>
			</div>
		</section>
		
		