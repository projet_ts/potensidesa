 
 <!-- Page Content -->
	    <div class="container blog singlepost">
			<div class="row">
				<article class="col-md-12"><br>
			        <h1 align="center" class="page-header sidebar-title">Informasi Harga Komoditi</h1>
			        
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>Informasi !</strong> silahkan mengisi data untuk melihat harga komoditi.
					</div>
				</div>

				 <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-body">
                      
                      <?php echo form_open("Menu_harga_komoditi/harga_komoditi_pasar_result", 
                            "class='form-horizontal' row-border")?> 
                      
					  <div class="form-group">
                        <label class="col-md-2 control-label">Pasar</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                 <select  class="form-control selectpicker" data-live-search="true" name="id_pasar" required>
								<option value="">
									- Pasar -
									</option>
                                <?php
									foreach ($pasar as $cb) {
                                    echo "<option value='$cb->id_pasar'>
									$cb->nama_pasar</option>";
									}
								
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Jenis Komoditi</label>
                        <div class="col-md-2">
                          <select class="form-control" name="jenis_komoditi" required>
							<option value ="">- Jenis Komoditi -</option>
							<option value ="Bahan Pokok">Pokok</option>
							<option value ="Bahan Strategis">Strategis</option>
						  </select>
                        </div>
					  </div> 
					  <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal</label>
						 <div class="col-md-2">
							<div class='input-group date' id='datetimepicker1'>
								<input class="form-control" type="text"  name="tanggal"  id="datetimepicker1" maxlength="100" required>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
					  </div>
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary" >Cetak</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
			
			<table class="table table-condensed table-striped table-hover table-bordered pull-left" >
                        
																		<thead>
																		  <tr>
																			<th style="width:50%">
																			  <center>Nama Komoditi</center>
																			</th>
																			<th style="width:30%">
																			  <center><?php
																			   $tanggal2 = date('d F Y', strtotime($tanggal2 ));
																				echo $tanggal2;
																			  ?></center>
																			</th>
																		  </tr>
																		</thead>
																		<tbody>
																		<?php 
																		foreach ($harga_komoditi as $v) {?>    
																		  <tr class="odd" style="font-size:11px;"class="gradeA success">
																			<td  class="w96px txt-right">
																			  <?php echo $v->nama_komoditi ?>
																			</td>
																			<td align="center">
																			  <?php  
																				$harga = "Rp " . number_format($v->harga,0,',','.');
																				echo $harga
																			  ?>
																			</td>
																		  </tr>
																		  <?php
																		  }//}
																		  ?>
																		</tbody>
																	  </table>

			    </article>
				<!-- Blog Sidebar Column -->
			</div>
	    </div>


<section id="contact">
			<div class="container"> 
				<div class="row">
					<div class="col-md-12">
					<center>	<div class="col-lg-12">
							<div class="text-center"><h2><img src="<?php echo base_url()?>asset/template/images/kontak2.png" alt="company logo" /></h2>
							</div>
						</div>
						
						<div class="col-lg-13">
							<form >
									<div class="col-sm-6 height-contact-element">
										<div class="col-md-12 height-contact-element">
											<div class="form-group">
												<i class="fa fa-2x fa-map-marker"></i>
												<span class="text">Alamat : Jl. Drs. H. Abdullah Silondae, No. 116, Kendari, Sulawesi Tenggara</span>
											</div>
										</div>
									</div>
									<div class="col-sm-6 height-contact-element">
										<div class="col-md-12 height-contact-element">
											<div class="form-group">
												<i class="fa fa-2x fa-phone"></i>
												<span class="text">Telp/Fax : +62 401 3128339</span>
											 </div>
										</div>
									</div>
									
							</form>
						</div></center>
						<!--<div class="col-lg-5 col-md-3 col-lg-offset-1 col-md-offset-1">
							<div class="row">
								<div class="col-md-12 height-contact-element">
									<div class="form-group">
										<i class="fa fa-2x fa-map-marker"></i>
										<span class="text">LOCATION</span>
									</div>
								</div>
								<div class="col-md-12 height-contact-element">
									<div class="form-group">
										<i class="fa fa-2x fa-phone"></i>
										<span class="text">0051 768622115</span>
									 </div>
								 </div>
								<div class="col-md-12 height-contact-element">    
									<div class="form-group">
										<i class="fa fa-2x fa-envelope"></i>
										<span class="text">info@company.com</span>
									</div>
								</div>
							</div>
						</div>-->
					</div>
				</div>
			</div>
		</section>

			<section id="follow-us">
			<div class="container"> 
				<div class="text-center height-contact-element">
					<h2>FOLLOW US</h2>
				</div>
				<img class="img-responsive displayed" src="<?php echo base_url();?>asset/template/images/short.png" alt="short" />
				<div class="text-center height-contact-element">
					<ul class="list-unstyled list-inline list-social-icons">
						<li class="active"><a href="#"><i class="fa fa-facebook social-icons"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter social-icons"></i></a></li>
						<!--<li><a href="#"><i class="fa fa-google-plus social-icons"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin social-icons"></i></a></li>-->
					</ul>
				</div>
			</div>
		</section>
		
		