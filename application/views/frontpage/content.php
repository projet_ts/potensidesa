<style>
		#myCarousel {
			margin-left: 30px;
			margin-right: 30px;
		}
		#myCarousel2 {
			margin-left: 30px;
			margin-right: 30px;
		}
		.carousel-control {
			top: 50%;
		}
		.carousel-control.left, .carousel-control.right {
			background: none;
			color: @red;
			border: none;
		}
		.carousel-control.left {
			margin-left: -33px;
			color: blue;
		}
		.carousel-control.right {
			margin-right: -34px;
			color: blue;
		}
		.carousel-indicators li {
			background-color: #999 !important;
			background-color: rgba(70,70,70,.100) !important;
		}
		.carousel-indicators .active {
			background-color: #444 !important;
		}
</style>
 
	<script>
	$(window).load(function() {
	$('#myCarousel').carousel({
  		interval: 6000
 		})
   	});
</script>

  <!-- Header Carousel -->
		<header id="home" class="carousel slide">
			<ul class="cb-slideshow">
				<li><span>image1</span>
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="text-center"><h3>Dinas Perindag Prov. Sulawesi Tenggara</h3></div>
							</div>
						</div>
						<h4>Selamat Datang Di Website Dinas Perindag Prov. Sulawesi Tenggara</h4>
					</div>
				</li>
				<li><span>image2</span>
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="text-center"><h3>Dinas Perindag Prov. Sulawesi Tenggara</h3></div>
							</div>
						</div>
						<h4>Selamat Datang Di Website Dinas Perindag Prov. Sulawesi Tenggara</h4>
					</div>
				</li>
				<li><span>image3</span>
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="text-center"><h3>Dinas Perindag Prov. Sulawesi Tenggara</h3></div>
							</div>
						</div>
						<h4>Selamat Datang Di Website Dinas Perindag Prov. Sulawesi Tenggara</h4>
					</div>
				</li>
				<li><span>Image 04</span>
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="text-center"><h3>Dinas Perindag Prov. Sulawesi Tenggara</h3></div>
							</div>
						</div>
						<h4>Selamat Datang Di Website Dinas Perindag Prov. Sulawesi Tenggara</h4>
					</div>
				</li>
				<li><span>Image 05</span>
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="text-center"><h3>Dinas Perindag Prov. Sulawesi Tenggara</h3></div>
							</div>
						</div>
						<h4>Selamat Datang Di Website Dinas Perindag Prov. Sulawesi Tenggara</h4>
					</div>
				</li>
				<li><span>Image 06</span>
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="text-center"><h3>Dinas Perindag Prov. Sulawesi Tenggara</h3></div>
							</div>
						</div>
						<h4>Selamat Datang Di Website Dinas Perindag Prov. Sulawesi Tenggara</h4>
					</div>
				</li>
			</ul>
			<div class="intro-scroller">
				<a class="inner-link" href="#about">
					<div class="mouse-icon" style="opacity: 1;">
						<div class="wheel"></div>
					</div>
				</a> 
			</div>          
		</header>
			
		<!-- Page Content -->
		<section id="about">
					<div class="container"><br>
				<div class="text-center">
					<img class="img-responsive displayed" src="<?php echo base_url()?>asset/template/images/profil2.png" alt="about" />
					<img class="img-responsive displayed" src="<?php echo base_url()?>asset/template/images/short.png" alt="about">
				</div>
				<div class="row">
					<div class="col-md-12 homeport1">
						<div class="col-md-2 col-sm-12 col-xs-12 portfolio-item">
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 portfolio-item">
							<figure class="effect-oscar">
								<img src="<?php echo base_url()?>files/profil_image/<?php echo $profil->gambar?>" alt="img09" height="270" />
								<figcaption>
									<h2><?php echo $profil->judul?></h2>
									<a href="#">View more</a>
								</figcaption>           
							</figure>
							<p class="text-center"><?php $content = $profil->content;
													$content=character_limiter($content,300);?>
													<?php echo $content;?></p>
							<div class="text-center"><a href="<?php echo base_url();?>menu_profil/profil/<?php echo $profil->id_profil?>" class="btn btn-primary btn-noborder-radius hvr-bounce-to-bottom">Read More</a></div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 portfolio-item">
							<figure class="effect-oscar">
								<img src="<?php echo base_url()?>files/profil_image/<?php echo $visi_misi->gambar?>" alt="img09" height="270"/>
								<figcaption>
									<h2><?php echo $visi_misi->judul?></h2>
									<a href="#">View more</a>
								</figcaption>           
							</figure>
							<p class="text-center"><?php $content = $visi_misi->content;
													$content=character_limiter($content,300);?>
													<?php echo $content;?></p>
							<div class="text-center"><a href="<?php echo base_url();?>menu_profil/profil/<?php echo $visi_misi->id_profil?>" class="btn btn-primary btn-noborder-radius hvr-bounce-to-bottom">Read More</a></div>
						</div>
					</div>
				</div>
			</div>
	
		</section>

		<section id="komoditi">
			<div class="orangeback">
				<div class="container"><br><br><br><br><br>
					<div class="text-center">
					<img class="img-responsive displayed" src="<?php echo base_url()?>asset/template/images/komoditi.png" alt="about" />
					</div>
					<div class="text-center"><a href="<?php echo base_url();?>Menu_harga_komoditi/harga_komoditi_pasar" class="btn btn-default btn-noborder-radius hvr-bounce-to-bottom2">CEK HARGA KOMODITI</a>
					<a href="<?php echo base_url();?>menu_stok/stok" class="btn btn-default btn-noborder-radius hvr-bounce-to-bottom2">GRAFIK STOK BARANG</a></div><br>
					<div class="text-center"><a href="<?php echo base_url();?>Menu_harga_komoditi/harga_komoditi_barang" class="btn btn-default btn-noborder-radius hvr-bounce-to-bottom2">GRAFIK HARGA BARANG</a>
					<a href="<?php echo base_url();?>menu_antar_pulau/antar_pulau" class="btn btn-default btn-noborder-radius hvr-bounce-to-bottom2">GRAFIK ANTAR PULAU</a></div><br>
					
					
<div class="span10">
 
					</ul><!--.breadcrumb-->

					
				</div>

				<div class="page-content">
					

					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->

	<!--<div class="span10">
			<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div><br><br><br>
	</div>
        
 
					</ul><!--.breadcrumb-->

					
				</div>

			


					<!--<div class="col-lg-3">
						<div class="panel panel-success">
                        <div class="panel-heading"><center><b>KABUPATEN</b></center></div>
                        <div class="panel-body">
                           <div class="row">
						   <?php foreach ($kabupaten as $kabupaten){
						   
								echo "<ul class='filter nav'>
								  <li data-value='kendari' ><a class='active' href='#'>$kabupaten->nama_kabupaten</a></li>
								</ul>";
							}
								?>
							</div> 
						</div>
                        <div class="panel-footer">
                        </div>
						</div>
					</div>-->
					
					<div class="col-lg-6">
						<div class="panel panel-success">
                        <div class="panel-heading"><center><b>HARGA BAHAN POKOK DI SELURUH PASAR PROV. SULAWESI TENGGARA</b></center></div>
                        <div class="panel-body">
                           <div class="row">
								<ul class="port2">
								
				
				
								   <div id="myCarousel" class="carousel slide" data-ride="carousel">
										<!-- Carousel Indikator 
										<ol class="carousel-indicators">
										<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
										<li data-target="#myCarousel" data-slide-to="1"></li>
										
										</ol>-->
								
								
										<!-- Wrapper for Slide -->
										<div class="carousel-inner" role="listbox">
										
											<?php
														$i  = 1;
														foreach ($kabupaten2 as $row) {
															//$Pasar     = $row->nama_pasar;
															$Kabupaten     = $row->nama_kabupaten;
															 
															$item_class = ($i == 1) ? 'item active' : 'item';
														?>
														<div class="<?php echo $item_class; ?>">
																	<center><b><?php echo $Kabupaten?></b></center><br> 
																<?php //echo $Pasar?><br>
																
														
														
													<center>	
													<table class="table-striped table-hover pull-center"  >
                        
																		<thead>
																		  <tr>
																			<th style="width:45%">
																			  Nama Komoditi
																			</th>
																			<th style="width:15%">
																			  Satuan
																			</th>
																			<th style="width:18%">
																			  Harga Kemarin
																			</th>
																			<th style="width:5%">
																			</th>
																			<th style="width:20%">
																			  Harga Hari Ini
																			</th>
																			
																			
																		  </tr>
																		</thead>
																		<tbody>
																		<?php
																				
																		foreach ($pokok as $v) {?>    
																		  <tr class="odd" style="font-size:14px;"class="gradeA success">
																		  <?php
																				if($row->id_kabupaten==$v->id_kabupaten) {
																		  ?>
																		  
																			<td  class="w96px txt-right">
																			  <?php echo $v->nama_komoditi?>
																			</td>
																			<td  class="w96px txt-right">
																			  <?php echo $v->satuan?>
																			</td>
																			
																			<td>
																			  <?php 
																			  $harga_kemarin = $v->harga_kemarin;
																			  $angka_format = "Rp " . number_format($harga_kemarin,0,",",".");
																			  echo $angka_format
																			  ?>
																			  <?php
																			  if($v->harga_hari_ini > $v->harga_kemarin){
																					$v->harga_hari_ini - $v->harga_kemarin
																			  ?>
																					<td>
																							<img alt="harga naik" src="<?php echo base_url()?>asset/template/images/up_tr.png" style="margin-top:4px;display:inline" >
																					</td>
																			  <?php
																			  } else if($v->harga_hari_ini < $v->harga_kemarin){
																			  ?>
																					<td>
																							<img alt="harga turun" src="<?php echo base_url()?>asset/template/images/down_tr.png" style="margin-top:4px;display:inline">
																					</td>
																			  <?php
																			  } else {
																			  ?>
																					<td>
																					</td>																			  
																			  <?php
																			  }
																			  ?>
																			  
																			  </td>	
																			<td>
																			  <?php 
																			  $harga_hari_ini = $v->harga_hari_ini;
																			  $angka_format2 = "Rp " . number_format($harga_hari_ini,0,",",".");
																			  echo $angka_format2
																			  ?>
																			</td>		
																		
																		 <?php
																			}}
																		  ?>
																			<td class="hidden-xs">
																		  </td>
																		 
																		  </tr>
																		</tbody>
																	  </table>
																	  </center>
															</div>
															<?php
															$i++;
														}
														?> 		  
																	  
<!-- Control -->
										<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
											<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
									  </div>
									  </ul>
							</div> 
						</div>
                        <div class="panel-footer">

                        </div>
						</div>
					</div>
							
							</div>
							
							
						
					<div class="col-lg-6">
						<div class="panel panel-success">
                        <div class="panel-heading"><center><b>HARGA BAHAN STRATEGIS DI SELURUH PASAR PROV. SULAWESI TENGGARA</b></center></div>
                        <div class="panel-body">
                           <div class="row">
								<ul class="port2">
								
								
				
				
								   <div id="myCarousel2" class="carousel slide" data-ride="carousel">
										<!-- Carousel Indikator 
										<ol class="carousel-indicators">
										<li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
										<li data-target="#myCarousel2" data-slide-to="1"></li>
										
										</ol>-->
								
								
										<!-- Wrapper for Slide -->
										<div class="carousel-inner" role="listbox">
										
											<?php
														$i  = 1;
														foreach ($kabupaten2 as $row) {
															//$Pasar     = $row->nama_pasar;
															$Kabupaten     = $row->nama_kabupaten;
															 
															$item_class = ($i == 1) ? 'item active' : 'item';
														?>
														<div class="<?php echo $item_class; ?>">
																<center><b><?php echo $Kabupaten?></b></center><br> 
																<?php //echo $Pasar?><br>
																
														
														
													<center>	
													<table class="table-striped table-hover pull-center"  >
                        
																		<thead>
																		  <tr>
																			<th style="width:45%">
																			  Nama Komoditi
																			</th>
																			<th style="width:15%">
																			  Satuan
																			</th>
																			<th style="width:18%">
																			  Harga Kemarin
																			</th>
																			<th style="width:5%">
																			</th>
																			<th style="width:20%">
																			  Harga Hari Ini
																			</th>
																			
																			
																		  </tr>
																		</thead>
																		<tbody>
																		<?php
																				
																		foreach ($strategis as $v) {?>    
																		  <tr class="odd" style="font-size:14px;"class="gradeA success">
																		  <?php
																				if($row->id_kabupaten==$v->id_kabupaten) {
																		  ?>
																		  
																			<td  class="w96px txt-right">
																			  <?php echo $v->nama_komoditi?>
																			</td>
																			<td  class="w96px txt-right">
																			  <?php echo $v->satuan?>
																			</td>
																			
																			<td>
																			  <?php 
																			  $harga_kemarin = $v->harga_kemarin;
																			  $angka_format = "Rp " . number_format($harga_kemarin,0,",",".");
																			  echo $angka_format
																			   ?>
																			  <?php
																			  if($v->harga_hari_ini > $v->harga_kemarin){
																			  $v->harga_hari_ini - $v->harga_kemarin
																			  ?>
																					  <td>
																						<img alt="harga naik" src="<?php echo base_url()?>asset/template/images/up_tr.png" style="margin-top:4px;display:inline" >
																					  </td>
																			  <?php
																			  } else if($v->harga_hari_ini < $v->harga_kemarin){
																			  ?>
																					  <td>
																						<img alt="harga turun" src="<?php echo base_url()?>asset/template/images/down_tr.png" style="margin-top:4px;display:inline">
																					  </td>
																			  <?php
																			  } else {
																			  ?>
																					  <td>
																					  </td>																			  
																			  <?php
																			  }
																			  ?>
																			  
																			  </td>	
																			<td>
																			  <?php 
																			  $harga_hari_ini = $v->harga_hari_ini;
																			  $angka_format2 = "Rp " . number_format($harga_hari_ini,0,",",".");
																			  echo $angka_format2
																			  ?>
																			</td>		
																		
																		 <?php
																			}}
																		  ?>
																			<td class="hidden-xs">
																		  </td>
																		 
																		  </tr>
																		</tbody>
																	  </table>
																	  </center>
															</div>
															<?php
															$i++;
														}
														?> 		  
																	  
										  <a class="carousel-control left" href="#myCarousel2" data-slide="prev">&lsaquo;</a>
											<a class="carousel-control right" href="#myCarousel2" data-slide="next">&rsaquo;</a>
									  </div>
								</ul>
							</div> 
						</div>
                        <div class="panel-footer">

                        </div>
						</div>
					</div>
							
							</div>
							
		
							
			</div>
		</section>

		<section id="berita">
			<div class="container"><br><br><br><br><br>
				<div class="text-center"><img class="img-responsive displayed" src="<?php echo base_url()?>asset/template/images/berita2.png"  />
				</div>
			<div class="row">
		        <!-- Blog Column -->
		        <div class="col-md-12">
		            <h1>
		            </h1>
		            <!-- First Blog Post -->
					<?php foreach($berita as $berita){
					
							$content = $berita->content;
							$content=character_limiter($content,500);
					?>	
		            <div class="row blogu">
		                <div class="col-sm-4 col-md-4 ">
		                    <div class="blog-thumb">
		                            <img class="img-responsive" src="<?php echo base_url()?>files/berita_image/<?php echo $berita->gambar?>" alt="photo" width="350" height="200">
		                      </div>
		                </div>
						
						
								<div class='col-sm-8 col-md-8'>
								<h2 class='blog-title'>
									<a href='#'>
									<?php echo $berita->judul?></a>
								</h2>
								<p><i class='fa fa-calendar-o'></i>  
								<?php 
								$tanggal = date('d F Y', strtotime($berita->tanggal ));
								echo $tanggal?> 
								 <span class='comments-padding'></span>
		                        <!--<i class='fa fa-comment'></i> 0 comments-->
		                    </p>
							 <p><?php echo $content ?></p><br>
		                </div> 
						<div class='text-right'><a href="<?php echo base_url()?>menu_berita/berita/<?php echo $berita->id_berita?>" class='btn btn-primary btn-noborder-radius hvr-bounce-to-bottom'>Read More</a></div>
					
		            </div>
		            <hr>	
					<?php
					}
					?>
		            <!-- Second Blog Post 
		            <div class="row">
		                <div class="col-sm-4 col-md-4">
		                    <div class="blog-thumb">
		                        <a href="#">
		                            <img class="img-responsive" src="<?php echo base_url()?>asset/template/images/blog-photo2.jpg" alt="photo">
		                        </a>
		                    </div>
		                </div>
		                <div class="col-sm-8 col-md-8">
		                    <h2 class="blog-title">
		                        <a href="#">Post title 2</a>
		                    </h2>
		                    <p><i class="fa fa-calendar-o"></i> August 28, 2013 
		                       <span class="comments-padding"></span>
		                       <i class="fa fa-comment"></i> 3 comments
		                    </p>
		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis ipsum officiis rerum.</p>
		                </div>
		            </div>
		            <hr>-->
		            <!-- Third Blog Post 
		            <div class="row">
		                <div class="col-sm-4 col-md-4">
		                    <div class="blog-thumb">
		                        <a href="#">
		                        <img class="img-responsive" src="<?php echo base_url()?>asset/template/images/blog-photo3.jpg" alt="photo">
		                        </a>
		                    </div>
		                </div>
		                <div class="col-sm-8 col-md-8">
		                    <h2 class="blog-title">
		                        <a href="#">Post title 3</a>
		                    </h2>
		                    <p><i class="fa fa-calendar-o"></i>  August 28, 2013 
		                        <span class="comments-padding"></span>
		                        <i class="fa fa-comment"></i> 1 comment
		                    </p>
		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis ipsum officiis rerum.</p>
		                </div>
		            </div>
		            <hr>-->
		            <!-- Fourth Blog Post
		            <div class="row">
		                <div class="col-sm-4 col-md-4">
		                    <div class="blog-thumb">
		                        <a href="#">
		                            <img class="img-responsive" src="<?php echo base_url()?>asset/template/images/blog-photo1.jpg" alt="photo">
		                        </a>
		                    </div>
		                </div>
		                <div class="col-sm-8 col-md-8">
		                    <h2 class="blog-title">
		                        <a href="#">Post title 4</a>
		                    </h2>
		                    <p><i class="fa fa-calendar-o"></i> August 28, 2013 
		                       <span class="comments-padding"></span>
		                       <i class="fa fa-comment"></i> 3 comments
		                    </p>
		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis ipsum officiis rerum.</p>
		                </div>
		            </div>

		             <hr> -->
		            <!-- Fifth Blog Post 
		            <div class="row">
		                <div class="col-sm-4 col-md-4">
		                    <div class="blog-thumb">
		                        <a href="#">
		                            <img class="img-responsive" src="<?php echo base_url()?>asset/template/images/blog-photo2.jpg" alt="photo">
		                        </a>
		                    </div>
		                </div>
		                <div class="col-sm-8 col-md-8">
		                    <h2 class="blog-title">
		                        <a href="#">Post title 5</a>
		                    </h2>
		                    <p><i class="fa fa-calendar-o"></i> August 28, 2013 
		                       <span class="comments-padding"></span>
		                       <i class="fa fa-comment"></i> 3 comments
		                    </p>
		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis ipsum officiis rerum.</p>
		                </div>
		            </div>

		             <hr>-->
		            <!-- Six Blog Post
		            <div class="row">
		                <div class="col-sm-4 col-md-4">
		                    <div class="blog-thumb">
		                        <a href="#">
		                            <img class="img-responsive" src="<?php echo base_url()?>asset/template/images/blog-photo3.jpg" alt="photo">
		                        </a>
		                    </div>
		                </div>
		                <div class="col-sm-8 col-md-8">
		                    <h2 class="blog-title">
		                        <a href="#">Post title 6</a>
		                    </h2>
		                    <p><i class="fa fa-calendar-o"></i> August 28, 2013 
		                       <span class="comments-padding"></span>
		                       <i class="fa fa-comment"></i> 3 comments
		                    </p>
		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis ipsum officiis rerum.</p>
		                </div>
		            </div> -->
		            


		        </div>
				</div>
			</div>
		</section>

		<!--<div id="location">
			<div class="row prodmap">
				<div id="map-canvas-holder" class="map_holder" style="height: 400px;"></div>
			</div>
		</div>-->

		<section id="contact">
			<div class="container"> 
				<div class="row">
					<div class="col-md-12">
					<center>
						<div class="col-lg-12">
							<div class="text-center"><img class="img-responsive displayed" src="<?php echo base_url()?>asset/template/images/kontak2.png" alt="company logo" />
							</div>
						</div>
						
						<div class="col-lg-13">
							<form >
									<div class="col-sm-6 height-contact-element">
										<div class="col-md-12 height-contact-element">
											<div class="form-group">
												<i class="fa fa-2x fa-map-marker"></i>
												<span class="text">Alamat : Jl. Drs. H. Abdullah Silondae, No. 116, Kendari, Sulawesi Tenggara</span>
											</div>
										</div>
									</div>
									<div class="col-sm-6 height-contact-element">
										<div class="col-md-12 height-contact-element">
											<div class="form-group">
												<i class="fa fa-2x fa-phone"></i>
												<span class="text">Telp/Fax : +62 401 3128339</span>
											 </div>
										</div>
									</div>
									
							</form>
						</div></center>
						<!--<div class="col-lg-5 col-md-3 col-lg-offset-1 col-md-offset-1">
							<div class="row">
								<div class="col-md-12 height-contact-element">
									<div class="form-group">
										<i class="fa fa-2x fa-map-marker"></i>
										<span class="text">LOCATION</span>
									</div>
								</div>
								<div class="col-md-12 height-contact-element">
									<div class="form-group">
										<i class="fa fa-2x fa-phone"></i>
										<span class="text">0051 768622115</span>
									 </div>
								 </div>
								<div class="col-md-12 height-contact-element">    
									<div class="form-group">
										<i class="fa fa-2x fa-envelope"></i>
										<span class="text">info@company.com</span>
									</div>
								</div>
							</div>
						</div>-->
					</div>
				</div>
			</div>
		</section>

		<section id="follow-us">
			<div class="container"> 
				<div class="text-center height-contact-element">
					<h2>FOLLOW US</h2>
				</div>
				<img class="img-responsive displayed" src="<?php echo base_url();?>asset/template/images/short.png" alt="short" />
				<div class="text-center height-contact-element">
					<ul class="list-unstyled list-inline list-social-icons">
						<li class="active"><a href="#"><i class="fa fa-facebook social-icons"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter social-icons"></i></a></li>
						<!--<li><a href="#"><i class="fa fa-google-plus social-icons"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin social-icons"></i></a></li>-->
					</ul>
				</div>
			</div>
		</section>
		
		