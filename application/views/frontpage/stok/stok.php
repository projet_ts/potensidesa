 
 <!-- Page Content -->
	    <div class="container blog singlepost">
			<div class="row">
				<article class="col-md-12"><br>
			        <h1 align="center" class="page-header sidebar-title">Informasi Ketersediaan Stok </h1>
			        
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>Informasi !</strong> silahkan mengisi data untuk melihat informasi keterediaan stok komoditi.
					</div>
				</div>

			<!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-body">
                      
                      <?php echo form_open("Menu_stok/stok_result", 
                            "class='form-horizontal' row-border")?> 
                      
					  <div class="form-group">
                        <label class="col-md-2 control-label">Nama Komoditi</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                 <select class="form-control selectpicker" data-live-search="true" name="id_komoditi_stok" required>
								<option value="">
									- Nama Komoditi -
									</option>
                                <?php
									foreach ($stok as $cb) {
                                    echo "<option value='$cb->id_komoditi_stok'>
									$cb->nama_komoditi</option>";
									}
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div> 
					  <div class="form-group">
                        <label class="col-md-2 control-label">Tahun</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control selectpicker" data-live-search="true" name="tahun" required>
										<option value="">- Tahun -</option>
										<?php
										$thn = date("Y")+ 2;
											for($i=2016;$i<=$thn;$i++){
											  echo "<option value='".$i."'>".$i."</option>";
											}
										?>
								</select>
                                </div>
                            </div>  
                        </div>
                      </div>
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary" >Cetak</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
					

			    </article>
				<!-- Blog Sidebar Column -->
			</div>
	    </div>


<section id="contact">
			<div class="container"> 
				<div class="row">
					<div class="col-md-12">
					<center>	<div class="col-lg-12">
							<div class="text-center"><img class="img-responsive displayed" src="<?php echo base_url()?>asset/template/images/kontak2.png" alt="company logo" />
							</div>
						</div>
						
						<div class="col-lg-13">
							<form >
									<div class="col-sm-6 height-contact-element">
										<div class="col-md-12 height-contact-element">
											<div class="form-group">
												<i class="fa fa-2x fa-map-marker"></i>
												<span class="text">Alamat : Jl. Drs. H. Abdullah Silondae, No. 116, Kendari, Sulawesi Tenggara</span>
											</div>
										</div>
									</div>
									<div class="col-sm-6 height-contact-element">
										<div class="col-md-12 height-contact-element">
											<div class="form-group">
												<i class="fa fa-2x fa-phone"></i>
												<span class="text">Telp/Fax : +62 401 3128339</span>
											 </div>
										</div>
									</div>
									
							</form>
						</div></center>
						<!--<div class="col-lg-5 col-md-3 col-lg-offset-1 col-md-offset-1">
							<div class="row">
								<div class="col-md-12 height-contact-element">
									<div class="form-group">
										<i class="fa fa-2x fa-map-marker"></i>
										<span class="text">LOCATION</span>
									</div>
								</div>
								<div class="col-md-12 height-contact-element">
									<div class="form-group">
										<i class="fa fa-2x fa-phone"></i>
										<span class="text">0051 768622115</span>
									 </div>
								 </div>
								<div class="col-md-12 height-contact-element">    
									<div class="form-group">
										<i class="fa fa-2x fa-envelope"></i>
										<span class="text">info@company.com</span>
									</div>
								</div>
							</div>
						</div>-->
					</div>
				</div>
			</div>
		</section>

			<section id="follow-us">
			<div class="container"> 
				<div class="text-center height-contact-element">
					<h2>FOLLOW US</h2>
				</div>
				<img class="img-responsive displayed" src="<?php echo base_url();?>asset/template/images/short.png" alt="short" />
				<div class="text-center height-contact-element">
					<ul class="list-unstyled list-inline list-social-icons">
						<li class="active"><a href="#"><i class="fa fa-facebook social-icons"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter social-icons"></i></a></li>
						<!--<li><a href="#"><i class="fa fa-google-plus social-icons"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin social-icons"></i></a></li>-->
					</ul>
				</div>
			</div>
		</section>
		
		