<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Komoditi</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("komoditi/update_data", 
                            "class='form-horizontal' row-border")?> 
                      <input type="hidden" name="id_komoditi" value="<?php echo $entry->id_komoditi?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Komoditi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_komoditi" 
							value="<?php echo $entry->nama_komoditi?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Satuan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="satuan" 
							value="<?php echo $entry->satuan?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Jenis Komoditi</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control" name="jenis_komoditi" required>
								<option value="">
									- Jenis Komoditi -
								</option>
                                <?php
									if ($entry->jenis_komoditi == 'Bahan Pokok') {
										echo "<option value='Bahan Pokok' selected>
											  Bahan Pokok</option>
											  <option value='Bahan Strategis'>
											  Bahan Strategis</option>
											  ";
									} else if ($entry->jenis_komoditi == 'Bahan Strategis') {
										echo "<option value='Bahan Pokok'>
											  Bahan Pokok</option>
											  <option value='Bahan Strategis' selected>
											  Bahan Strategis</option>
											  ";
									}
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" title="Update Data">Update</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/komoditi/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data komoditi">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   