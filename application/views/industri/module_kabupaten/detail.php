<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail kabupaten</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("", 
                            "class='form-horizontal' row-border")?> 
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">kabupaten</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_kabupaten" 
							value="<?php echo $entry->nama_kabupaten?>"  maxlength="100" readonly>
                        </div>
                      </div>
					  <div class="form-group">
						<label class="col-md-2 control-label">Keterangan</label>
						<div class="col-md-6">
                          <textarea class="form-control" type="text" name="keterangan" 
						  placeholder="Keterangan" readonly><?php echo $entry->keterangan?></textarea>
                        </div>
					   </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <?php echo anchor('/kabupaten/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data kabupaten">
											Kembali</button>'); ?>
                        </div>
						
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   