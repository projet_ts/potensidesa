<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah Komoditi Tetap</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("komoditi_tetap/create_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Komoditi Tetap</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_komoditi_tetap" 
							placeholder="Nama Komoditi Tetap" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Satuan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="satuan" 
							placeholder="Satuan" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Komoditi</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="id_jenis_komoditi"required >
                                <option value="">
                                  - Jenis Komoditi -
                                </option>
                                <?php
                                foreach ($jenis_komoditi as $cb) {
                                    echo "<option value='$cb->id_jenis_komoditi'>
                                  $cb->nama_jenis_komoditi</option>"; 
                                }
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" >Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/komoditi_tetap/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data komoditi Tetap">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   