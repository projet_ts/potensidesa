<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data Template</a>
                    </div>
                  </div>
                  <div class="widget-body">
					<div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									<?php echo anchor('/template_report/insert', 
									'<button type="button" class="btn btn-info 
										" data-toggle="tooltip" data-placement="top" title="Tambah Data Template">
											Tambah</button>'); ?>
									<?php echo anchor('/template_report/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Refresh Data Template">
											Refresh</button>'); ?>
								</div>
							</div>
							
							
							<?php echo form_open("template_report/result");?>
							
							<div class="col-md-3 col-sm-4 col-xs-4">
								<input type="text" class="form-control" name="key" placeholder="Cari">
							</div>
							
							<div class="btn-group">
								<button type="submit" class="btn btn-info" title="Cari Data Template">
									<i class="fa fa-search"></i>
								</button>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
                      
					  
                    <div class="clearfix">
                    </div>  
                    <div id="dt_example" class="example_alt_pagination">
                        <br>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left" >
                        
                        <thead>
                          <tr>
                            <th style="width:5%">
                              No
                            </th>  
                            <th style="width:30%">
                              Judul Template
                            </th>
                            <th style="width:15%">
                              Keterangan
                            </th>
                            <th style="width:10%">
                              Pilihan
                            </th>
                            
                          </tr>
                        </thead>
                        <tbody>
                        <?php
							
                            $no = $number + 1;
                            foreach ($template_report as $v) {


                        ?>    
                          <tr class="gradeA success">
                            <td>
                              <?php echo $no ?>
                            </td>  
                            <td>
                              <?php echo $v->judul ?>
                            </td>
                            <td>
                              <?php echo $v->keterangan ?>
                            </td>
                            <td class="hidden-xs">
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Pilihan 
                                  <span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu">
                                <li><?php echo anchor("/template_report/detail?id=".$v->id_template, "Detail"); ?></li>
                                <li><?php echo anchor("/template_report/update?id=".$v->id_template, "Edit"); ?></li>
                                <li class="divider"></li>
                                <li><?php echo anchor("/template_report/delete?id=".$v->id_template, "Hapus"
								, "onclick=\"return confirm('Anda Yakin Akan Menghapus?')\""); ?></li>
                              </ul>
                            </div>
                          </td>
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
						<?php echo $links?>
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   
