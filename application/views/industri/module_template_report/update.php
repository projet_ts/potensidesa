<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Template</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("template_report/update_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <input type="hidden" name="id_template" value="<?php echo $entry->id_template?>">
					  
                      <div class="form-group">
                        <label class="col-md-2 control-label">Judul Template</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="judul" value="<?php echo $entry->judul ?>">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">Desain</label>
                        <div class="col-md-10">
						  <textarea name="desain_template" class="textarea form-control ckeditor"><?php echo $entry->desain_template?></textarea>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Keterangan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="keterangan" 
                                 value="<?php echo $entry->keterangan?>">
                        </div>
                      </div>
					  
                      
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" title="Update Data">Update</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/template_report/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data User">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   