
                      <div class="form-group">
                        <label class="col-md-2 control-label">NPWP Perusahaan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="npwp_perusahaan" 
							value="<?php echo $entry->npwp_perusahaan ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Badan Usaha</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="badan_usaha" readonly>
                                <option>
                                  - Jenis Badan Usaha -
                                </option>
                                <?php
								$jb=array(1=>"CV", "PT", "KOPERASI", "PERUM", "PERSERO", "FIRMA", "MAATSCHAP", "YAYASAN", 
											"PERUSAHAAN DAERAH", "PERORANGAN", "BMT");
								
								for($i=1;$i<=11;$i++) {
									if($jb[$i]==$entry->badan_usaha) {
										echo "<option value='$jb[$i]' selected/>$jb[$i]</option>";
									} else {
										echo "<option value='$jb[$i]'/>$jb[$i]</option>";
									}
								}
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Perusahaan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_perusahaan" 
							value="<?php echo $entry->nama_perusahaan ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Direktur</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_direktur" 
							value="<?php echo $entry->nama_direktur ?>" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Bidang Usaha</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="bidang_usaha" readonly>
                                <option>
                                  - Jenis Bidang Usaha -
                                </option>
                                <?php
								$jb=array(1=>"INDUSTRI", "NON INDUSTRI");
								
								for($i=1;$i<=2;$i++) {
									if($jb[$i]==$entry->bidang_usaha) {
										echo "<option value='$jb[$i]' selected/>$jb[$i]</option>";
									} else {
										echo "<option value='$jb[$i]'/>$jb[$i]</option>";
									}
								}
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Status Badan Hukum</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="status_badan_hukum" readonly>
                                <option>
                                  - Status Badan Hukum -
                                </option>
                                <?php
								$jb=array(1=>"Belum Berbadan Hukum", "Sudah Berbadan Hukum");
								
								for($i=1;$i<=2;$i++) {
									if($jb[$i]==$entry->status_badan_hukum) {
										echo "<option value='$jb[$i]' selected/>$jb[$i]</option>";
									} else {
										echo "<option value='$jb[$i]'/>$jb[$i]</option>";
									}
								}
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Alamat Perusahaan</label>
                        <div class="col-md-6">
						  <textarea name="alamat_perusahaan" class="textarea form-control" readonly><?php echo $entry->alamat_perusahaan ?></textarea>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Telepon Perusahaan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="telepon_perusahaan" 
							value="<?php echo $entry->telepon_perusahaan ?>" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Email Perusahaan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="email_perusahaan" 
							value="<?php echo $entry->email_perusahaan ?>" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">No Akta Pendirian / Perubahan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nomor_akta" 
							value="<?php echo $entry->nomor_akta ?>" readonly>
                        </div>
                      </div>
                     