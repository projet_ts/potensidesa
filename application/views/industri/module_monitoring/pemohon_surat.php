
                      <div class="form-group">
                        <label class="col-md-2 control-label">NPWP Pemohon</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="npwp_pemohon" 
							value="<?php echo $entry->npwp_pemohon ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Jenis Identitas</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="jenis_identitas" readonly>
                                <option>
                                  - Jenis Identitas -
                                </option>
                                <?php
								$jb=array(1=>"KTP", "SIM", "LAINNYA");
								
								for($i=1;$i<=3;$i++) {
									if($jb[$i]==$entry->jenis_identitas) {
										echo "<option value='$jb[$i]' selected/>$jb[$i]</option>";
									} else {
										echo "<option value='$jb[$i]'/>$jb[$i]</option>";
									}
								}
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Nomor Identitas</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="no_identitas"
							value="<?php echo $entry->no_identitas ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Pemohon</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_pemohon" 
							value="<?php echo $entry->nama_pemohon ?>" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Jabatan</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="jabatan" readonly>
                                <option>
                                  - Nama Jabatan -
                                </option>
                                <?php
								$jb=array(1=>"Direktur", "Wakil Direktur", "Pemegang Saham", "Kuasa");
								
								for($i=1;$i<=4;$i++) {
									if($jb[$i]==$entry->jabatan) {
										echo "<option value='$jb[$i]' selected/>$jb[$i]</option>";
									} else {
										echo "<option value='$jb[$i]'/>$jb[$i]</option>";
									}
								}
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="col-md-2 control-label">Alamat Pemohon</label>
                        <div class="col-md-6">
						  <textarea name="alamat_pemohon" class="textarea form-control" readonly><?php echo $entry->alamat_pemohon ?></textarea>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Handphone Pemohon</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="hp_pemohon" 
							value="<?php echo $entry->hp_pemohon ?>" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Telepon Pemohon</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="telepon_pemohon" 
							value="<?php echo $entry->telepon_pemohon ?>" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Fax Pemohon</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="fax_pemohon" 
							value="<?php echo $entry->fax_pemohon ?>" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Email Pemohon</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="email_pemohon" 
							value="<?php echo $entry->email_pemohon ?>" readonly>
                        </div>
                      </div>
					  