<div class="form-group">
	<label class="col-md-2 control-label">Jenis/Bidang Usaha</label>
	<div class="col-md-6">
		<textarea name="jenis_usaha" class="textarea form-control" readonly><?php echo $entry->jenis_usaha?></textarea>
	</div>
</div>

<h3>Lokasi Usaha</h3>
<div class="form-group">
	<label class="col-md-2 control-label">Jalan</label>
	<div class="col-md-6">
		<input class="form-control" type="text" name="jalan" value="<?php echo $entry->jalan?>" readonly>
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Desa/Kelurahan</label>
	<div class="col-md-6">
		<input class="form-control" type="text" name="desa" value="<?php echo $entry->desa?>" readonly>
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Kecamatan</label>
	<div class="col-md-6">
		<input class="form-control" type="text" name="kecamatan" value="<?php echo $entry->kecamatan?>" readonly>
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Kabupaten</label>
	<div class="col-md-6">
		<input class="form-control" type="text" name="kabupaten" value="<?php echo $entry->kabupaten?>" readonly>
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Jumlah Tenaga Kerja</label>
	<div class="col-md-6">
		<input class="form-control" type="text" name="tenaga_kerja" value="<?php echo $entry->tenaga_kerja?>" readonly> 
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Luas Bangunan Usaha</label>
	<div class="col-md-6">
		<input class="form-control" type="text" name="luas" value="<?php echo $entry->luas?>" readonly>
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Peralatan</label>
	<div class="col-md-6">
		<textarea name="peralatan" class="textarea form-control" readonly><?php echo $entry->peralatan?></textarea>
	</div>
</div>
<div class="form-group">
	<label class="col-md-2 control-label">Nilai Investasi</label>
	<div class="col-md-6">
		<input class="form-control" type="text" name="investasi" value="<?php echo $entry->investasi?>" readonly>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Waktu Usaha</label>
	<div class="col-sm-10">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-4">
				<select id="active" class="form-control" name="waktu_usaha" readonly>
				<?php
				$jb=array(1=>"Siang", "Malam", "Siang-Malam");
				for($i=1;$i<=3;$i++) {
					if($jb[$i]==$entry->waktu_usaha) {
						echo "<option value='$jb[$i]' selected/>$jb[$i]</option>";
					} else {
						echo "<option value='$jb[$i]'/>$jb[$i]</option>";
					}
				}
                ?>
                </select>
			</div>
		</div>    
	</div>
</div>