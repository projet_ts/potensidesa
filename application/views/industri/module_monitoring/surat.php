<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data Monitoring Permohonan / Surat</a>
                    </div>
                  </div>
                  <div class="widget-body">
                      <div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									
									<?php echo anchor('/monitoring/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Refresh Data Monitoring">
											Refresh</button>'); ?>
								</div>
							</div>
							
							<?php echo form_open("reviewkasi/result");?>
							
							<div class="col-md-3 col-sm-4 col-xs-4">
								<input type="text" class="form-control" name="key" placeholder="Cari">
							</div>
							
							<div class="btn-group">
								<button type="submit" class="btn btn-info" id="confirm" title="Cari Data Review Surat">
									<i class="fa fa-search"></i>
								</button>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
                    <div class="clearfix">
                    </div> 
                      
                    <div id="dt_example" class="example_alt_pagination">
                        <br>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left">
                        
                        <thead>
                          <tr>
                            <th style="width:5%">
                              No
                            </th>  
                            <th style="width:20%">
                              NUP / Tanggal Masuk
                            </th>
                            <th style="width:20%">
							  Identitas Pemohon
                            </th>
							<th style="width:20%">
							  Identitas Perusahaan
                            </th>
                            <th style="width:20%">
                              Penerima / Pemroses
                            </th>
							<th style="width:10%">
                              Status
                            </th>
							<th style="width:1%">
                              Pilihan
                            </th>
                            
                          </tr>
                        </thead>
                        
                        <tbody>
                        <?php
                            $no = $number + 1;
                            foreach ($surat as $v) {
								$tempo = new DateTime($v->tgl_jatuh_tempo);
								$jedatempo = new DateTime($v->tgl_jatuh_tempo);
								$jedatempo = $jedatempo->modify('-1 month');
								
								//waktu sekarang
								//$now = new DateTime('2020-11-17');
								$now = new DateTime(date("Y-m-d"));
								
								$cls = "success";
								
								if ($now >= $tempo) {
									$cls = "danger";
								} else if ($now >= $jedatempo) {
									$cls = "warning";
								}
                        ?>    
						 <tr class="gradeA <?php echo $cls ?>">
							<td rowspan="2">
                              <?php echo $no ?>
                            </td>  
							<td colspan="6"><b><?php echo $v->nama_izin?></b> - <i><?php echo $v->nama_instansi?></i></td>
						</tr>
                          <tr >
                             
                            <td>
                              <?php echo $v->nup." <br> <i>".$v->tanggal_masuk." / ".$v->jam_masuk."</i>" ?>
                            </td>
                            <td>
                              <?php echo $v->no_identitas." <br> ".$v->nama_pemohon?>
                            </td>
							<td>
                              <?php echo $v->npwp_perusahaan." <br> ".$v->nama_perusahaan?>
                            </td>
                            <td>
								<?php echo $v->frontoffice." / ".$v->backoffice?>
                            </td>
							<?php
								foreach ($alur as $r) {
									$fo = $r->step_frontoffice;
									$kab = $r->step_kabid;
									$dis = $r->step_disposisi;
									$sur = $r->step_survey;
									$ret = $r->step_retribusi;
									//$bo = $r->step_backoffice;
									$pem = $r->step_pembayaran;
									$srt = $r->step_surat;
									$amb = $r->step_pengambilan;
									if ($r->nup == $v->nup) {
										//$alurx = $r->nup;
										if($fo == "1" && $kab == "0" && $dis == "0" && $sur == "0" && $ret == "0" && $pem == "0" && $srt == "0" && $amb == "0") {
											$alurx = "Frontoffice";
										} else if($fo == "1" && $kab == "1" && $dis == "0" && $sur == "0" && $ret == "0" && $pem == "0" && $srt == "0" && $amb == "0") {
											$alurx = "Kabid";
										} else if($fo == "1" && $kab == "1" && $dis == "1" && $sur == "0" && $ret == "0" && $pem == "0" && $srt == "0" && $amb == "0") {
											$alurx = "Disposisi";
										} else if($fo == "1" && $kab == "1" && $dis == "1" && $sur == "1" && $ret == "0" && $pem == "0" && $srt == "0" && $amb == "0") {
											$alurx = "Survey";
										} else if($fo == "1" && $kab == "1" && $dis == "1" && $sur == "1" && $ret == "1" && $pem == "0" && $srt == "0" && $amb == "0") {
											$alurx = "Retribusi";
										} else if($fo == "1" && $kab == "1" && $dis == "1" && $sur == "1" && $ret == "1" && $pem == "1" && $srt == "0" && $amb == "0") {
											$alurx = "Pembayaran";
										} else if($fo == "1" && $kab == "1" && $dis == "1" && $sur == "1" && $ret == "1" && $pem == "1" && $srt == "1" && $amb == "0") {
											$alurx = "Surat";
										} else if($fo == "1" && $kab == "1" && $dis == "1" && $sur == "1" && $ret == "1" && $pem == "1" && $srt == "1" && $amb == "1") {
											$alurx = "Selesai";
										}
										else {
											$alurx = "Tidak Diketahui";
										}
									}
								}
							?>
							<td>
								<?php echo $alurx; ?>
                            </td>
                            <td class="hidden-xs">
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Pilihan 
                                  <span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu">
								<?php 
									if($v->step_frontoffice==1 AND $v->step_kabid==1 AND $v->step_backoffice==1) {
								?>
										<li><?php echo anchor("/monitoring/printout?id=".$v->nup, "<i class='fa fa-print'></i> Cetak", "target='_blank'"); ?></li>
										<li><?php echo anchor("/monitoring/surat?id=".$v->nup, "<i class='fa fa-file-o'></i> Data"); ?></li>
								<?php
									} else {
								?>
									<li><?php echo anchor("/monitoring/surat?id=".$v->nup, "<i class='fa fa-file-o'></i> Data"); ?></li>
								<?php
									}
								?>
							    
                              </ul>
                            </div>
                          </td>
                          </tr>
						 
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
					  <?php echo $links?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   