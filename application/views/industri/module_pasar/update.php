<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Pasar</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("pasar/update_data", 
                            "class='form-horizontal' row-border")?> 
                      <input type="hidden" name="id_pasar" value="<?php echo $entry->id_pasar?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Pasar</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_pasar" 
							value="<?php echo $entry->nama_pasar?>" maxlength="100" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Keterangan</label>
                        <div class="col-md-6">
						  <textarea name="keterangan" class="textarea form-control"><?php echo $entry->keterangan?></textarea>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Kabupaten</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" required>
								<?php
								if($group_id=="group1000"){?>
									<option value="">
									- Kabupaten -
									</option>
									<?php
									foreach ($kabupaten as $cb) {
									if ($cb->id_kabupaten == $entry->id_kabupaten) {
										echo "<option value='$cb->id_kabupaten' selected>
											  $cb->nama_kabupaten</option>";
									} else {
										echo "<option value='$cb->id_kabupaten'>
											  $cb->nama_kabupaten</option>";
									}
									}
								} else {
									foreach ($kabupaten as $cb) {
									if ($cb->id_kabupaten == $entry->id_kabupaten) {
										echo "<option value='$cb->id_kabupaten' selected>
											  $cb->nama_kabupaten</option>";
									} else {
										echo "<option value='$cb->id_kabupaten'>
											  $cb->nama_kabupaten</option>";
									}
                                }
								}
								
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" title="Update Data">Update</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/pasar/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data pasar">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   