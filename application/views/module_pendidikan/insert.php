<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah Data Pendidikan</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-rocket"></i>
                    </span>
                  </div>
                  <div class="widget-body">

                      <?php echo form_open("pendidikan/create_data",
                            "class='form-horizontal' row-border")?>
                      <input type="hidden" name="desa_id" value="<?php echo $id?>">
                      <div class="form-group">
                        <!-- <label class="col-sm-2 control-label">Id pendidikan</label> -->
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="hidden" name="pendidikan_id" placeholder="Tahun" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">SD</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="sd" placeholder="SD" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">SMP</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="smp" placeholder="SMP" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">SMA</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="sma" placeholder="SMA" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">S1</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="s1" placeholder="S1" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">S2</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="s2" placeholder="S2" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">S3</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="s3" placeholder="S3" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Non</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="non" placeholder="Non" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm">Simpan</button>
            						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
            						  <?php echo anchor('/pendidikan/data?id='.$id,
            									'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Kembali Data Hak Akses">
            											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
