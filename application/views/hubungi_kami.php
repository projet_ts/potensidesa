<!--Page header & Title-->
<section id="page_header4">
<div class="page_title4">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Hubungi Kami</h2>
         <div class="page_link"><a href="<?php echo base_url();?>home/beranda">Beranda</a><span><i class="fa fa-long-arrow-right"></i>Hubungi Kami</span></div>
      </div>
    </div>
  </div>
</div>  
</section>

<!--LOcation Map-->
<section class="padding">
<h3 class="hidden">hidden</h3>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="map"></div>
        <div class="search_location text-center">
          <input id="lat3" type="hidden" value="-4.016680">
          <input id="lng3" type="hidden" value="122.538307">
          <input id="address3" type="text" value="" placeholder="Cari Lokasimu">
          <button type="button button3"><i class="fa fa-search"></i></button>
        </div>
      </div>
    </div>
  </div>
</section>

<!--Contact Form & Address-->
<section class="padding bg_grey">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-8">
        <h2 class="heading">Berikan Saran & Kritik Anda</h2>
        <hr class="heading_space">
        <form class="callus" onSubmit="return false"  id="contact_form">
          <div class="row">
          
           <div class="col-md-12">
                    <div id="result" class="text-center form-group"></div>
           </div>
          
            <div class="col-md-6">
              <div class="form-group">
                <input class="form-control" type="text" name="f_name" id="f_name"  placeholder="First Name" required />
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input class="form-control" type="text" name="l_name" id="l_name"  placeholder="Last Name" required />
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input class="form-control" type="email" name="email" id="email"  placeholder="Email Address" required />
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input class="form-control" type="text" name="phone" id="phone" required placeholder="Phone No">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <textarea placeholder="Message" name="message" id="message"></textarea>
              </div>
              <div class="form-group">
                 <div class="btn-submit button3">
                <input type="submit" value="Submit Request" id="btn_contact_submit">
                
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-4 col-sm-4 medix">
        <h2 class="heading">Alamat</h2>
        <hr class="heading_space"> 
        <p><strong>Phone:</strong> 08117797978</p>
        <p><strong>Email:</strong> <a href="mailto:balitbang-sultra@gmail.com">balitbang-sultra@gmail.com</a></p>
        <p><a href="#">Web: http://balitbang.sulawesitenggaraprov.go.id</a></p>
        <p><strong>Address:</strong> Alamat</p>
          <ul class="social_icon">
            <li class="black"><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li class="black"><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
            <li class="black"><a href="#" class="google"><i class="fa fa-instagram"></i></a></li>
          </ul>
      </div>
    </div>
  </div>
</section>
