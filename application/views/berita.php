<?php foreach($berita as $news){ ?>

<section class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="heading"><?php echo $news->judul;?></h2>
         <hr class="heading_space">
      </div>
      <div class="col-md-7 col-sm-6 department">
        <?php echo $news->content;?>
        <a class="red-btn button3" data-text="Appointment"><?php echo $news->tanggal;?></a>
      </div>
      <div class="col-md-5 col-sm-6">
       <img class="img-responsive" src="<?php echo base_url();?>files/image_struktur/<?php echo $news->gambar;?>" height="300" width="350" alt="welcome medix">
      </div>
    </div>
  </div>
</section> 

<?php } ?>