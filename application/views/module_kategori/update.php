<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="left-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Update Data Kategori Perizinan</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-rocket"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("kategori/update_data", 
                            "class='form-horizontal' row-border")?> 
                      <input type="hidden" name="id_kategori" value="<?php echo $entry->id_kategori?>">
                      
                      <div class="form-group">
                        
                        <label class="col-md-2 control-label">Nama Jenis Kategori</label>
                        <div class="col-md-6">
                            
                            <input class="form-control" type="text" name="nama_jenis" value="<?php echo $entry->nama_jenis?>">
                           
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Keterangan</label>
                        <div class="col-md-6">
                           
                            <input class="form-control" type="text" name="keterangan" value="<?php echo $entry->keterangan?>">
                           
                        </div>
                      </div>
                          
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm">Simpan</button>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   