<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="left-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data Kategori Perizinan</a>
                    </div>
                  </div>
                  <div class="widget-body">
                      <div class="btn-group">
                          <?php echo anchor('kategori/insert', 
                                  '<button type="button" class="btn btn-info 
                                      " data-toggle="tooltip" data-placement="top" title="Tambah Data Tipe Laptop">
                                        Tambah</button>'); ?>
                        
                      </div>
                      
                    <div id="dt_example" class="example_alt_pagination">
                        <br>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
                        
                        <thead>
                          <tr>
                            <th style="width:5%">
                              No
                            </th>  
                            <th style="width:20%">
                              Kategori
                            </th>
                            <th style="width:40%">
                              Keterangan
                            </th>
                            <th style="width:16%">
                              Pilihan
                            </th>
                            
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no = 1;
                            foreach ($entry as $v) {
                        ?>    
                          <tr class="gradeA success">
                            <td>
                              <?php echo $no ?>
                            </td>  
                            <td>
                              <?php echo $v->nama_jenis ?>
                            </td>
                            <td>
                              <?php echo $v->keterangan ?>
                            </td>
                            
                            <td class="hidden-xs">
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Pilihan 
                                  <span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu">
                                <li><?php echo anchor("kategori/detail?id=".$v->id_kategori, "Detail"); ?></li>
                                <li><?php echo anchor("kategori/update?id=".$v->id_kategori, "Edit"); ?></li>
                                <li class="divider"></li>
                                <li><?php echo anchor("kategori/delete?id=".$v->id_kategori, "Hapus"); ?></li>
                              </ul>
                            </div>
                          </td>
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   