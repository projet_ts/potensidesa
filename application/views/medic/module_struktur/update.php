<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Struktur</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open_multipart("struktur/update_data", 
                            "class='form-horizontal' row-border")?> 
                      <input type="hidden" name="id_struktur" value="<?php echo $entry->id_struktur?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama" 
							value="<?php echo $entry->nama?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Jabatan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="jabatan" 
							value="<?php echo $entry->jabatan?>" maxlength="100">
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Deskripsi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="deskripsi" 
							value="<?php echo $entry->deskripsi?>" maxlength="100">
                        </div>
					   </div>
					   <div class="form-group">
						  <label class="col-md-2 control-label" >Upload File</label>
						  <div class="col-md-6">
							  <?php     
			  
							$image = array(
							  'src' => '/files/image_struktur/'.($entry->gambar),
							  'class' => 'photo',
							  'width' => '200',
							  'height' => '200',
							);

							echo img($image); ?>
							  <input class="form-control" type="file" name="userfile" >
						  </div>
						</div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" title="Update Data">Update</button>
						              <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						                <?php echo anchor('/struktur/data', 
									                           '<button type="button" class="btn btn-success 
										                          " data-toggle="tooltip" data-placement="top" title="Kembali Data struktur">
										                          	Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   