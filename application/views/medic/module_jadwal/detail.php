<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail Jadwal</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("", 
                            "class='form-horizontal' row-border")?> 
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Dokter</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_dokter" 
							value="<?php echo $entry->nama_dokter?>" maxlength="100" readonly>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Senin</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="senin" 
							value="<?php echo $entry->senin?>" maxlength="100" readonly>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Selasa</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="selasa" 
							value="<?php echo $entry->selasa?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Rabu</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="rabu" 
							value="<?php echo $entry->rabu?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Kamis</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="kamis" 
							value="<?php echo $entry->kamis?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Jumat</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="jumat" 
							value="<?php echo $entry->jumat?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Sabtu</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="sabtu" 
							value="<?php echo $entry->sabtu?>" maxlength="100" required>
                        </div>
					   </div>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <?php echo anchor('/jadwal/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali ke Data jadwal">
											Kembali</button>'); ?>
                        </div>
						
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   