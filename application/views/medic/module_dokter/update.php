<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Dokter</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open_multipart("dokter/update_data", 
                            "class='form-horizontal' row-border")?> 
                      <input type="hidden" name="id_dokter" value="<?php echo $entry->id_dokter?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Dokter</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_dokter" 
							value="<?php echo $entry->nama_dokter?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Spesialisasi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="divisi" 
							value="<?php echo $entry->divisi?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Deskripsi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="deskripsi" 
							value="<?php echo $entry->deskripsi?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
						  <label class="col-md-2 control-label" >Upload File</label>
						  <div class="col-md-6">
							  <?php     
			  
							$image = array(
							  'src' => '/files/image_struktur/'.($entry->gambar),
							  'class' => 'photo',
							  'width' => '200',
							  'height' => '200',
							);

							echo img($image); ?>
							  <input class="form-control" type="file" name="userfile" >
						  </div>
						</div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">fb</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="fb" 
							value="<?php echo $entry->fb?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Twitter</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="twitter" 
							value="<?php echo $entry->twitter?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Instagram</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="instagram" 
							value="<?php echo $entry->instagram?>" maxlength="100" required>
                        </div>
					   </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" title="Update Data">Update</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/jadwal/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data komoditi">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   