<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah Dokter</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open_multipart("dokter/create_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Dokter</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_dokter" 
							placeholder="Nama Dokter" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Spesialisasi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="divisi" 
							placeholder="divisi" maxlength="100" required>
                        </div>
					   </div>
					 <div class="form-group">
                        <label class="col-md-2 control-label">Deskripsi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="deskripsi" 
							placeholder="deskripsi" maxlength="100" required>
                        </div>
					   </div>
						<div class="form-group">
							<label class="col-md-2 control-label" >Upload File</label>
							  <div class="col-md-6">
								  <input class="form-control" type="file" name="userfile" required>
							  </div>
						</div>

					   <div class="form-group">
                        <label class="col-md-2 control-label">fb</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="fb" 
							placeholder="facebook" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Twitter</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="twitter" 
							placeholder="Twitter" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Instagram</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="instagram" 
							placeholder="instagram" maxlength="100" required>
                        </div>
					   </div>
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" >Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/dokter/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali ke Data Dokter">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   