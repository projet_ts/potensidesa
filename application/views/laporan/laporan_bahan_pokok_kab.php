<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Laporan Bahan Pokok</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
				  <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#home" data-toggle="tab">Kabupaten</a>
                                </li>
                                <li><a href="#profile" data-toggle="tab">Pasar</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="home">
										<?php echo form_open("cetak_bahan_pokok_kab/cetak", 
													"class='form-horizontal' row-border")?>
											  
											  <div class="form-group">
												<label class="col-md-2 control-label">Kabupaten</label>
												  <div class="col-sm-10">
													<div class="row">
														<div class="col-md-4 col-sm-4 col-xs-4">
														<select  class="form-control selectpicker" data-live-search="true" name="id_kabupaten" required>
														
														<?php
														if($group_id=="group1000"){?>
															<option value="">
															- Kabupaten -
															</option>
															<?php
															foreach ($kabupaten as $cb) {
															echo "<option value='$cb->id_kabupaten'>
															$cb->nama_kabupaten</option>";
															}
														} else {
															foreach ($kabupaten as $cb) {
															echo "<option value='$cb->id_kabupaten' selected>
															$cb->nama_kabupaten</option>";
															}
														}
														
														?>
													  </select>
														</div>
													</div>  
												</div>
											  </div>
											 <div class="form-group">
												<label class="col-md-2 control-label">Tanggal</label>
												 <div class="col-md-2">
													<div class='input-group date' id='datetimepicker1'>
														<input class="form-control" type="text"  name="tanggal" placeholder="Tanggal" id="datetimepicker1" maxlength="100" required>
														<span class="input-group-addon">
															<span class="glyphicon glyphicon-calendar"></span>
														</span>
													</div>
												</div>
											  </div>
											  <div class="form-group">
												<div class="col-sm-offset-2 col-sm-10">
												  <button type="submit" class="btn btn-info" >Cetak</button>
												  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
												</div>
											  </div>
											  
											<?php echo form_close();?>
								</div>
                                <div class="tab-pane fade" id="profile">
										<?php echo form_open("cetak_bahan_pokok/cetak", 
												"class='form-horizontal' row-border")?> 
										  
										  <div class="form-group">
											<label class="col-md-2 control-label">Pasar</label>
											  <div class="col-sm-10">
												<div class="row">
													<div class="col-md-4 col-sm-4 col-xs-4">
													<select class="form-control selectpicker" data-live-search="true" name="id_pasar" required>
													
													<!--<?php
													if($group_id=="group1000"){?>
														<option value="">
														- Pasar -
														</option>
														<?php
														foreach ($pasar as $cb) {
														echo "<option value='$cb->id_pasar'>
														$cb->nama_pasar</option>";
														}
													} else {
														foreach ($pasar as $cb) {
														echo "<option value='$cb->id_pasar' selected>
														$cb->nama_pasar</option>";
														}
													}
													
													?>-->
													<option value="">
														- Pasar -
													</option>
													<?php
													foreach ($pasar as $cb) {
														echo "<option value='$cb->id_pasar'>
													  $cb->nama_pasar</option>";
													}
													?>
												  </select>
													</div>
												</div>  
											</div>
										  </div>
										 <div class="form-group">
											<label class="col-md-2 control-label">Tanggal</label>
											<div class="col-md-2">
												<div class='input-group date' id='datetimepicker2'>
													<input class="form-control" type="text"  name="tanggal" placeholder="Tanggal" id="datetimepicker2" maxlength="100" required>
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>
												</div>
											</div>
										  </div>
										  <div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
											  <button type="submit" class="btn btn-info" >Cetak</button>
											  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
											</div>
										  </div>
										<?php echo form_close();?>
								</div>
                            </div>
                    </div>
                    <!-- /.panel -->
                </div>
                
            </div>
                      
                     
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   