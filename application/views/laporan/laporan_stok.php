<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Laporan Stok</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                  <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#home" data-toggle="tab">Mingguan</a>
                                </li>
                                <li><a href="#profile" data-toggle="tab">Tahunan</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="home">
										<?php echo form_open("cetak_stok/cetak_mingguan", 
												"class='form-horizontal' row-border")?> 
										  
										  <div class="form-group">
											<label class="col-md-2 control-label">Kabupaten</label>
											  <div class="col-sm-10">
												<div class="row">
													<div class="col-md-4 col-sm-4 col-xs-4">
													<select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" required>
															<?php
															if($group_id=="group1000"){?>
																<option value="">
																- Kabupaten -
																</option>
																<?php
																foreach ($kabupaten as $cb) {
																echo "<option value='$cb->id_kabupaten'>
																$cb->nama_kabupaten</option>";
																}
															} else {
																foreach ($kabupaten as $cb) {
																echo "<option value='$cb->id_kabupaten' selected>
																$cb->nama_kabupaten</option>";
																}
															}
															
															?>
												  </select>
													</div>
												</div>  
											</div>
										  </div>
										   <div class="form-group">
											<label class="col-md-2 control-label">Bulan</label>
											  <div class="col-sm-10">
												<div class="row">
													<div class="col-md-4 col-sm-4 col-xs-4">
													 <select class="form-control selectpicker" data-live-search="true" name="bulan" required>
																<option value="">- Bulan -</option>
																<option value="Januari">Januari</option>
																<option value="Februari">Februari</option>
																<option value="Maret">Maret</option>
																<option value="April">April</option>
																<option value="Mei">Mei</option>
																<option value="Juni">Juni</option>
																<option value="Juli">Juli</option>
																<option value="Agustus">Agustus</option>
																<option value="September">September</option>
																<option value="Oktober">Oktober</option>
																<option value="November">November</option>
																<option value="Desember">Desember</option>
													</select>
													</div>
												</div>  
											</div>
										  </div> 
										  <div class="form-group">
											<label class="col-md-2 control-label">Minggu</label>
											  <div class="col-sm-10">
												<div class="row">
													<div class="col-md-4 col-sm-4 col-xs-4">
													<select class="form-control selectpicker" data-live-search="true" name="minggu" required>
																<option value="">- Minggu -</option>
																<option value="Minggu 1">Minggu 1</option>
																<option value="Minggu 2">Minggu 2</option>
																<option value="Minggu 3">Minggu 3</option>
																<option value="Minggu 4">Minggu 4</option>
													</select>
													</div>
												</div>  
											</div>
										  </div>
										 <div class="form-group">
											<label class="col-md-2 control-label">Tahun</label>
											<div class="col-md-2">
												<select class="form-control selectpicker" data-live-search="true" name="tahun" required>
													 <option value="">- Tahun -</option>
													<?php
													$tahun = date("Y")+ 2;
													echo $tahun;
													for($i=2016;$i<=$tahun;$i++){
														echo "<option value='".$i."'>".$i."</option>";
													}
													?>
												</select>
											</div>
										  </div>
										  
											
											
											
										  <div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
											  <button type="submit" class="btn btn-info" >Cetak</button>
											  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
											</div>
										  </div>
										<?php echo form_close();?>
								</div>
                                <div class="tab-pane fade" id="profile">
										<?php echo form_open("cetak_stok/cetak", 
												"class='form-horizontal' row-border")?> 
										  
										  <div class="form-group">
											<label class="col-md-2 control-label">Kabupaten</label>
											  <div class="col-sm-10">
												<div class="row">
													<div class="col-md-4 col-sm-4 col-xs-4">
													<select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" required>
															<?php
															if($group_id=="group1000"){?>
																<option value="">
																- Kabupaten -
																</option>
																<?php
																foreach ($kabupaten as $cb) {
																echo "<option value='$cb->id_kabupaten'>
																$cb->nama_kabupaten</option>";
																}
															} else {
																foreach ($kabupaten as $cb) {
																echo "<option value='$cb->id_kabupaten' selected>
																$cb->nama_kabupaten</option>";
																}
															}
															
															?>
												  </select>
													</div>
												</div>  
											</div>
										  </div>
										 <div class="form-group">
											<label class="col-md-2 control-label">Bulan</label>
											<div class="col-md-2">
												<select class="form-control selectpicker" data-live-search="true" name="tahun" required>
													 <option value="">- Tahun -</option>
													<?php
													$tahun = date("Y")+ 2;
													echo $tahun;
													for($i=2016;$i<=$tahun;$i++){
														echo "<option value='".$i."'>".$i."</option>";
													}
													?>
												</select>
											</div>
										  </div>
										  
											
											
											
										  <div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
											  <button type="submit" class="btn btn-info" >Cetak</button>
											  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
											</div>
										  </div>
										<?php echo form_close();?>
								</div>
                            </div>
                    </div>
                    <!-- /.panel -->
                </div>
                
            </div>

			
                     
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   