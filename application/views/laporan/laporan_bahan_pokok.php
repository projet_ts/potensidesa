<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Laporan Bahan Pokok</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("cetak_bahan_pokok/cetak", 
                            "class='form-horizontal' row-border")?> 
                      
					  <div class="form-group">
                        <label class="col-md-2 control-label">Pasar</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control selectpicker" data-live-search="true" name="id_pasar" required>
								
                                <!--<?php
								if($group_id=="group1000"){?>
									<option value="">
									- Pasar -
									</option>
									<?php
									foreach ($pasar as $cb) {
                                    echo "<option value='$cb->id_pasar'>
									$cb->nama_pasar</option>";
									}
								} else {
									foreach ($pasar as $cb) {
                                    echo "<option value='$cb->id_pasar' selected>
									$cb->nama_pasar</option>";
									}
								}
								
                                ?>-->
								<option value="">
									- Pasar -
								</option>
                                <?php
								foreach ($pasar as $cb) {
                                    echo "<option value='$cb->id_pasar'>
                                  $cb->nama_pasar</option>";
                                }
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
					 <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal</label>
                        <div class="col-md-2">
							<div class='input-group date' id='datetimepicker1'>
								<input class="form-control" type="text"  name="tanggal" placeholder="Tanggal" id="datetimepicker1" maxlength="100" required>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
					  </div>
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" >Cetak</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   