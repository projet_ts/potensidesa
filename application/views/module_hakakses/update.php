<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Hak Akses</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("hakakses/update_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <input type="hidden" name="group_id" value="<?php echo $entry->group_id?>">
					  <input type="hidden" name="menu_id2" value="<?php echo $entry->menu_id?>">
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Menu</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="menu_id">
                                <option>
                                  - Nama Menu -
                                </option>
                                <?php
                                foreach ($menus as $cb) {
									if($cb->menu_id==$entry->menu_id) {
										echo "<option value='$cb->menu_id' selected>
											$cb->menu_name</option>"; 
									} else {
										echo "<option value='$cb->menu_id'>
											$cb->menu_name</option>"; 
									}
                                    
                                }
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Create</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="c">
                                <?php 
                                if($entry->c=="Y") {
                                    echo "
                                        <option value='Y' selected> Ya</option>
                                        <option value='T'>Tidak</option>"
                                    ;
                                } else if($entry->c=="T") {
                                    echo "
                                        <option value='Y' > Ya</option>
                                        <option value='T' selected>Tidak</option>"
                                    ;
                                }
                                ?>   
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Read</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="r">
                                <?php 
                                if($entry->r=="Y") {
                                    echo "
                                        <option value='Y' selected> Ya</option>
                                        <option value='T'>Tidak</option>"
                                    ;
                                } else if($entry->r=="T") {
                                    echo "
                                        <option value='Y' > Ya</option>
                                        <option value='T' selected>Tidak</option>"
                                    ;
                                }
                                ?>   
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Update</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="u">
                                <?php 
                                if($entry->u=="Y") {
                                    echo "
                                        <option value='Y' selected> Ya</option>
                                        <option value='T'>Tidak</option>"
                                    ;
                                } else if($entry->u=="T") {
                                    echo "
                                        <option value='Y' > Ya</option>
                                        <option value='T' selected>Tidak</option>"
                                    ;
                                }
                                ?>   
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Delete</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="d">
                                <?php 
                                if($entry->d=="Y") {
                                    echo "
                                        <option value='Y' selected> Ya</option>
                                        <option value='T'>Tidak</option>"
                                    ;
                                } else if($entry->d=="T") {
                                    echo "
                                        <option value='Y' > Ya</option>
                                        <option value='T' selected>Tidak</option>"
                                    ;
                                }
                                ?>   
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Special</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="s">
                                <?php 
                                if($entry->s=="Y") {
                                    echo "
                                        <option value='Y' selected> Ya</option>
                                        <option value='T'>Tidak</option>"
                                    ;
                                } else if($entry->s=="T") {
                                    echo "
                                        <option value='Y' > Ya</option>
                                        <option value='T' selected>Tidak</option>"
                                    ;
                                }
                                ?>   
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm">Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/hakakses/data?id='.$entry->group_id, 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Group">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   