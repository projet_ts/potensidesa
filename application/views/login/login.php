<!DOCTYPE html>
<html>
<head>
    <title>Login Admin</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Blue Moon - Responsive Admin Dashboard" />
    <meta name="keywords" content="Notifications, Admin, Dashboard, Bootstrap3, Sass, transform, CSS3, HTML5, Web design, UI Design, Responsive Dashboard, Responsive Admin, Admin Theme, Best Admin UI, Bootstrap Theme, Wrapbootstrap, Bootstrap, bootstrap.gallery" />
    <meta name="author" content="Bootstrap Gallery" />
    <!--<link rel="shortcut icon" href="<?php echo base_url()."asset/";?>img/favicon.ico">-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	
    <link rel="shortcut icon" href="<?php echo base_url();?>images/kdi.png">

    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>css/login.css" rel="stylesheet">
    <!-- Important. For Theming change primary-color variable in main.css  -->

    <link href="<?php echo base_url();?>fonts/font-awesome.min.css" rel="stylesheet">
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-40304444-1', 'iamsrinu.com');
      ga('send', 'pageview');

    </script>
  </head>

  <body>
    <div id="wrapper">
    <?php echo form_open("login/login", "id='login' class='front box'");?>
      <div class="default"><img src="<?php echo base_url();?>images/kdi.png"><h4>WEBSITE BALITBANG</h4><p>PROV. SULAWESI TENGGARA</p></div>
          <input type="text" name="username" placeholder="username" />
          <input type="password" name="password" placeholder="password" />
          <button class="login"><i class="fa fa-check"></i></button>
    </form>
    </div>

    <script src="<?php echo base_url()."asset/";?>js/jquery.js"></script>
    <script src="<?php echo base_url()."asset/";?>js/bootstrap.min.js"></script>
    <script>

    </script>

  </body>
</html>
