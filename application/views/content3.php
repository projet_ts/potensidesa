<!--Latest News-->
<section id="news" class="bg_grey padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <h2 class="heading">Berita</h2>
      <hr class="heading_space">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="specialists_wrap_slider">
          <div id="news-slider" class="owl-carousel">		  
			<?php foreach($berita as $news){ ?>
            <div class="item">
              <div class="news_content">
               <img src="<?php echo base_url();?>files/image_struktur/<?php echo $news->gambar;?>" height="100px" width="100px" alt="image unavailable">
               <div class="date_comment">
                  <span></span>                  
               </div>
               <div class="comment_text">
                 <h3><a href="<?php echo base_url();?>home/berita/<?php echo $news->id_berita?>"><?php echo $news->judul;?></a></h3>
                 <p><?php $content = $news->content;
													$content=character_limiter($content,200);?>
													<?php echo $content;?></p>
				<div class='text-right'><a href="<?php echo base_url();?>home/berita/<?php echo $news->id_berita?>" class='btn btn-primary btn-noborder-radius hvr-bounce-to-bottom'>Read More</a></div>
				</div>
              </div>			  
            </div>
			<?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

