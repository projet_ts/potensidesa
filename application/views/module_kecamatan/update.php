<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Kecamatan</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">

        <?php echo form_open_multipart("kecamatan/update_data",
                            "class='form-horizontal' row-border")?>
                      <input type="hidden" name="kec_id" value="<?php echo $entry->kec_id?>">
					   <div class="form-group">
                        <label class="col-md-2 control-label">Nama Kecamatan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_kecamatan"
							value="<?php echo $entry->nama_kecamatan?>" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Keterangan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="keterangan"
							                   value="<?php echo $entry->keterangan?>"
                                 maxlength="100" required>
                        </div>
					   </div>

                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" title="Update Data">Update</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/kecamatan/data',
									'<button type="button" class="btn btn-success
										" data-toggle="tooltip" data-placement="top" title="Kembali Data kecamatan">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
