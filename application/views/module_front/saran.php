<script>
    $(document).ready(function(){
    $('#txtKeyboard').keyboard();
    $('#txtKeyboard1').keyboard();
    $('#txtKeyboard2').keyboard();
    
});
</script>

<script>
    var availableTags = ["ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure",
    "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript",
    "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme" ];

    $('#texti').keyboard({ layout: 'qwerty' }).autocomplete({source: availableTags
    })
    // position options added after v1.23.4
    .addAutocomplete({
            position : {
                    of : null,        // when null, element will default to kb.$keyboard
                    my : 'right top', // 'center top', (position under keyboard)
                    at : 'left top',  // 'center bottom',
                    collision: 'flip'
            }
    })
    .addTyping();
</script>

<section id="content"><div class="ic">More Website Templates @ TemplateMonster.com - July 28, 2014!</div>
  <div class="container">
    <div class="row">
      <div class="grid_12">
        <h2>Pengaduan :</h2>
        
        <?php echo form_open("Frontpage/create_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Pengadu</label>
                        <div class="col-md-6">
                            <input class="form-control" type="text" name="nama_pengadu" placeholder="Nama Pengadu" id="txtKeyboard" maxlength="100" required>
                        </div>
                        
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">No. Hp / Tlp</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="no_hp" id="txtKeyboard1"
							placeholder="No. Hp / Tlp">
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Isi Aduan</label>
                        <div class="col-md-6">
                            <textarea name="isi_aduan" class="textarea form-control" id="txtKeyboard2" required="required"></textarea>
                        </div>
                      </div>
			<div class="form-group">
                        <label class="col-md-2 control-label">Status</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <input name="status" type="hidden" value="Belum Dikonfirmasi">
                                
                                </div>
                            </div>  
                        </div>
                      </div>
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" >Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/Frontpage/index', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Pengaduan">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>  
      </div>
    </div>
  </div>
</section>