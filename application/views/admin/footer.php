<footer>
          <p>DATABASE POTENSI DESA KAB. KOLAKA TIMUR © By Techno's Studio</p>
        </footer>

      </div>
    </div>
    <!-- Main Container end -->


    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.scrollUp.js"></script>

	<!-- Alert -->
	<script src="<?php echo base_url();?>js/alertify.min.js"></script>

    <!-- Sparkline JS -->
    <script src="<?php echo base_url();?>js/sparkline.js"></script>

     <!-- Tiny Scrollbar JS -->
    <script src="<?php echo base_url();?>js/tiny-scrollbar.js"></script>

    <!-- Custom JS -->
    <script src="<?php echo base_url();?>js/menu.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>


<script>
  $(document).ready(function () {
    var mySelect = $('#first-disabled2');

    $('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    $('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    $('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>

	<script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
    <script>
		$(function() {
			$( "#datetimepicker1" ).datepicker({
				format:'yyyy-mm-dd'
			});
		});
	</script>

	<script>
		$(function() {
			$( "#datetimepicker2" ).datepicker({
				format:'yyyy-mm-dd'
			});
		});
	</script>

	<script>
		$(function() {
			$( "#datetimepicker3" ).datepicker({
				format:'yyyy-mm-dd'
			});
		});
	</script>

	<script>
		$(function() {
			$( "#datetimepicker4" ).datepicker({
				format:'yyyy-mm-dd'
			});
		});
	</script>

	<script>
		$(function() {
			var startDate,
			endDate;

      $('#weekpicker').datepicker({
        autoclose: true,
        format :'yyyy-mm-dd',
        forceParse :false
    }).on("changeDate", function(e) {
        //console.log(e.date);
        var date = e.date;
        startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
        endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay()+6);
        //$('#weekpicker').datepicker("setDate", startDate);
        $('#weekpicker').datepicker('update', startDate);
        $('#weekpicker').val(
		 startDate.getFullYear()
		+ '-' +
		(startDate.getMonth() + 1)
		+ '-' +
		startDate.getDate());
    });


        //new
        $('#prevWeek').click(function(e){
          var date = $('#weekpicker').datepicker('getDate');
          //dateFormat = "mm/dd/yy"; //$.datepicker._defaults.dateFormat;
          startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay()- 7);
          endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() - 1);
          $('#weekpicker').datepicker("setDate", new Date(startDate));
          $('#weekpicker').val((startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' +  startDate.getFullYear() + ' - ' + (endDate.getMonth() + 1) + '/' + endDate.getDate() + '/' +  endDate.getFullYear());

          return false;
        });
        $('#nextWeek').click(function(){
          var date = $('#weekpicker').datepicker('getDate');
          //dateFormat = "mm/dd/yy"; // $.datepicker._defaults.dateFormat;
          startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay()+ 7);
          endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 13);
          $('#weekpicker').datepicker("setDate", new Date(startDate));
          $('#weekpicker').val((startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' +  startDate.getFullYear() + ' - ' + (endDate.getMonth() + 1) + '/' + endDate.getDate() + '/' +  endDate.getFullYear());

          return false;
        });
		});
	</script>

    <script type="text/javascript">

      //ScrollUp
      $(function () {
        $.scrollUp({
          scrollName: 'scrollUp', // Element ID
          topDistance: '300', // Distance from top before showing element (px)
          topSpeed: 300, // Speed back to top (ms)
          animation: 'fade', // Fade, slide, none
          animationInSpeed: 400, // Animation in speed (ms)
          animationOutSpeed: 400, // Animation out speed (ms)
          scrollText: 'Top', // Text for element
          activeOverlay: false // Set CSS color to display scrollUp active point, e.g '#00FFFF'
        });
      });

      //Tooltip
      $('a').tooltip('hide');

      //Popover
      $('.popover-pop').popover('hide');

      //Dropdown
      $('.dropdown-toggle').dropdown();

    </script>
	<script type="text/javascript">
var editor = CKEDITOR.replace( 'editor1', {
    filebrowserBrowseUrl : '<?php echo base_url();?>asset/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl : '<?php echo base_url();?>asset/ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl : '<?php echo base_url();?>asset/ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl : '<?php echo base_url();?>asset/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl : '<?php echo base_url();?>asset/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl : '<?php echo base_url();?>asset/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
});
CKFinder.setupCKEditor( editor, '../' );
</script>
  </body>
</html>
