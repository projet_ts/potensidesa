<!-- Main Container start -->
<!-- Header Start -->
    <header>
        <a href="<?php echo base_url()?>" class="logo">
        <img src="<?php echo base_url();?>images/logo.png" alt="Logo"/><span id="logo-spi">POTENSI DESA</span>
      </a>
      <div class="pull-right">
        <ul id="mini-nav" class="clearfix">
          <li class="list-box hidden-xs">
            <a href="#" data-toggle="modal" data-target="#modalMd">
              <span class="text-white"><?php// echo $nama_user?></span>
            </a>
            <!-- Modal -->
            <div class="modal fade" id="modalMd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-danger" id="myModalLabel5">POTENSI DESA KAB. KOLAKA TIMUR</h4>
                  </div>
                  <div class="modal-body">
                   POTENSI DESA KAB. KOLAKA TIMUR
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                  </div>
                </div>
              </div>
            </div>
          </li>
		  <?php
			$photo = $this->session->userdata('photo');
			if ($photo == "" || $photo == " " || $photo == null) {
				$photo = "profile.png";
			}
		  ?>
          <li class="list-box user-profile">
            <a id="drop7" href="#" role="button" class="dropdown-toggle user-avtar" data-toggle="dropdown">
              <img src="<?php echo base_url()."images/admin.png"?>" alt="Admin">
            </a>
            <ul class="dropdown-menu server-activity">
              <li>
				  <?php
					echo anchor('home', "<p><i class='fa fa-cog text-info'></i><span class='text-info'>Profil Pengguna</span></p>");
				  ?>
              </li>
              <li>
                <div class="demo-btn-group clearfix">
                    <?php
                        echo anchor('login/logout', "<button class='btn btn-danger'>
                    Logout
                  </button>");
                    ?>

                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </header>
    <!-- Header End -->
