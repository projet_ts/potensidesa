<style>
		#chart
		{
			z-index:-10;
		}
		
</style>

	
	<div id="chart"></div>


<script type="text/javascript" src="asset/highcharts/jquery.min.js"></script>
<script type="text/javascript" src="asset/highcharts/highcharts.js"></script>
<script type="text/javascript" src="asset/highcharts/modules/exporting.js"></script>
<script type="text/javascript" src="asset/highcharts/themes/skies.js"></script>
<script type="text/javascript">
jQuery(function(){
	new Highcharts.Chart({
		chart: {
			renderTo: 'chart',
			type: 'line',
		},
		title: {
			text: 'Pendapatan  2014',
			x: -20
		},
		subtitle: {
			text: 'Total uang yang didapat dalam tiap bulan',
			x: -20
		},
		xAxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun',
                    'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des']
		},
		yAxis: {
			title: {
				text: 'Pendapatan (Ribu)'
			}
		},
		series: [{
			name: 'Pendapatan (Ribu)',
			data: <?php echo json_encode($grafik); ?>
		}]
	});
}); 
</script>

</body>
</html>