<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">

  <!-- Left Sidebar Start -->
  <div class="center-sidebar">
      <!-- Row Start -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget">
              <div class="widget-header">
                <div class="title">
                  Selamat Datang Di Aplikasi Perindustrian dan Perdagangan Prov. Sulawesi Tenggara
                </div>
              </div>
              <div class="widget-body">
                <div class="panel panel-default no-margin">
                  <div class="panel-body">
                    <div class="col-lg-3 col-md-3 col-sm-6">
						 <div class="mini-widget">
							<div class="mini-widget-heading clearfix">
							  <div class="pull-left">Pasar</div>
							</div>
							<div class="mini-widget-body clearfix">
							  <div class="pull-left">
								<i class="fa fa-building-o"></i>
							  </div>
							  <div class="pull-right number">
								<?php
									echo $pasar;
								?>
							  </div>
							</div>
						  </div> 
						  
						<div class="mini-widget mini-widget-yellow">
							<div class="mini-widget-heading clearfix">
							  <div class="pull-left">Pelabuhan</div>
							</div>
							<div class="mini-widget-body clearfix">
							  <div class="pull-left">
								<i class="fa fa-bullhorn"></i>
							  </div>
							  <div class="pull-right number">
								<?php
									echo $pelabuhan;
								?>
							  </div>
							</div>
							
						  </div>
					</div>
					
					<div class="col-lg-3 col-md-3 col-sm-6">
						<div class="mini-widget">
							<div class="mini-widget-heading clearfix">
							  <div class="pull-left">User</div>
							</div>
							<div class="mini-widget-body clearfix">
							  <div class="pull-left">
								<i class="fa fa-user"></i>
							  </div>
							  <div class="pull-right number">
								<?php
									echo $user;
								?>
							  </div>
							</div>
						  </div>
						  <div class="mini-widget mini-widget-green">
							<div class="mini-widget-heading clearfix">
							  <div class="pull-left">Komoditi</div>
							</div>
							<div class="mini-widget-body clearfix">
							  <div class="pull-left">
								<i class="fa fa-check"></i>
							  </div>
							  <div class="pull-right number">
								<?php
									echo $komoditi;
								?>
							  </div>
							</div>
						  </div> 
					</div>
					
					 
						
						
						
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Row End -->
      
  </div>


</div>