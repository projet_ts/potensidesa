<!--Page header & Title-->
<section id="page_header2">
<div class="page_title2">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Galeri</h2>
         <div class="page_link"><a href="<?php echo base_url();?>home/beranda">Beranda</a><span><i class="fa fa-long-arrow-right"></i>Galeri</span></div>
      </div>
    </div>
  </div>
</div>  
</section>

<section id="gallery" class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="work-filter">
          <ul class="text-center">
             <li><a href="javascript:;" data-filter="all" class="active filter">Semua Berkas</a></li>
            <li><a href="javascript:;" data-filter=".technolog" class="filter">Foto</a></li>
			<li><a href="javascript:;" data-filter=".facilities" class="filter">Video </a></li>
             
          </ul>
        </div>
      </div>
    </div>
     <div class="row">
      <div class="zerogrid">
        <div class="wrap-container">
          <div class="wrap-content clearfix">
		  
            <!--video-->
			<?php foreach($video as $vid){ ?>
			<div class="col-1-4 mix work-item facilities heading_space">
              <div class="wrap-col">
                <div class="item-container">
                  <div class="image">
                    <img src="<?php echo base_url();?>files/image_struktur/<?php echo $vid->gambar;?>" width="70px" height="100px" alt="work"/>
                    <div class="overlay">
                      <div class="overlay-inner">                        
						<a class="video fancybox.iframe icon" href="<?php echo $vid->link;?>?fs=1&amp;autoplay=1"><i class="fa fa-play"></i></a>
                      </div>
                    </div>
                  </div> 
                  <div class="gallery_content">
                    <h3><?php echo $vid->judul;?></h3>
                    <p><?php echo $vid->deskripsi;?></p>
                  </div>
                </div>
              </div>
            </div>
			<?php } ?>
			
			<!--foto-->
			<?php foreach($foto as $img){ ?>
            <div class="col-1-4 mix work-item technolog heading_space">
              <div class="wrap-col">
                <div class="item-container">
                  <div class="image">
                    <img src="<?php echo base_url();?>files/image_struktur/<?php echo $img->gambar;?>" alt="work"/>
                    <div class="overlay">
                      <div class="overlay-inner">
                        <a class="fancybox icon" href="<?php echo base_url();?>files/image_struktur/<?php echo $img->gambar;?>" width="70px" height="100px" data-fancybox-group="gallery"><i class="fa fa-eye"></i></a>
                      </div>
                    </div>
                  </div>
                  <div class="gallery_content">
                    <h3><?php echo $img->judul;?></h3>
                  </div> 
                </div>
              </div>
            </div>
			<?php } ?>

            
          </div>
        </div>
       </div>
      </div>
  </div>
</section>
