<!--Page header & Title-->
<section id="page_header">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Litbang</h2>
         <div class="page_link"><a href="<?php echo base_url();?>home/beranda">Beranda</a><span><i class="fa fa-long-arrow-right"></i>Litbang</span></div>
      </div>
    </div>
  </div>
</div>  
</section>


<!-- Blogs -->
<section id="blog" class="padding-top">
  <div class="container">
    <div class="row">
	<div class="col-md-4 col-sm-5">
        <aside class="sidebar">
           <div class="widget">
             <h3>Kategori</h3>
             <ul class="widget_links">
				<?php
					 foreach ($jenisjurnal as $cb) {
                                
				?>
					<li><a href="<?php echo base_url().'home/litbang2/'.$cb->id_jenisjurnal;?>">
					<?php echo $cb->nama_jenisjurnal?></a></li>
               <?php
					}
			   ?>
               </ul>
           </div>
           </aside>
    </div>
      <div class="col-md-8 col-sm-7">
		
		<?php 
		$no=1;
		foreach($lit as $inf){ ?>
        <div class="blog_item padding-bottom">
           <h2><?php echo $inf->judul;?></h2>
           <ul class="comments">
             <li><a href="#"><i class="fa fa-book"></i><?php echo $inf->edisi;?></a></li>
             <li><a href="#"><i class="fa fa-users"></i><?php echo $inf->penyusun;?></a></li>
			 
           </ul>
          
          <p><?php $content = $inf->abstrak;
			$content=character_limiter($content,600);?>
			<?php echo $content;?></p>
          
		  <!-- Trigger/Open The Modal -->
		<button class="btn-common button3" data-toggle="modal" data-target="#myModal<?php echo $no;?>">Review</button>
		<br><br>
		  <div id="myModal<?php echo $no;?>" class="modal fade" role="dialog">
			<div class="modal-dialog ">
			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
			  
			  <br>
			  <h2><?php echo $inf->judul;?></h2>
			  <br>
			</div>	
			<div class="modal-body">
																
			<br>
			Penyusun : <?php echo $inf->penyusun;?>
		<br>
		<br>
			Edisi : <?php echo $inf->edisi;?>
		<br>
		<br>
			Kata Kunci : <?php echo $inf->kata_kunci;?>
		<br><br>
		<b>ABSTRAK</b>
		<br>
			<?php echo $inf->abstrak;?>
		<br>	
		<br>
		<h3><a href="<?php echo base_url()."files/uploads/".$inf->download?>" target="_blank">Download</a></h3>
		
		<br>	
		<br>
																		
			</div>
			<div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
			</div>

			</div>
		</div>
		  
		  <?php $no++; } ?>
        
      </div>
	  
    </div>
  </div>
</section>
