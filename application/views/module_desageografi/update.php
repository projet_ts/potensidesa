<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Data Desa Geografi</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">

                      <?php echo form_open("desageografi/update_data",
                            "class='form-horizontal' row-border")?>

                      <input type="hidden" name="desageografi_id" value="<?php echo $entry->desageografi_id?>">
                      <input type="hidden" name="desa_id" value="<?php echo $entry->desa_id?>">

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Tahun</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="tahun" value="<?php echo $entry->tahun?>"
        							 maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Ketinggian</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="ketinggian" value="<?php echo $entry->ketinggian?>"
                       maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Curah Hujan</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="curah_hujan" value="<?php echo $entry->curah_hujan?>"
                       maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Topografi</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="topografi" value="<?php echo $entry->topografi?>"
                       maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Iklim</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="iklim" value="<?php echo $entry->iklim?>"
                       maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm">Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/desageografi/data?id='.$entry->desa_id,
									'<button type="button" class="btn btn-success
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Menu">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
