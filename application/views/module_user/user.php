<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data User</a>
                    </div>
                  </div>
                  <div class="widget-body">
					<div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									<?php echo anchor('/user/insert', 
									'<button type="button" class="btn btn-info 
										" data-toggle="tooltip" data-placement="top" title="Tambah Data User">
											Tambah</button>'); ?>
									<?php echo anchor('/user/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Refresh Data User">
											Refresh</button>'); ?>
								</div>
							</div>
							
							
							<?php echo form_open("user/result");?>
							
							<div class="col-md-3 col-sm-4 col-xs-4">
								<input type="text" class="form-control" name="key" placeholder="Cari">
							</div>
							
							<div class="btn-group">
								<button type="submit" class="btn btn-info" title="Cari Data User">
									<i class="fa fa-search"></i>
								</button>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
                      
					  
                    <div class="clearfix">
                    </div>  
                    <div id="dt_example" class="example_alt_pagination">
                        <br>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left" >
                        
                        <thead>
                          <tr>
                            <th style="width:5%">
                              No
                            </th>  
                            <th style="width:20%">
                              Username
                            </th>
                            <th style="width:20%">
                              Nama Lengkap
                            </th>
                            <th style="width:20%">
                              Group
                            </th>
							<th style="width:20%">
                              Instansi
                            </th>
                            <th style="width:20%">
                              Aktif
                            </th>
                            <th style="width:5%">
                              Pilihan
                            </th>
                            
                          </tr>
                        </thead>
                        <tbody>
                        <?php
							
                            $no = $number + 1;
                            foreach ($user as $v) {


                        ?>    
                          <tr class="gradeA success">
                            <td>
                              <?php echo $no ?>
                            </td>  
                            <td>
                              <?php echo $v->username ?>
                            </td>
                            <td>
                              <?php echo $v->fullname ?>
                            </td>
                            <td>
                              <?php echo $v->group_name?>
                            </td>
							<td>
                              <?php echo $v->nama_instansi?>
                            </td>
                            <td>
                              <?php if($v->active=='Y') {
                                  echo "Ya";
                              } else {
                                  echo "Tidak";
                              } ?>
                            </td>
                            <td class="hidden-xs">
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Pilihan 
                                  <span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu">
                                <li><?php echo anchor("/user/detail?id=".$v->userid, "Detail"); ?></li>
                                <li><?php echo anchor("/user/update?id=".$v->userid, "Edit"); ?></li>
                                <li class="divider"></li>
                                <li><?php echo anchor("/user/delete?id=".$v->userid, "Hapus", 
									"onclick=\"return confirm('Anda Yakin Akan Menghapus?')\"");
                                ?></li>
                              </ul>
                            </div>
                          </td>
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
						<div align="right"><?php echo $links?> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   

 