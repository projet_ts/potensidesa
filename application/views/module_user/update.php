<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit User</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("user/update_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <input type="hidden" name="userid" value="<?php echo $entry->userid?>">
					  <input type="hidden" name="oldpassword" value="<?php echo $entry->password?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Username</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="username" 
                                 value="<?php echo $entry->username?>"  maxlength="100" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Nama Lengkap</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="fullname" 
                                 value="<?php echo $entry->fullname?>"  maxlength="100" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">email</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="email" 
                                 value="<?php echo $entry->email?>"  maxlength="100" required>
                        </div>
                      </div>
					  
                      <div class="form-group">
                        <label class="col-md-2 control-label">Waktu Pembuatan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="description" 
                                 value="<?php echo $entry->register_date."(".$entry->register_time.")" ?>" readonly="">
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Group</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="group_id">
								
								<?php
								if($group_id=="group1000"){?>
									 <option>
                                  - Nama Group -
                                </option>
                                <?php
                                foreach ($group as $cb) {
									if($cb->group_id == $entry->group_id) {
										echo "<option value='$cb->group_id' selected>
										$cb->group_name</option>"; 
									} else {
										echo "<option value='$cb->group_id'>
										$cb->group_name</option>"; 
									}
                                }
								} else {
									foreach ($group as $cb) {
                                    echo "<option value='$cb->group_id' selected>
									$cb->group_name</option>";
									}
								}
								
                                ?>
								
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Nama Instansi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_instansi" 
							placeholder="Nama Instansi" maxlength="100" value="<?php echo $entry->nama_instansi?>" required>
                        </div>
                      </div>
					  <!--<div class="form-group">
                        <label class="col-sm-2 control-label">Nama Instansi</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-10 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="kode_instansi">
                                <option>
                                  - Nama Instansi -
                                </option>
                                <?php
                                foreach ($instansi as $ca) {
									if($ca->kode_instansi == $entry->kode_instansi) {
										echo "<option value='$ca->kode_instansi' selected>
										$ca->nama_instansi</option>"; 
									} else {
										echo "<option value='$ca->kode_instansi'>
										$ca->nama_instansi</option>"; 
									}
                                }
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>-->
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Kabupaten/Kota</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control selectpicker" data-live-search="true" name="id_kabupaten" required>
                                <?php
								if($group_id=="group1000"){?>
									<option value="">
									- Kabupaten -
									</option>
									<?php
									foreach ($kabupaten as $cb) {
                                    if($cb->id_kabupaten == $entry->id_kabupaten) {
										echo "<option value='$cb->id_kabupaten' selected>
										$cb->nama_kabupaten</option>"; 
									} else {
										echo "<option value='$cb->id_kabupaten'>
										$cb->nama_kabupaten</option>"; 
									}
									}
								} else {
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten' selected>
									$cb->nama_kabupaten</option>";
									}
								}
								
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Aktif</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <select id="active" class="form-control" name="active">
                                <?php 
                                if($entry->active=="Y") {
                                    echo "
                                        <option value='Y' selected> Ya</option>
                                        <option value='T'>Tidak</option>"
                                    ;
                                } else if($entry->active=="T") {
                                    echo "
                                        <option value='Y' > Ya</option>
                                        <option value='T' selected>Tidak</option>"
                                    ;
                                }
                                ?>    
                                
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-4 control-label">GANTI PASSWORD *) Jika Diperlukan</label>
                        
                      </div>
					  
					  <div class="form-group">
                        <label class="col-md-2 control-label">Password Lama</label>
                        <div class="col-md-6">
                          <input class="form-control" type="hidden" name="oldpassword" value=<?php echo $entry->password?> /> 
                          <input class="form-control" type="password" name="checkoldpassword" 
                                 value="" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Password Baru</label>
                        <div class="col-md-6">
                          <input class="form-control" type="password" name="password" 
                                 value="">
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Password Lagi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="password" name="matchpassword" 
                                 value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" title="Update Data">Update</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/user/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data User">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   