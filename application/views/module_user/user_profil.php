<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      
	  <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget no-margin">
                  <div class="widget-header">
                    <div class="title">
                      Edit Profile
                    </div>
                    <span class="tools">
                      <i class="fa fa-cogs"></i>
                    </span>
                  </div>
                  <div class="widget-body">
				  
				  <?php echo form_open_multipart("user_profil/update_data", 
                            "class='form-horizontal' row-border name='form_user_profil' 
                            onsubmit=\"return validateUser()\"")?> 
                      
                      <input type="hidden" name="userid" value="<?php echo $entry->userid?>">
					  <input type="hidden" name="group_id" value="<?php echo $entry->group_id?>">
					  <input type="hidden" name="oldpassword" value="<?php echo $entry->password?>">
					  <input type="hidden" name="photo" value="<?php echo $entry->photo?>">
				  
                    <div class="row">
                      <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                        <div class="thumbnail">
						  <br>
                          <img alt="300x200" src="<?php echo base_url()."asset/img/".$entry->photo?>">
						  <br>
						  <h4 class="center-align-text"><?php echo $entry->photo?></h4>
						  <br>
						  <input type="file" name="userfile" >
						  <br>
                        </div>
                        <br>
                      </div>
                      <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                        <div class="form-horizontal">
						  <h5>
                            Informasi Personal
                          </h5>
                          <hr>
                          <div class="form-group">
                            <label for="username" class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-9">
                              <input class="form-control" type="text" name="username" 
                                 value="<?php echo $entry->username?>"  maxlength="100" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="fullname" class="col-sm-3 control-label">Nama Lengkap</label>
                            <div class="col-sm-9">
                              <input class="form-control" type="text" name="fullname" 
                                 value="<?php echo $entry->fullname?>"  maxlength="100" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                              <input class="form-control" type="text" name="email" 
                                 value="<?php echo $entry->email?>"  maxlength="100" required>
                            </div>
                          </div>
						  
						  <br />	
						  
                          <h5>
                            Informasi Login
                          </h5>
                          <hr>
						  <div class="form-group">
                            <label class="col-sm-5 control-label text-info">GANTI PASSWORD *) Jika Diperlukan</label>
                          </div>
                          <div class="form-group">
                            <label for="checkoldpassword" class="col-sm-3 control-label">Password Lama</label>
                            <div class="col-sm-9">
                              <input class="form-control" type="password" name="checkoldpassword" 
                                 value="">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="password" class="col-sm-3 control-label">Password Baru</label>
                            <div class="col-sm-9">
                              <input class="form-control" type="password" name="password" 
                                 value="">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="matchpassword" class="col-sm-3 control-label">Password Lagi</label>
                            <div class="col-sm-9">
                              <input class="form-control" type="password" name="matchpassword" 
                                 value="">
                            </div>
                          </div>
						  <hr>
						  <div class="form-group">
                            <label class="col-sm-3 control-label"></label>
                            <div class="col-sm-9">
							<div class="form-inline">
                              <button type="submit" class="btn btn-info" title="Update Data">Update</button> &nbsp;&nbsp;&nbsp;
							  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
							</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
					<?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
	  
     </div>       
</div>   