<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail User</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("", 
                            "class='form-horizontal' row-border")?> 
                      
                      <input type="hidden" name="userid" value="<?php echo $entry->userid?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Username</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="username" 
                                 value="<?php echo $entry->username?>" readonly="">
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Nama Lengkap</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="fullname" 
                                 value="<?php echo $entry->fullname?>"  maxlength="100" readonly="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">email</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="email" 
                                 value="<?php echo $entry->email?>" readonly="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Waktu Pembuatan User</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="description" 
                                 value="<?php echo $entry->register_date."(".$entry->register_time.")" ?>" readonly="">
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Group</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="group_id" disabled>
                                <option>
                                  - Nama Group -
                                </option>
                                <?php
                                foreach ($group as $cb) {
									if($cb->group_id == $entry->group_id) {
										echo "<option value='$cb->group_id' selected>
										$cb->group_name</option>"; 
									} else {
										echo "<option value='$cb->group_id'>
										$cb->group_name</option>"; 
									}
                                }
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Instansi</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-10 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="kode_instansi" disabled>
                                <option>
                                  - Nama Instansi -
                                </option>
                                <?php
                                foreach ($instansi as $cb) {
									if($cb->kode_instansi == $entry->kode_instansi) {
										echo "<option value='$cb->kode_instansi' selected>
										$cb->nama_instansi</option>"; 
									} else {
										echo "<option value='$cb->kode_instansi'>
										$cb->nama_instansi</option>"; 
									}
                                }
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Aktif</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <select id="active" class="form-control" name="active" disabled="">
                                <?php 
                                if($entry->active=="Y") {
                                    echo "
                                        <option value='Y' selected> Ya</option>
                                        <option value='T'>Tidak</option>"
                                    ;
                                } else if($entry->active=="T") {
                                    echo "
                                        <option value='Y' > Ya</option>
                                        <option value='T' selected>Tidak</option>"
                                    ;
                                }
                                ?>    
                                
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <?php echo anchor('/user/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data User">
											Kembali</button>'); ?>
                        </div>
					   </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   