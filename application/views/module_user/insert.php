<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah User</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-rocket"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("user/create_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama User</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="username" 
							placeholder="Username" maxlength="100" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Nama Lengkap</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="fullname" 
							placeholder="Nama Lengkap" maxlength="100" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Password</label>
                        <div class="col-md-6">
                          <input class="form-control" type="password" name="password" 
							placeholder="Password" maxlength="100" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Email</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="email" 
							placeholder="Email" maxlength="100" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Group</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="group_id" required>
								<?php
								if($group_id=="group1000"){?>
									<option>
                                  - Nama Group -
									</option>
									<?php
									foreach ($group as $cb) {
										echo "<option value='$cb->group_id'>
									  $cb->group_name</option>"; 
									}
								} else {
									foreach ($group as $cb) {
									  echo "<option value='$cb->group_id'>
									  $cb->group_name</option>"; 
									}
								}
								
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Nama Instansi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_instansi" 
							placeholder="Nama Instansi" maxlength="100" required>
                        </div>
                      </div>
					  <!--<div class="form-group">
                        <label class="col-sm-2 control-label">Nama Instansi</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-10 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="kode_instansi">
                                <option>
                                  - Nama Instansi -
                                </option>
                                <?php
                                foreach ($instansi as $cb) {
                                    echo "<option value='$cb->kode_instansi'>
                                  $cb->nama_instansi</option>"; 
                                }
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>-->
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Kabupaten/Kota</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active"  class="form-control selectpicker" data-live-search="true" name="id_kabupaten" required>
                                <?php
								if($group_id=="group1000"){?>
									<option value="">
									- Kabupaten -
									</option>
									<?php
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten'>
									$cb->nama_kabupaten</option>";
									}
								} else {
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten' selected>
									$cb->nama_kabupaten</option>";
									}
								}
								
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Aktif</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="active">
                                <option value="Y">
                                  Ya
                                </option>
                                <option value="T">
                                  Tidak
                                </option>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm" title="Simpan Data">Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/user/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data User">
											Kembali</button>'); ?>
                        </div>
						
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   