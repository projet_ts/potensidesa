<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah Data Kelembagaan</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-rocket"></i>
                    </span>
                  </div>
                  <div class="widget-body">

                      <?php echo form_open("kelembagaan/create_data",
                            "class='form-horizontal' row-border")?>
                      <input type="hidden" name="desa_id" value="<?php echo $id?>">
                      <div class="form-group">
                        <!-- <label class="col-sm-2 control-label">Id kelembagaan</label> -->
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="hidden" name="kelembagaan_id" placeholder="Tahun" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Lembaga</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="nama_kelembagaan" placeholder="Nama Kelembagaan" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Lembaga</label>
                        <div class="col-sm-10">
                            <div class="row">
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                <select name="jenis_kelembagaan" class="form-control" >
                                    <option value="adat">Adat</option>
                                    <option value="politik">Politik</option>
                                    <option value="pendidikan">Pendidikan</option>
                                    <option value="hukum">Hukum</option>
                                </select>
                            </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="alamat" placeholder="Alamat"  required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Tahun</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="tahun" placeholder="Tahun" maxlength="4" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm">Simpan</button>
            						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
            						  <?php echo anchor('/kelembagaan/data?id='.$id,
            									'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Kembali Data Hak Akses">
            											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
