<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail Lembaga</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">

                      <?php echo form_open("",
                            "class='form-horizontal' row-border")?>

                      <input type="hidden" name="kelembagaan_id" value="<?php echo $entry->kelembagaan_id?>">
                      <div class="form-group">
                        <!-- <label class="col-sm-2 control-label">Id kelembagaan</label> -->
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="hidden" name="kelembagaan_id" value="<?php echo $entry->kelembagaan_id?>"  required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Lembaga</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="nama_kelembagaan" value="<?php echo $entry->nama_kelembagaan?>"  disabled>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Lembaga</label>
                        <div class="col-sm-10">
                            <div class="row">
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                <select name="jenis_kelembagaan" class="form-control" disabled>
                                  <?php
                                    if ($entry->jenis_kelembagaan== "adat"){
                                      echo '<option value="adat" selected>adat</option>
                                            <option value="politik">Politik</option>
                                            <option value="pendidikan">Pendidikan</option>
                                            <option value="hukum">Hukum</option>';
                                    }else if ($entry->jenis_kelembagaan== "politik"){
                                      echo '<option value="adat" >adat</option>
                                            <option value="politik" selected>Politik</option>
                                            <option value="pendidikan">Pendidikan</option>
                                            <option value="hukum">Hukum</option>';
                                    }else if ($entry->jenis_kelembagaan== "pendidikan"){
                                      echo '<option value="adat" >adat</option>
                                            <option value="politik" >Politik</option>
                                            <option value="pendidikan" selected>Pendidikan</option>
                                            <option value="hukum">Hukum</option>';
                                    }else {
                                      echo '<option value="adat" >adat</option>
                                            <option value="politik" >Politik</option>
                                            <option value="pendidikan" >Pendidikan</option>
                                            <option value="hukum" selected>Hukum</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Tahun</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="tahun" value="<?php echo $entry->tahun?>" maxlength="4" disabled>
                                </div>
                            </div>
                        </div>
                      </div>


                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
            						  <?php echo anchor('/kelembagaan/data?id='.$entry->desa_id,
            									'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Kembali Data Group">Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
