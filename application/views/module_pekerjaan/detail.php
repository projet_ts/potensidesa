<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Penduduk</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">

                      <?php echo form_open("",
                            "class='form-horizontal' row-border")?>

                      <input type="hidden" name="penduduk_id" value="<?php echo $entry->penduduk_id?>">
                      <div class="form-group">
                        <!-- <label class="col-sm-2 control-label">Id Penduduk</label> -->
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="hidden" name="penduduk_id" value="<?php echo $entry->penduduk_id?>" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Jumlah</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="jumlah" value="<?php echo $entry->jumlah?>" maxlength="100" disabled>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Kelamin</label>
                        <div class="col-sm-10">
                            <div class="row">
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                <select name="kelamin" class="form-control" disabled>
                                  <?php
                                    if ($entry->kelamin== "pria"){
                                      echo '<option value="pria" selected>Pria</option>
                                            <option value="wanita">Wanita</option>';
                                    }else{
                                      echo '<option value="pria" >Pria</option>
                                            <option value="wanita" selected>Wanita</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Tahun</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="tahun" value="<?php echo $entry->tahun?>" maxlength="4" disabled>
                                </div>
                            </div>
                        </div>
                      </div>


                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
            						  <?php echo anchor('/penduduk/data?id='.$entry->desa_id,
            									'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Kembali Data Group">Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
