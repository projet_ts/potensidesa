<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah Jam Kerja</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("jamker/create_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">Senin</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="senin" 
							placeholder="senin" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Selasa</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="selasa" 
							placeholder="selasa" maxlength="100" required>
                        </div>
					   </div>
					 <div class="form-group">
                        <label class="col-md-2 control-label">Rabu</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="rabu" 
							placeholder="rabu" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Kamis</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="kamis" 
							placeholder="kamis" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Jumat</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="jumat" 
							placeholder="jumat" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Sabtu</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="sabtu" 
							placeholder="sabtu" maxlength="100" required>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Minggu</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="minggu" 
							placeholder="minggu" maxlength="100" required>
                        </div>
					   </div>
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" >Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/jamker/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali ke Data Jamker">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   