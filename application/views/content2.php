<!-- Specialists -->
<section id="specialists" class="bg_grey padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">      
		<h2 class="heading">Jadwal Dokter</h2>
		<hr class="heading_space">
     <table class="table table-inverse table-striped table-bordered">
	 </div>
  <thead>
    <tr>
      <th>Nama Dokter</th>
      <th>Senin</th>
      <th>Selasa</th>
      <th>Rabu</th>
	  <th>Kamis</th>
	  <th>Jumat</th>
	  <th>Sabtu</th>
    </tr>
  </thead>
  <tbody>
	<?php foreach($jadwal as $jad){ ?>
    <tr>
      <th scope="row"><?php echo $jad->nama_dokter;?></th>
      <td><?php echo $jad->senin;?></td>
      <td><?php echo $jad->selasa;?></td>
      <td><?php echo $jad->rabu;?></td>
	  <td><?php echo $jad->kamis;?></td>
      <td><?php echo $jad->jumat;?></td>
      <td><?php echo $jad->sabtu;?></td>
    </tr>
    <?php } ?>

  </tbody>
</table>
  </div>
   </div>
</section>



<!-- image with content -->
<section class="info_section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 col-sm-4">
      </div>
      <div class="col-md-6 col-sm-8 right_box">
        <div class="right_box_inner padding clearfix">
          <div class="col-md-6 col-sm-6 white_content">
            <i class="fa fa-medkit"></i>
            <h3><a href="services.html">Operation Theater</a></h3>
            <p>Duis autem vel eum iriure dolor in hend rerit in vulputate velit esse molestie vel illum dolore nulla facilisis.</p>
          </div>
          <div class="col-md-6 col-sm-6 white_content">
            <i class="fa fa-hospital-o"></i>
            <h3><a href="services.html">Operation Theater</a></h3>
            <p>Duis autem vel eum iriure dolor in hend rerit in vulputate velit esse molestie vel illum dolore nulla facilisis.</p>
          </div>
          <div class="col-md-6 col-sm-6 white_content">
            <i class="fa fa-user-md"></i>
            <h3><a href="services.html">Operation Theater</a></h3>
            <p>Duis autem vel eum iriure dolor in hend rerit in vulputate velit esse molestie vel illum dolore nulla facilisis.</p>
          </div>
          <div class="col-md-6 col-sm-6 white_content">
            <i class="fa fa-ambulance"></i>
            <h3><a href="services.html">Operation Theater</a></h3>
            <p>Duis autem vel eum iriure dolor in hend rerit in vulputate velit esse molestie vel illum dolore nulla facilisis.</p>
          </div> 
        </div>
      </div>
    </div>
  </div>
</section>
