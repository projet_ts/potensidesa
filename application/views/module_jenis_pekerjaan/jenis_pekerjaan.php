<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data Jenis Pekerjaan</a>
                    </div>
                  </div>
                  <div class="widget-body">
                      <div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									<?php echo anchor('/jenis_pekerjaan/insert?id='.$id,
									'<button type="button" class="btn btn-info
										" data-toggle="tooltip" data-placement="top" title="Tambah Data Jenis Pekerjaan">
											Tambah</button>'); ?>
									<?php echo anchor('/jenis_pekerjaan/data?jenis_pekerjaan_id='.$id,
									'<button type="button" class="btn btn-success
										" data-toggle="tooltip" data-placement="top" title="Refresh Data Geografi">
											Refresh</button>'); ?>
									<?php echo anchor('/pekerjaan/data',
									'<button type="button" class="btn btn-warning
										" data-toggle="tooltip" data-placement="top" title="Kembali">
											Kembali</button>'); ?>
								</div>
							</div>

							<?php echo form_open("jenis_pekerjaan/result");?>

							<div class="col-md-3 col-sm-4 col-xs-4">
								<input type="hidden" name="id" value="<?php echo $id?>">
								<input type="text" class="form-control" name="key" placeholder="Cari">
							</div>

							<div class="btn-group">
								<button type="submit" class="btn btn-info" title="Cari Data Submenu">
									<i class="fa fa-search"></i>
								</button>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
                    <div class="clearfix">
                    </div>

                    <div id="dt_example" class="example_alt_pagination">
                        <br>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left">

                        <thead>
                          <tr>
                            <th style="width:5%">
                              No
                            </th>
                            <th style="">
                              Jenis Pekerjaan
                            </th>
                            <th style="width:15%">
                              Pilihan
                            </th>

                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no = $number + 1;
                            foreach ($jenis_pekerjaan as $v) {
                        ?>
                          <tr class="gradeA success">
                            <td>
                              <?php echo $no ?>
                            </td>
                            <td>
                              <?php echo $v->jenis_pekerjaan ?>
                            </td>

                            <td class="hidden-xs">
                            <div class="btn-group">
                            <?php echo anchor('jenis_pekerjaan/update?jenis_pekerjaan_id='.$v->jenis_pekerjaan_id, '<button class="btn btn-primary">Edit</button>'); ?>
                            <!-- <li class="divider"></li> -->
                            <?php echo anchor('jenis_pekerjaan/delete?jenis_pekerjaan_id='.$v->jenis_pekerjaan_id, '<button class="btn btn-danger">Hapus</button.'  , "onclick=\"return confirm('Anda Yakin Akan Menghapus?')\"");?>

                            </div>
                          </td>
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
					  <?php echo $links?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
