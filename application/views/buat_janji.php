<!--Page header & Title-->
<section id="page_header3">
<div class="page_title3">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Buat Janji</h2>
         <div class="page_link"><a href="<?php echo base_url();?>home/beranda">Beranda</a><span><i class="fa fa-long-arrow-right"></i>Form Janji</span></div>
      </div>
    </div>
  </div>
</div>  
</section>


<section class="padding">
  <div class="container appointment_wrap padding-half">
    <div class="row">
      <div class="col-md-12">
        <h2>Buat Pertemuan Dengan Dokter Kami</h2>
        <p>Janji yang anda buat akan disesuaikan dengan jadwal maupun kesibukan dokter kami</p>
       
        <div class="col-md-7 col-sm-8">
          
          <div class="row">
            <form class="callus" onSubmit="return false"  id="app_form">
              <div class="row">
                
                 <div class="col-md-12">
                    <div id="result" class="text-center form-group"></div>
                 </div>
                
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" type="text" id="name" name="name"  placeholder="Your Name" required />
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" type="text" name="phone" id="phone"  placeholder="Phone No" required />
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" type="email"  name="email" id="email" placeholder="Email" required />
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="date" class="form-control" placeholder="Appointment Date" id="app_date" name="app_date" />
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <textarea placeholder="Message" cols="11" id="message" name="message" required></textarea>
                  </div>
                  <div class="form-group">
                     <div class="btn-submit button3">
                    <input type="submit" id="btn_app_submit" value="Submit Request" />
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-5 col-sm-4"> </div>
      </div>
    </div>
  </div>
</section>
