<!--Page header & Title-->
<section id="page_header">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">FAQ</h2>
         <div class="page_link"><a href="<?php echo base_url();?>home/beranda">Beranda</a><i class="fa fa-long-arrow-right"></i><a href="#">Halaman</a><i class="fa fa-long-arrow-right"></i><span>Faq</span></div>
      </div>
    </div>
  </div>
</div>  
</section>



<section id="faq" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <h2 class="heading">Frequently Asked Questions</h2>
      <hr class="heading_space">
        <div class='faq_wrapper'>
  <ul class='items'>
  
	<?php foreach($faq as $ask){ ?>
    <li>
      <a href='#'> <?php echo $ask->tanya;?></a>
      <ul class='sub-items'>
        <li>
          <p class="half_space"> <?php echo $ask->jawab;?></p>          
        </li>
      </ul>
    </li>
	<?php } ?>
	
	
  </ul>
</div>

      
      </div>
    </div>
  </div>
</section>
