<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Grafik Harian</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("grafik/harian_result", 
                            "class='form-horizontal' row-border")?> 
					 <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal :</label>
                        <div class="col-md-2">
							<div class='input-group date' id='datetimepicker1'>
								<input class="form-control" type="text"  name="tanggal" placeholder="Tanggal" id="datetimepicker1" maxlength="100" required>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
					  </div>	
				
				<div class="form-group">
					<label class="control-label col-md-2">Cari Berdasarkan :</label>
					<div class="col-md-2">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
								<input type="radio" value="kabupaten" name="cakupan" id="cek" checked="true">Kabupaten
							</label>
							<label class="btn btn-default active">
								<input type="radio" value="pasar" name="cakupan" id="cek">Pasar
							</label>
						</div>

					</div>
				</div>
				
				<div class="form-group hidden" id="kabupaten">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="id_kabupaten">
								
                                <!--<?php
								if($id_kabupaten==1){?>
									<option value="">
									- Kabupaten -
									</option>
									<?php
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten'>
									$cb->nama_kabupaten</option>";
									}
								} else {
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten' selected>
									$cb->nama_kabupaten</option>";
									}
								}
								
                                ?>-->
								<option value="">
									- Kabupaten -
									</option>
                                <?php
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten'>
									$cb->nama_kabupaten</option>";
									}
								
                                ?>
                         </select>	
				</div>
					
				</div>
				
				
				
				<div class="form-group" id="pasar">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="id_pasar" >
								
                                <option value="">
									- Pasar -
								</option>
                                <?php
								foreach ($pasar as $cb) {
                                    echo "<option value='$cb->id_pasar'>
                                  $cb->nama_pasar</option>";
                                }
                                ?>
                         </select>					
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-2">Jenis Komoditi</label>
					<div class="col-md-3">
						<select class="form-control" name="jenis_komoditi" required>
						<option value="" selected>- Jenis Komoditi-</option>
						<option value="Bahan Pokok" >Bahan Pokok</option>
						<option value="Bahan Strategis">Bahan Strategis</option>
                         </select>	

					</div>
				</div>
				
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary" >Cetak</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   

<script type="text/javascript">
		$('label > #cek').change(function(){ if( $(this).is(":checked") ) 
			{ 
				$('#pasar').toggleClass('hidden'); 
				$('#kabupaten').toggleClass('hidden'); 
			}; 
		}); 
</script>
