<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail Berita</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("", 
                            "class='form-horizontal' row-border")?> 
                         <input type="hidden" name="id_berita" value="<?php echo $entry->id_berita?>" readonly>
					    <div class="form-group">
                        <label class="col-md-2 control-label">Judul</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="judul" placeholder="Judul" value="<?php echo $entry->judul?>" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Isi</label>
                        <div class="col-sm-10">
                           <?php echo $entry->content?>
                        </div>
                      </div>
					  <div class="form-group">
							<label class="col-md-2 control-label" >Gambar</label>
							<div class="col-md-6">
									<?php 		
	
								$image = array(
								  'src' => '/files/profil_image/'.($entry->gambar),
								  'class' => 'photo',
								  'width' => '200',
								  'height' => '200',
								);

								echo img($image); ?>
									<input class="form-control" type="file" name="userfile">
							</div>
					  </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <?php echo anchor('/berita/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Kelurahan">
											Kembali</button>'); ?>
                        </div>
						
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   