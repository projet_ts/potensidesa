<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Profil</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open_multipart("profil/update_data", 
                            "class='form-horizontal' row-border")?> 
                      <input type="hidden" name="id_profil" value="<?php echo $entry->id_profil?>">
					    <div class="form-group">
                        <label class="col-md-2 control-label">Judul</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="judul" placeholder="Judul" value="<?php echo $entry->judul?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Isi</label>
                        <div class="col-sm-10">
                            <textarea name="content" id="content1"><?php echo $entry->content?></textarea><?php echo display_ckeditor($editor['ckeditor1']);?>
                        </div>
                      </div>
					  <div class="form-group">
							<label class="col-md-2 control-label" >Upload File</label>
							<div class="col-md-6">
									<?php 		
	
								$image = array(
								  'src' => '/files/profil_image/'.($entry->gambar),
								  'class' => 'photo',
								  'width' => '200',
								  'height' => '200',
								);

								echo img($image); ?>
									<input class="form-control" type="file" name="userfile">
							</div>
					  </div>
                     <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" title="Update Data">Update</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/profil/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Profil">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   