<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data Berita</a>
                    </div>
					<span class="tools">
                      <i class="fa fa-list-ol"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      <div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									<?php echo anchor('/berita/insert', 
									'<button type="button" class="btn btn-info 
										" data-toggle="tooltip" data-placement="top" title="Tambah Data Berita">
											<i class="fa fa-plus"></i> Tambah</button>'); ?>
									<?php echo anchor('/berita/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Refresh Data Berita">
											<i class="fa fa-refresh"></i> Refresh</button>'); ?>
								</div>
							</div>
							
							<?php echo form_open("berita/result");?>
							
							<div class="col-md-3 col-sm-4 col-xs-4">
								<input type="text" class="form-control" name="key" placeholder="Cari">
							</div>
							
							<div class="btn-group">
								<button type="submit" class="btn btn-info" title="Cari Data User">
									<i class="fa fa-search"></i>
								</button>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
                    <div class="clearfix">
                    </div> 
                      
                    <div class="container">
                        <br>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left">
                        
                        <thead>
                          <tr>
                            <th style="width:2%">
                              No
                            </th>
							<th style="width:10%">
                              Tanggal
                            </th>
							<th style="width:20%">
                              Judul
                            </th>
							<th style="width:60%">
                              Isi
                            </th>
							<th style="width:60%">
                              Gambar
                            </th>
                            <th style="width:1%">
                              Pilihan
                            </th>
                            
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no = $number + 1;
                            foreach ($berita as $v) {
                        ?>    
                          <tr class="gradeA success">
                            <td>
                              <?php echo $no ?>
                            </td>  
							<td>
                              <?php 
							  $tanggal = date('d-m-Y', strtotime($v->tanggal ));
							  echo $tanggal;
							  ?>
                            </td>  
                            <td>
                              <?php echo $v->judul ?>
                            </td>
							<td>
                              <?php echo $v->content ?>
                            </td>
							<td>
                              <?php 		
	
								$image = array(
								  'src' => '/files/berita_image/'.($v->gambar),
								  'class' => 'photo',
								  'width' => '200',
								  'height' => '200',
								);

								echo img($image); ?>
                            </td>
                            <td class="hidden-xs">
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Pilihan 
                                  <span class="caret"></span></button>
                              <ul class="dropdown-menu pull-right" role="menu">
                                <li><?php echo anchor("/berita/detail?id=".$v->id_berita, "<i class='fa fa-check-square-o'></i> Detail"); ?></li>
                                <li><?php echo anchor("/berita/update?id=".$v->id_berita, "<i class='fa fa-pencil-square-o'></i> Edit"); ?></li>
                                <li class="divider"></li>
                                <li><?php echo anchor("/berita/delete?id=".$v->id_berita, "
                                    <i class='fa fa-trash-o'></i> Hapus
                                ", "onclick=\"return confirm('Anda Yakin Akan Menghapus?')\"");
                                ?></li>
                              </ul>
                            </div>
                          </td>
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
					  <div align="right"><?php echo $links?> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   