<!--Slider-->
<section class="slider-main">

			<div class="rev_slider_wrapper">			
				<!-- START REVOLUTION SLIDER 5.0 auto mode -->
				<div id="rev_slider" class="rev_slider"  data-version="5.0">
					<ul>	
						<!-- SLIDE  -->
						<li data-transition="fade">
							<!-- MAIN IMAGE -->
							<img src="<?php echo base_url();?>images/banner1.png" alt="" data-bgposition="center center" data-bgfit="cover">							
							<!-- LAYER NR. 1 -->
                     <h1 class="tp-caption tp-resizeme" 
                          data-x="left" data-hoffset="15"
                          data-y="200" data-width="full" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="500" 
                          data-splitin="none" 
                          data-splitout="none"
                          style="z-index: 6;">
                          
                     </h1>
							
                     <p class="tp-caption  tp-resizeme" 							
                      data-x="left" data-hoffset="15"
                      data-y="340"							
                      data-width="full"
                      data-transform_idle="o:1;"
                      data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                      data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                      data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                      data-mask_out="x:0;y:0;s:inherit;e:inherit;" 							 
                      data-start="800">
							</p>
						</li>
						
						<!-- SLIDE  -->
						<li data-transition="fade">
							<img src="<?php echo base_url();?>images/banner2.png"  alt="" data-bgposition="center center" data-bgfit="cover">							
							<h1 class="tp-caption tp-resizeme" 
                          data-x="left" data-hoffset="15"
                          data-y="200" data-width="full" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="500" 
                          data-splitin="none" 
                          data-splitout="none" 
                          style="z-index: 6;">
                          
                     </h1>
                     <p class="tp-caption  tp-resizeme" 							
                      data-x="left" data-hoffset="15"
                      data-y="340"							
                      data-width="full"
                      data-transform_idle="o:1;"
                      data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                      data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                      data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                      data-mask_out="x:0;y:0;s:inherit;e:inherit;" 							 
                      data-start="800">
					  
							</p>
						</li>

						<!-- SLIDE  -->
						<li data-transition="fade">
							<img src="<?php echo base_url();?>images/banner3.png"  alt="" data-bgposition="center center" data-bgfit="cover">							
							<h1 class="tp-caption tp-resizeme" 
                          data-x="left" data-hoffset="500" 
                          data-y="200" data-width="full" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="500" 
                          data-splitin="none" 
                          data-splitout="none" 
                          style="z-index: 6;">
                          
                     </h1>
                     <p class="tp-caption tp-resizeme" 							
                      data-x="left" data-hoffset="500" 
                      data-y="340"							
                      data-width="full"
                      data-transform_idle="o:1;"
                      data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                      data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                      data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                      data-mask_out="x:0;y:0;s:inherit;e:inherit;" 							 
                      data-start="800">
							</p>
						</li>
					</ul>				
				</div><!-- END REVOLUTION SLIDER -->
			</div>	

</section>
