<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//controller for modul sistem industri

class Banner extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_banner');
		$this->load->model('m_login');
		
		$this->load->helper(array('url', 'html', 'text', 'ckeditor', 'date'));
        $this->load->library(array('form_validation','session'));
        
        $this->data['ckeditor1'] = array(		
	    //ID of the textarea that will be replaced
	    'id' 	=> 	'content',
	    'path'	=>	'asset/ckeditor',
            'config'    =>      array(
		'toolbar' => 	array(	//Setting a custom toolbar
		    array('Source','Maximize','ShowBlocks'),
                    array('Styles','Format','FontSize','Font'),
                    array('Cut','Copy','Paste'),
                    array('Save','Preview','Print','Templates'),
                    array('Bold','Italic','Underline','Strike','Subscript','Superscript'),
                    array('Undo','Redo'),
                    array('Find','Replace'),
                    array('NumberedList','BulletedList','Outdent','Indent','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'),                    
                    array('Link','Unlink','Anchor','Table','Image','Smiley'),
		    '/'
		)
	    )
        );
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		
    }

    public function data(){
		 //config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/banner/data";
		$config["total_rows"] = $this->m_banner->record_count();
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		$data["banner"] = $this->m_banner->fetch_banner($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('posts/module_banner/banner', $data);
        $this->load->view('admin/footer');
    }
	
	public function result(){
        
		//config for pagination
		$search = ($this->input->post("key"))? $this->input->post("key") : "NIL";
		$search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;
				
		$config = array();
		$config["base_url"] = base_url()."berita/result/".$search;
		$config["total_rows"] = $this->m_berita->record_count_search($search);
		$config["per_page"] = 30;
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
	
		if($this->uri->segment(4)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(4);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4))?$this->uri->segment(4) : 0;
				
		$data["berita"] = $this->m_berita->search_berita($config["per_page"], $page, $search);
		$data["links"] = $this->pagination->create_links();
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('admin/header');
		$this->load->view('admin/notification');
		$this->load->view('admin/menu', $data);
		$this->load->view('posts/module_berita/berita', $data);
		$this->load->view('admin/footer');	
		
    }
	
    public function create_data(){
	
		$filename = $this->input->post('userfile');
        $config['upload_path'] = "./files/banner/";
        $config['allowed_types'] = "jpg|jpeg|png";
        $config['overwrite']="true";
        $config['max_size']="200000000";
        $config['file_name'] = $this->input->post('judul');
        $this->load->library('upload', $config);
		

		if(!$this->upload->do_upload())
        {
				//get data
				$data['judul'] = $this->input->post('judul');
				
				
				//call function
				$this->m_banner->createnoimage($data);
				
		}else{
						 //get data
				$dat = $this->upload->data();
				$data['download'] = $dat['file_name'];
				$data['judul'] = $this->input->post('judul');
				
				//call function
				$this->m_banner->create($data);
				

		
		
		}
        //redirect to page
        redirect('banner/data');
    }
    
    public function update(){
		
		$data['editor']=$this->data;
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_banner->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('banner/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('posts/module_banner/update', $data);
            $this->load->view('admin/footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_berita->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('berita/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('posts/module_berita/detail', $data);
            $this->load->view('admin/footer');
        }
    }
    
    public function insert() {
       
		$data['editor']=$this->data;
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
			$this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('posts/module_banner/insert', $data);
            $this->load->view('admin/footer');
        
    }
    
    public function update_data() {
        
        $filename = $this->input->post('userfile');
        $config['upload_path'] = "./files/banner/";
        $config['allowed_types'] = "jpg|jpeg|png";
        $config['overwrite']="true";
        $config['max_size']="20000000";
        $config['file_name'] = $this->input->post('judul');
        $this->load->library('upload', $config);
		

		if(!$this->upload->do_upload())
        {
				//get data
        		$data['id_banner'] = $this->input->post('id_banner');
				$data['judul'] = $this->input->post('judul');
				
				//call function
				$this->m_banner->updatenoimage($data);
				
		}else{
						 //get data
				$dat = $this->upload->data();
				$data['download'] = $dat['file_name'];
				$data['id_banner'] = $this->input->post('id_banner');
				$data['judul'] = $this->input->post('judul');
				//call function
				$this->m_banner->update($data);
				
		
		}
        //redirect to page
        redirect('banner/data');
        
    }

    public function delete() {
                
        if($this->input->get('id')!="") {
		
			$gambar = $this->m_banner->link_gambar($this->input->get('id'));
			if ($gambar->num_rows() > 0)
			{
				$row = $gambar->row();			
				$file_gambar = $row->gambar;
				echo $file_gambar;
				$path_file = './files/banner/';
				unlink($path_file.$file_gambar);
			}
			
            $this->m_berita->delete($this->input->get('id'));
			
		
        }
        
        //redirect to page
        redirect('banner/data');
    }
      
}
