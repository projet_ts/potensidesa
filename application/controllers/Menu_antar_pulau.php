<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_antar_pulau extends CI_Controller {
    public function __construct(){  
        parent::__construct();
		//error_reporting(0);
		
		$this->load->model('m_pasar');
		$this->load->model('m_pokok');
		$this->load->model('m_komoditi_tetap');
		$this->load->model('m_kabupaten');
		$this->load->model('m_antar_pulau');
		$this->load->model('m_grafik');
	}
	
	public function antar_pulau() {
		$data["komoditi_tetap"] = $this->m_komoditi_tetap->data();
		$this->load->view('frontpage/header');
        $this->load->view('frontpage/nav2');
        $this->load->view('frontpage/antar_pulau/antar_pulau', $data);
        $this->load->view('frontpage/footer');
		
    }
       
	public function antar_pulau_result() {
	
		$data['id_komoditi_stok'] = $this->input->post("id_komoditi_stok");
		$data['tahun'] = $this->input->post("tahun");
		$data["komoditi_tetap"] = $this->m_komoditi_tetap->data();
		
		$data['grafik'] = $this->m_grafik->grafik_antar_pulau($data['id_komoditi_stok'],$data['tahun']);
		$data['kabupaten'] = $this->m_kabupaten->data2();
		$data['nama_komoditi'] = $this->m_komoditi_tetap->get($data['id_komoditi_stok']);
		$data['nama_komoditi'] = $data['nama_komoditi'][0];
	
		
		$this->load->view('frontpage/header');
        $this->load->view('frontpage/nav2');
        $this->load->view('frontpage/antar_pulau/antar_pulau_result', $data);
        $this->load->view('frontpage/footer');
		
    }
	
       
}