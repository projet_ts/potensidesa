<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Submenu extends CI_Controller {

    public function __construct(){
    parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_menu');
		$this->load->model('m_submenu');

    //cek login
		if(empty($this->session->userdata('userid'))) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
    }

		//cek hak akses
		if(empty($this->m_login->access($this->session->userdata('group_id'), "Setting"))) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));

      //view
			$this->load->view('admin/header');
			$this->load->view('admin/notification');
			$this->load->view('admin/menu', $data);
			$this->load->view('admin/forbidden');
			$this->load->view('admin/footer');
		}
    }

    public function data(){

		//get id from menu_id
		$data["id"] = $this->input->get('id');

		//config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/submenu/data";
		$config["total_rows"] = $this->m_submenu->record_count($data["id"]);
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;

		//config css for pagination
		  $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
    //select data from table_menu
		$data["ssubmenu"] = $this->m_submenu->fetch_submenu($config["per_page"], $page, $data["id"]);
		$data["links"] = $this->pagination->create_links();
    //select menu for navigation
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$data["id"] = $this->input->get('id');
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        //view module
        $this->load->view('module_submenu/submenu', $data);
        $this->load->view('admin/footer');
    }

	public function result(){
    //get id from menu_id
		$data["id"] = $this->input->post('id');

		if(!$this->input->post('key')){
			redirect("submenu/data?id=".$data["id"]);
		}else{
			$data["ssubmenu"] = $this->m_submenu->search_submenu($this->input->post('key'), $this->input->post('id'));
			$data["links"] = "";
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('admin/header');
			$this->load->view('admin/notification');
			$this->load->view('admin/menu', $data);
			$this->load->view('module_submenu/submenu', $data);
			$this->load->view('admin/footer');
		}
    }

    public function insert(){

		//get id
		$data["id"] = $this->input->get('id');

    		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
    		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
    		$this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('module_submenu/insert', $data);
        $this->load->view('admin/footer');
    }

    public function update(){

		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_submenu->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('submenu/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('module_submenu/update', $data);
            $this->load->view('admin/footer');
        }

    }

    public function detail(){

		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_submenu->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('submenu/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('module_submenu/detail', $data);
            $this->load->view('admin/footer');
        }
    }

    public function create_data() {

        //get data
        $data['submenu_id'] = "submenu".rand(999, 100);
		    $data['menu_id'] = $this->input->post('menu_id');
        $data['submenu_name'] = $this->input->post('submenu_name');
        $data['attribute'] = $this->input->post('attribute');
		    $data['link'] = $this->input->post('link');
        $data['active'] = $this->input->post('active');

        //call function
        $this->m_submenu->create($data);

  		//log system
  		$this->m_log->create($this->session->userdata('userid'),
			"Insert Data Submenu dengan submenu_id = ".$data['submenu_id']);

        //redirect to page
        redirect('submenu/data?id='.$data['menu_id']);

    }

    public function update_data() {

        //get data
		    $data['submenu_id'] = $this->input->post('submenu_id');
        $data['menu_id'] = $this->input->post('menu_id');
        $data['submenu_name'] = $this->input->post('submenu_name');
        $data['attribute'] = $this->input->post('attribute');
		    $data['link'] = $this->input->post('link');
        $data['active'] = $this->input->post('active');

        //call function
        $this->m_submenu->update($data);

		//log system
		$this->m_log->create($this->session->userdata('userid'),
			"Update Data Submenu dengan submenu_id = ".$data['submenu_id']);

        //redirect to page
        redirect('submenu/data?id='.$data['menu_id']);

    }

    public function delete() {

        if($this->input->get('id')!="") {
            $this->m_submenu->delete($this->input->get('id'));

  			//log system
  			$this->m_log->create($this->session->userdata('userid'),
  			"Hapus Data Submenu dengan submenu_id = ".$this->input->get('id'));
        }

        //redirect to page
        redirect('submenu/data');

    }

}
