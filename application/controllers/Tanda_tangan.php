<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//controller for modul sistem perindustrian

class Tanda_tangan extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_pelabuhan');
		$this->load->model('m_tanda_tangan');
		$this->load->model('m_kabupaten');
		$this->load->model('m_login');
		$as=$this->session->userdata('userid');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Industri")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('forbidden');
			$this->load->view('footer');
		}
    }

    public function data(){
	
		$id_kabupaten = $this->session->userdata('id_kabupaten');
		
		 //config for pagination
		$config = array();
		$config["base_url"] = base_url()."tanda_tangan/data";
		
		if($this->session->userdata('group_id')=="group1000"){
			$config["total_rows"] = $this->m_tanda_tangan->record_count();
		}else{
			$config["total_rows"] = $this->m_tanda_tangan->record_count2($this->session->userdata('id_kabupaten'));
		}
		
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		
		if($this->session->userdata('group_id')=="group1000"){
			$data["tanda_tangan"] = $this->m_tanda_tangan->fetch_tanda_tangan($config["per_page"], $page);
		}else{
			$data["tanda_tangan"] = $this->m_tanda_tangan->fetch_tanda_tangan2($config["per_page"], $page, $this->session->userdata('id_kabupaten'));
		}
		
		$data["links"] = $this->pagination->create_links();
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('tanda_tangan/tanda_tangan', $data);
        $this->load->view('footer');
		
		
    }
	
	public function result(){
        //config for pagination
		$id_kabupaten = $this->session->userdata('id_kabupaten');
		
		if(!$this->input->post('key')){
				redirect('tanda_tangan/data');
		}else{
				if($this->session->userdata('group_id')=="group1000"){
					$data["tanda_tangan"] = $this->m_tanda_tangan->search_tanda_tangan($this->input->post('key'));
				}else{
					$data["tanda_tangan"] = $this->m_tanda_tangan->search_tanda_tangan2($this->input->post('key'), $this->session->userdata('id_kabupaten'));
				}
				
				$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
				$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
				$data["links"] = "";
				$this->load->view('header');
				$this->load->view('notification');
				$this->load->view('menu', $data);
				$this->load->view('tanda_tangan/tanda_tangan', $data);
				$this->load->view('footer');
		}
    }
	
    public function insert(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('tanda_tangan/insert');
        $this->load->view('footer');
    }
    
    public function update(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_tanda_tangan->get($this->input->get('id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('tanda_tangan/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('tanda_tangan/update', $data);
            $this->load->view('footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_tanda_tangan->get($this->input->get('id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('tanda_tangan/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('tanda_tangan/detail', $data);
            $this->load->view('footer');
        }
    }
    
    public function create_data() {
        //get data
		$data['nama'] = $this->input->post('nama');
		$data['nip'] = $this->input->post('nip');
		$data['golongan'] = $this->input->post('golongan');
		$data['jabatan'] = $this->input->post('jabatan');
		$data['id_kabupaten'] = $this->input->post('id_kabupaten');
        
        //call function
        $this->m_tanda_tangan->create($data);
        
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Insert Data Tanda Tangan");
		
        //redirect to page
        redirect('tanda_tangan/data');
    }
    
    public function update_data() {
        //get data
		$data['id_tanda_tangan'] = $this->input->post('id_tanda_tangan');
		$data['nama'] = $this->input->post('nama');
		$data['nip'] = $this->input->post('nip');
		$data['golongan'] = $this->input->post('golongan');
		$data['jabatan'] = $this->input->post('jabatan');
		$data['id_kabupaten'] = $this->input->post('id_kabupaten');
        
        //call function
        $this->m_tanda_tangan->update($data);
		
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Update Data Tanda Tangan dengan id_tanda_tangan= ".$data['id_tanda_tangan']);
        
        //redirect to page
        redirect('tanda_tangan/data');
        
    }
    
    public function delete() {
                
        if($this->input->get('id')!="") {
            $this->m_tanda_tangan->delete($this->input->get('id'));
			
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Hapus Data Tanda Tangan dengan id_tanda_tangan = ".$this->input->get('id'));
        }
        
        //redirect to page
        redirect('tanda_tangan/data');
        
    }
      
}
