<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_profil extends CI_Controller {
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		
		$this->load->model('m_profil');
		$this->load->model('m_berita');
        
        
	}
	
	public function profil() {
		
		$data['profil'] =  $this->m_profil->get($this->uri->segment(3));
		$data['profil'] = $data['profil'][0];
		$this->load->view('frontpage/header');
        $this->load->view('frontpage/nav2');
        $this->load->view('frontpage/profil', $data);
        $this->load->view('frontpage/footer');
		
    }
	
       
}