<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//controller for modul sistem industri

class Jenis_komoditi extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_jenis_komoditi');
		$this->load->model('m_login');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Industri")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('forbidden');
			$this->load->view('footer');
		} 
    }

    public function data(){
		 //config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/jenis_komoditi/data";
		$config["total_rows"] = $this->m_jenis_komoditi->record_count();
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		$data["jenis_komoditi"] = $this->m_jenis_komoditi->fetch_jenis_komoditi($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('industri/module_jenis_komoditi/jenis_komoditi', $data);
        $this->load->view('footer');
    }
	
	public function result(){
       
	   //config for pagination
		$search = ($this->input->post("key"))? $this->input->post("key") : "NIL";
		$search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;
				
		$config = array();
		$config["base_url"] = base_url()."jenis_komoditi/result/".$search;
		$config["total_rows"] = $this->m_jenis_komoditi->record_count_search($search);
		$config["per_page"] = 30;
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
	
		if($this->uri->segment(4)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(4);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4))?$this->uri->segment(4) : 0;
				
		$data["jenis_komoditi"] = $this->m_jenis_komoditi->search_jenis_komoditi($config["per_page"], $page, $search);
		$data["links"] = $this->pagination->create_links();
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('header');
		$this->load->view('notification');
		$this->load->view('menu', $data);
		$this->load->view('industri/module_jenis_komoditi/jenis_komoditi', $data);
		$this->load->view('footer');
		
    }
	
    public function insert(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('industri/module_jenis_komoditi/insert');
        $this->load->view('footer');
    }
    
    public function update(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_jenis_komoditi->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('jenis_komoditi/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('industri/module_jenis_komoditi/update', $data);
            $this->load->view('footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_jenis_komoditi->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('komoditi/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('industri/module_jenis_komoditi/detail', $data);
            $this->load->view('footer');
        }
    }
    
    public function create_data() {
        //get data
		$data['nama_jenis_komoditi'] = $this->input->post('nama_jenis_komoditi');
		$data['keterangan'] = $this->input->post('keterangan');
        
		//call function
        $this->m_jenis_komoditi->create($data);
        
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Insert Data Jenis komoditi");
		
        //redirect to page
        redirect('jenis_komoditi/data');
        
    }
    
    public function update_data() {
        
        //get data
		$data['id_jenis_komoditi'] = $this->input->post('id_jenis_komoditi');
		$data['nama_jenis_komoditi'] = $this->input->post('nama_jenis_komoditi');
		$data['keterangan'] = $this->input->post('keterangan');
		
        //call function
        $this->m_jenis_komoditi->update($data);
		
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Update Data jenis komoditi dengan id_komoditi = ".$data['id_komoditi']);
        
        //redirect to page
        redirect('jenis_komoditi/data');
        
    }
    
    public function delete() {
        if($this->input->get('id')!="") {
            $this->m_jenis_komoditi->delete($this->input->get('id'));
			
			//log system
			//$this->m_log->create($this->session->userdata('userid'), 
				//"Hapus Data komoditi dengan id_komoditi = ".$this->input->get('id'));
        }
        
        //redirect to page
        redirect('jenis_komoditi/data');
        
    }
      
}
