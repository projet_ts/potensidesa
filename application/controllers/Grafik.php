<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grafik extends CI_Controller {
	/**
	 * @author : Mahesa p Kannygara
	 **/
	function __construct()
	{
		parent::__construct();	
		error_reporting(0);
		$this->load->model('m_chart','',TRUE);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_pelabuhan');
		$this->load->model('m_pasar');
		$this->load->model('m_kabupaten');
		$this->load->model('m_pokok');
		$this->load->model('m_strategis');
		$this->load->model('m_grafik');
		$this->load->model('m_komoditi');
		$this->load->model('m_login');
		$as=$this->session->userdata('userid');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Industri")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('forbidden');
			$this->load->view('footer');
		}
	}
	
	public function index()
	{
		foreach($this->m_chart->laporanTahunan()->result_array() as $row)
		{
			$data['grafik'][]=(float)$row['Januari'];
			$data['grafik'][]=(float)$row['Februari'];
			$data['grafik'][]=(float)$row['Maret'];
			$data['grafik'][]=(float)$row['April'];
			$data['grafik'][]=(float)$row['Mei'];
			$data['grafik'][]=(float)$row['Juni'];
			$data['grafik'][]=(float)$row['Juli'];
			$data['grafik'][]=(float)$row['Agustus'];
			$data['grafik'][]=(float)$row['September'];
			$data['grafik'][]=(float)$row['Oktober'];
			$data['grafik'][]=(float)$row['November'];
			$data['grafik'][]=(float)$row['Desember'];
		}
		$this->load->view('v_grafik',$data);
	}
	
	public function harian(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
		
		$data['pokok']= $this->m_pokok->getpokok();
		$data['strategis']= $this->m_strategis->getStrategis();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('grafik/grafik_harian/grafik_harian', $data);
        $this->load->view('footer');
	}
	
	public function harian_result(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['tanggal'] = $this->input->post("tanggal");
		$id_pasar = $this->input->post("id_pasar");
		$id_kabupaten = $this->input->post("id_kabupaten");
		$data['jenis_komoditi'] = $this->input->post("jenis_komoditi");
		
		if($this->input->post("id_kabupaten")==""){
			$data['grafik'] = $this->m_grafik->grafik_harian_pasar($data['tanggal'],$id_pasar,$data['jenis_komoditi']);
			$data['tempat'] = $this->m_pasar->get($id_pasar);
			$data['tempat2'] = $data['tempat'][0];
		} 
		else if($this->input->post("id_pasar")==""){
			$data['grafik'] = $this->m_grafik->grafik_harian_kabupaten($data['tanggal'],$id_kabupaten,$data['jenis_komoditi']);
			$data['tempat'] = $this->m_kabupaten->get($id_kabupaten);
			$data['tempat2'] = $data['tempat'][0];
		}else{
			$data['grafik'] = $this->m_grafik->grafik_harian_kabupaten($data['tanggal'],$id_kabupaten,$data['jenis_komoditi']);
			$data['tempat'] = $this->m_kabupaten->get($id_kabupaten);
			$data['tempat2'] = $data['tempat'][0];
		}
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
		
		$data['pokok']= $this->m_pokok->getpokok();
		$data['strategis']= $this->m_strategis->getStrategis();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('grafik/grafik_harian/grafik_harian_result', $data);
        $this->load->view('footer');
	}
	
	public function mingguan(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
		
		$data['pokok']= $this->m_pokok->getpokok();
		$data['strategis']= $this->m_strategis->getStrategis();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('grafik/grafik_mingguan/grafik_mingguan', $data);
        $this->load->view('footer');
	}
	
	public function mingguan_result(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['tanggal'] = $this->input->post("tanggal");
		$id_pasar = $this->input->post("id_pasar");
		$id_kabupaten = $this->input->post("id_kabupaten");
		
		if($this->input->post("bahan_pokok")==""){
			$data['jenis_komoditi'] = $this->input->post("bahan_strategis");
			$data['jenis_komoditi2']= $this->m_komoditi->get($data['jenis_komoditi']);
			$data['jenis_komoditi2']= $data['jenis_komoditi2'][0];
			
		}else if($this->input->post("bahan_strategis")==""){
			$data['jenis_komoditi'] = $this->input->post("bahan_pokok");
			$data['jenis_komoditi2']= $this->m_komoditi->get($data['jenis_komoditi']);
			$data['jenis_komoditi2']= $data['jenis_komoditi2'][0];
			
		}else{
			$data['jenis_komoditi'] = $this->input->post("bahan_pokok");
			$data['jenis_komoditi2']= $this->m_komoditi->get($data['jenis_komoditi']);
			$data['jenis_komoditi2']= $data['jenis_komoditi2'][0];
		}
		
		
		
		if($this->input->post("id_kabupaten")==""){
				
				foreach($this->m_grafik->grafik_mingguan_pasar($data['tanggal'],$id_pasar,$data['jenis_komoditi'])->result_array() as $row)
				{
					 $data['grafik'][]=(double)$row['1'];
					 $data['grafik'][]=(double)$row['2'];
					 $data['grafik'][]=(double)$row['3'];
					 $data['grafik'][]=(double)$row['4'];
					 $data['grafik'][]=(double)$row['5'];
					 $data['grafik'][]=(double)$row['6'];
					 $data['grafik'][]=(double)$row['7'];
				}
				
				foreach($this->m_grafik->grafik_mingguan_tanggal($data['tanggal'])->result_array() as $row)
				{
					 $data['grafik2'][]=(string)$row['1'];
					 $data['grafik2'][]=(string)$row['2'];
					 $data['grafik2'][]=(string)$row['3'];
					 $data['grafik2'][]=(string)$row['4'];
					 $data['grafik2'][]=(string)$row['5'];
					 $data['grafik2'][]=(string)$row['6'];
					 $data['grafik2'][]=(string)$row['7'];
				}
				
			$data['tempat'] = $this->m_pasar->get($id_pasar);
			$data['tempat2'] = $data['tempat'][0];
		
		} else if ($this->input->post("id_pasar")==""){
				
				foreach($this->m_grafik->grafik_mingguan_kabupaten($data['tanggal'],$id_kabupaten,$data['jenis_komoditi'])->result_array() as $row)
				{
					 $data['grafik'][]=(double)$row['1'];
					 $data['grafik'][]=(double)$row['2'];
					 $data['grafik'][]=(double)$row['3'];
					 $data['grafik'][]=(double)$row['4'];
					 $data['grafik'][]=(double)$row['5'];
					 $data['grafik'][]=(double)$row['6'];
					 $data['grafik'][]=(double)$row['7'];
				}
				
				foreach($this->m_grafik->grafik_mingguan_tanggal($data['tanggal'])->result_array() as $row)
				{
					 $data['grafik2'][]=(string)$row['1'];
					 $data['grafik2'][]=(string)$row['2'];
					 $data['grafik2'][]=(string)$row['3'];
					 $data['grafik2'][]=(string)$row['4'];
					 $data['grafik2'][]=(string)$row['5'];
					 $data['grafik2'][]=(string)$row['6'];
					 $data['grafik2'][]=(string)$row['7'];
				}
			$data['tempat'] = $this->m_kabupaten->get($id_kabupaten);
			$data['tempat2'] = $data['tempat'][0];
		} 
		else {
			foreach($this->m_grafik->grafik_mingguan_kabupaten($data['tanggal'],$id_kabupaten,$data['jenis_komoditi'])->result_array() as $row)
				{
					 $data['grafik'][]=(double)$row['1'];
					 $data['grafik'][]=(double)$row['2'];
					 $data['grafik'][]=(double)$row['3'];
					 $data['grafik'][]=(double)$row['4'];
					 $data['grafik'][]=(double)$row['5'];
					 $data['grafik'][]=(double)$row['6'];
					 $data['grafik'][]=(double)$row['7'];
				}
				
				foreach($this->m_grafik->grafik_mingguan_tanggal($data['tanggal'])->result_array() as $row)
				{
					 $data['grafik2'][]=(string)$row['1'];
					 $data['grafik2'][]=(string)$row['2'];
					 $data['grafik2'][]=(string)$row['3'];
					 $data['grafik2'][]=(string)$row['4'];
					 $data['grafik2'][]=(string)$row['5'];
					 $data['grafik2'][]=(string)$row['6'];
					 $data['grafik2'][]=(string)$row['7'];
				}
			$data['tempat'] = $this->m_kabupaten->get($id_kabupaten);
			$data['tempat2'] = $data['tempat'][0];
		}
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
		
		$data['pokok']= $this->m_pokok->getpokok();
		$data['strategis']= $this->m_strategis->getStrategis();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('grafik/grafik_mingguan/grafik_mingguan_result', $data);
        $this->load->view('footer');
	}
	
	public function bulanan(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
		
		$data['pokok']= $this->m_pokok->getpokok();
		$data['strategis']= $this->m_strategis->getStrategis();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('grafik/grafik_bulanan/grafik_bulanan', $data);
        $this->load->view('footer');
	}
	
	public function bulanan_result(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['bulan'] = $this->input->post("bulan");
		$data['tahun'] = $this->input->post("tahun");
		$id_pasar = $this->input->post("id_pasar");
		$id_kabupaten = $this->input->post("id_kabupaten");
		
		if($this->input->post("bahan_pokok")==""){
			$data['jenis_komoditi'] = $this->input->post("bahan_strategis");
			$data['jenis_komoditi2']= $this->m_komoditi->get($data['jenis_komoditi']);
			$data['jenis_komoditi2']= $data['jenis_komoditi2'][0];
			
		}else if ($this->input->post("bahan_strategis")==""){
			$data['jenis_komoditi'] = $this->input->post("bahan_pokok");
			$data['jenis_komoditi2']= $this->m_komoditi->get($data['jenis_komoditi']);
			$data['jenis_komoditi2']= $data['jenis_komoditi2'][0];
		}else{
			$data['jenis_komoditi'] = $this->input->post("bahan_pokok");
			$data['jenis_komoditi2']= $this->m_komoditi->get($data['jenis_komoditi']);
			$data['jenis_komoditi2']= $data['jenis_komoditi2'][0];
		}
		
		
		
		if($this->input->post("id_kabupaten")==""){
				
				foreach($this->m_grafik->grafik_bulanan_pasar($data['bulan'],$data['tahun'],$id_pasar,$data['jenis_komoditi'])->result_array() as $row)
				{
					 $data['grafik'][]=(double)$row['1'];
					 $data['grafik'][]=(double)$row['2'];
					 $data['grafik'][]=(double)$row['3'];
					 $data['grafik'][]=(double)$row['4'];
					 $data['grafik'][]=(double)$row['5'];
					 $data['grafik'][]=(double)$row['6'];
					 $data['grafik'][]=(double)$row['7'];
					 $data['grafik'][]=(double)$row['8'];
					 $data['grafik'][]=(double)$row['9'];
					 $data['grafik'][]=(double)$row['10'];
					 $data['grafik'][]=(double)$row['11'];
					 $data['grafik'][]=(double)$row['12'];
					 $data['grafik'][]=(double)$row['13'];
					 $data['grafik'][]=(double)$row['14'];
					 $data['grafik'][]=(double)$row['15'];
					 $data['grafik'][]=(double)$row['16'];
					 $data['grafik'][]=(double)$row['17'];
					 $data['grafik'][]=(double)$row['18'];
					 $data['grafik'][]=(double)$row['19'];
					 $data['grafik'][]=(double)$row['20'];
					 $data['grafik'][]=(double)$row['21'];
					 $data['grafik'][]=(double)$row['22'];
					 $data['grafik'][]=(double)$row['23'];
					 $data['grafik'][]=(double)$row['24'];
					 $data['grafik'][]=(double)$row['25'];
					 $data['grafik'][]=(double)$row['26'];
					 $data['grafik'][]=(double)$row['27'];
					 $data['grafik'][]=(double)$row['28'];
					 $data['grafik'][]=(double)$row['29'];
					 $data['grafik'][]=(double)$row['30'];
					 $data['grafik'][]=(double)$row['31'];
				}
				
				foreach($this->m_grafik->grafik_bulanan_tanggal($data['bulan'],$data['tahun'])->result_array() as $row)
				{
					 $data['grafik2'][]=(string)$row['1'];
					 $data['grafik2'][]=(string)$row['2'];
					 $data['grafik2'][]=(string)$row['3'];
					 $data['grafik2'][]=(string)$row['4'];
					 $data['grafik2'][]=(string)$row['5'];
					 $data['grafik2'][]=(string)$row['6'];
					 $data['grafik2'][]=(string)$row['7'];
					 $data['grafik2'][]=(string)$row['8'];
					 $data['grafik2'][]=(string)$row['9'];
					 $data['grafik2'][]=(string)$row['10'];
					 $data['grafik2'][]=(string)$row['11'];
					 $data['grafik2'][]=(string)$row['12'];
					 $data['grafik2'][]=(string)$row['13'];
					 $data['grafik2'][]=(string)$row['14'];
					 $data['grafik2'][]=(string)$row['15'];
					 $data['grafik2'][]=(string)$row['16'];
					 $data['grafik2'][]=(string)$row['17'];
					 $data['grafik2'][]=(string)$row['18'];
					 $data['grafik2'][]=(string)$row['19'];
					 $data['grafik2'][]=(string)$row['20'];
					 $data['grafik2'][]=(string)$row['21'];
					 $data['grafik2'][]=(string)$row['22'];
					 $data['grafik2'][]=(string)$row['23'];
					 $data['grafik2'][]=(string)$row['24'];
					 $data['grafik2'][]=(string)$row['25'];
					 $data['grafik2'][]=(string)$row['26'];
					 $data['grafik2'][]=(string)$row['27'];
					 $data['grafik2'][]=(string)$row['28'];
					 $data['grafik2'][]=(string)$row['29'];
					 $data['grafik2'][]=(string)$row['30'];
					 $data['grafik2'][]=(string)$row['31'];
				}
				
			$data['tempat'] = $this->m_pasar->get($id_pasar);
			$data['tempat2'] = $data['tempat'][0];
		} 
		else if($this->input->post("id_pasar")==""){
			foreach($this->m_grafik->grafik_bulanan_kabupaten($data['bulan'],$data['tahun'],$id_kabupaten,$data['jenis_komoditi'])->result_array() as $row)
				{
					
					 $data['grafik'][]=(double)$row['1'];
					 $data['grafik'][]=(double)$row['2'];
					 $data['grafik'][]=(double)$row['3'];
					 $data['grafik'][]=(double)$row['4'];
					 $data['grafik'][]=(double)$row['5'];
					 $data['grafik'][]=(double)$row['6'];
					 $data['grafik'][]=(double)$row['7'];
					 $data['grafik'][]=(double)$row['8'];
					 $data['grafik'][]=(double)$row['9'];
					 $data['grafik'][]=(double)$row['10'];
					 $data['grafik'][]=(double)$row['11'];
					 $data['grafik'][]=(double)$row['12'];
					 $data['grafik'][]=(double)$row['13'];
					 $data['grafik'][]=(double)$row['14'];
					 $data['grafik'][]=(double)$row['15'];
					 $data['grafik'][]=(double)$row['16'];
					 $data['grafik'][]=(double)$row['17'];
					 $data['grafik'][]=(double)$row['18'];
					 $data['grafik'][]=(double)$row['19'];
					 $data['grafik'][]=(double)$row['20'];
					 $data['grafik'][]=(double)$row['21'];
					 $data['grafik'][]=(double)$row['22'];
					 $data['grafik'][]=(double)$row['23'];
					 $data['grafik'][]=(double)$row['24'];
					 $data['grafik'][]=(double)$row['25'];
					 $data['grafik'][]=(double)$row['26'];
					 $data['grafik'][]=(double)$row['27'];
					 $data['grafik'][]=(double)$row['28'];
					 $data['grafik'][]=(double)$row['29'];
					 $data['grafik'][]=(double)$row['30'];
					 $data['grafik'][]=(double)$row['31'];
				}
				
				foreach($this->m_grafik->grafik_bulanan_tanggal($data['bulan'],$data['tahun'])->result_array() as $row)
				{
					 $data['grafik2'][]=(string)$row['1'];
					 $data['grafik2'][]=(string)$row['2'];
					 $data['grafik2'][]=(string)$row['3'];
					 $data['grafik2'][]=(string)$row['4'];
					 $data['grafik2'][]=(string)$row['5'];
					 $data['grafik2'][]=(string)$row['6'];
					 $data['grafik2'][]=(string)$row['7'];
					 $data['grafik2'][]=(string)$row['8'];
					 $data['grafik2'][]=(string)$row['9'];
					 $data['grafik2'][]=(string)$row['10'];
					 $data['grafik2'][]=(string)$row['11'];
					 $data['grafik2'][]=(string)$row['12'];
					 $data['grafik2'][]=(string)$row['13'];
					 $data['grafik2'][]=(string)$row['14'];
					 $data['grafik2'][]=(string)$row['15'];
					 $data['grafik2'][]=(string)$row['16'];
					 $data['grafik2'][]=(string)$row['17'];
					 $data['grafik2'][]=(string)$row['18'];
					 $data['grafik2'][]=(string)$row['19'];
					 $data['grafik2'][]=(string)$row['20'];
					 $data['grafik2'][]=(string)$row['21'];
					 $data['grafik2'][]=(string)$row['22'];
					 $data['grafik2'][]=(string)$row['23'];
					 $data['grafik2'][]=(string)$row['24'];
					 $data['grafik2'][]=(string)$row['25'];
					 $data['grafik2'][]=(string)$row['26'];
					 $data['grafik2'][]=(string)$row['27'];
					 $data['grafik2'][]=(string)$row['28'];
					 $data['grafik2'][]=(string)$row['29'];
					 $data['grafik2'][]=(string)$row['30'];
					 $data['grafik2'][]=(string)$row['31'];
				}
			$data['tempat'] = $this->m_kabupaten->get($id_kabupaten);
			$data['tempat2'] = $data['tempat'][0];
		} else {
			foreach($this->m_grafik->grafik_bulanan_kabupaten($data['bulan'],$data['tahun'],$id_kabupaten,$data['jenis_komoditi'])->result_array() as $row)
				{
					
					 $data['grafik'][]=(double)$row['1'];
					 $data['grafik'][]=(double)$row['2'];
					 $data['grafik'][]=(double)$row['3'];
					 $data['grafik'][]=(double)$row['4'];
					 $data['grafik'][]=(double)$row['5'];
					 $data['grafik'][]=(double)$row['6'];
					 $data['grafik'][]=(double)$row['7'];
					 $data['grafik'][]=(double)$row['8'];
					 $data['grafik'][]=(double)$row['9'];
					 $data['grafik'][]=(double)$row['10'];
					 $data['grafik'][]=(double)$row['11'];
					 $data['grafik'][]=(double)$row['12'];
					 $data['grafik'][]=(double)$row['13'];
					 $data['grafik'][]=(double)$row['14'];
					 $data['grafik'][]=(double)$row['15'];
					 $data['grafik'][]=(double)$row['16'];
					 $data['grafik'][]=(double)$row['17'];
					 $data['grafik'][]=(double)$row['18'];
					 $data['grafik'][]=(double)$row['19'];
					 $data['grafik'][]=(double)$row['20'];
					 $data['grafik'][]=(double)$row['21'];
					 $data['grafik'][]=(double)$row['22'];
					 $data['grafik'][]=(double)$row['23'];
					 $data['grafik'][]=(double)$row['24'];
					 $data['grafik'][]=(double)$row['25'];
					 $data['grafik'][]=(double)$row['26'];
					 $data['grafik'][]=(double)$row['27'];
					 $data['grafik'][]=(double)$row['28'];
					 $data['grafik'][]=(double)$row['29'];
					 $data['grafik'][]=(double)$row['30'];
					 $data['grafik'][]=(double)$row['31'];
				}
				
				foreach($this->m_grafik->grafik_bulanan_tanggal($data['bulan'],$data['tahun'])->result_array() as $row)
				{
					 $data['grafik2'][]=(string)$row['1'];
					 $data['grafik2'][]=(string)$row['2'];
					 $data['grafik2'][]=(string)$row['3'];
					 $data['grafik2'][]=(string)$row['4'];
					 $data['grafik2'][]=(string)$row['5'];
					 $data['grafik2'][]=(string)$row['6'];
					 $data['grafik2'][]=(string)$row['7'];
					 $data['grafik2'][]=(string)$row['8'];
					 $data['grafik2'][]=(string)$row['9'];
					 $data['grafik2'][]=(string)$row['10'];
					 $data['grafik2'][]=(string)$row['11'];
					 $data['grafik2'][]=(string)$row['12'];
					 $data['grafik2'][]=(string)$row['13'];
					 $data['grafik2'][]=(string)$row['14'];
					 $data['grafik2'][]=(string)$row['15'];
					 $data['grafik2'][]=(string)$row['16'];
					 $data['grafik2'][]=(string)$row['17'];
					 $data['grafik2'][]=(string)$row['18'];
					 $data['grafik2'][]=(string)$row['19'];
					 $data['grafik2'][]=(string)$row['20'];
					 $data['grafik2'][]=(string)$row['21'];
					 $data['grafik2'][]=(string)$row['22'];
					 $data['grafik2'][]=(string)$row['23'];
					 $data['grafik2'][]=(string)$row['24'];
					 $data['grafik2'][]=(string)$row['25'];
					 $data['grafik2'][]=(string)$row['26'];
					 $data['grafik2'][]=(string)$row['27'];
					 $data['grafik2'][]=(string)$row['28'];
					 $data['grafik2'][]=(string)$row['29'];
					 $data['grafik2'][]=(string)$row['30'];
					 $data['grafik2'][]=(string)$row['31'];
				}
			$data['tempat'] = $this->m_kabupaten->get($id_kabupaten);
			$data['tempat2'] = $data['tempat'][0];
		}
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
		
		$data['pokok']= $this->m_pokok->getpokok();
		$data['strategis']= $this->m_strategis->getStrategis();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('grafik/grafik_bulanan/grafik_bulanan_result', $data);
        $this->load->view('footer');
	}
	
	public function triwulan(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
		
		$data['pokok']= $this->m_pokok->getpokok();
		$data['strategis']= $this->m_strategis->getStrategis();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('grafik/grafik_triwulan/grafik_triwulan', $data);
        $this->load->view('footer');
	}
	
	public function triwulan_result(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['bulan'] = $this->input->post("bulan");
		
		if($this->input->post("bulan")=="1"){
			$data["bulan1"]="1";
			$data["bulan2"]="2";
			$data["bulan3"]="3";
		}else if($this->input->post("bulan")=="2"){
			$data["bulan1"]="4";
			$data["bulan2"]="5";
			$data["bulan3"]="6";
		}else if($this->input->post("bulan")=="3"){
			$data["bulan1"]="7";
			$data["bulan2"]="8";
			$data["bulan3"]="9";
		}else{
			$data["bulan1"]="10";
			$data["bulan2"]="11";
			$data["bulan3"]="12";
		}
		
		$data['tahun'] = $this->input->post("tahun");
		$id_pasar = $this->input->post("id_pasar");
		$id_kabupaten = $this->input->post("id_kabupaten");
		
		if($this->input->post("bahan_pokok")==""){
			$data['jenis_komoditi'] = $this->input->post("bahan_strategis");
			$data['jenis_komoditi2']= $this->m_komoditi->get($data['jenis_komoditi']);
			$data['jenis_komoditi2']= $data['jenis_komoditi2'][0];
			
		}else if($this->input->post("bahan_strategis")==""){
			$data['jenis_komoditi'] = $this->input->post("bahan_pokok");
			$data['jenis_komoditi2']= $this->m_komoditi->get($data['jenis_komoditi']);
			$data['jenis_komoditi2']= $data['jenis_komoditi2'][0];
		}else{
			$data['jenis_komoditi'] = $this->input->post("bahan_pokok");
			$data['jenis_komoditi2']= $this->m_komoditi->get($data['jenis_komoditi']);
			$data['jenis_komoditi2']= $data['jenis_komoditi2'][0];
		}
		
		
		
		if($this->input->post("id_kabupaten")==""){
				
				foreach($this->m_grafik->grafik_triwulan_pasar($data['bulan1'],$data['bulan2'],$data['bulan3'],$data['tahun'],$id_pasar,$data['jenis_komoditi'])->result_array() as $row)
				{
					 $data['grafik'][]=(double)$row['1'];
					 $data['grafik'][]=(double)$row['2'];
					 $data['grafik'][]=(double)$row['3'];
				}
				
				foreach($this->m_grafik->grafik_triwulan_tanggal($data['bulan1'],$data['bulan2'],$data['bulan3'],$data['tahun'])->result_array() as $row)
				{
					 $data['grafik2'][]=(string)$row['1'];
					 $data['grafik2'][]=(string)$row['2'];
					 $data['grafik2'][]=(string)$row['3'];
				}
				
			$data['tempat'] = $this->m_pasar->get($id_pasar);
			$data['tempat2'] = $data['tempat'][0];
		} 
		else if($this->input->post("id_pasar")==""){
			foreach($this->m_grafik->grafik_triwulan_kabupaten($data['bulan1'],$data['bulan2'],$data['bulan3'],$data['tahun'],$id_kabupaten,$data['jenis_komoditi'])->result_array() as $row)
				{
					 $data['grafik'][]=(double)$row['1'];
					 $data['grafik'][]=(double)$row['2'];
					 $data['grafik'][]=(double)$row['3'];
				}
				
				foreach($this->m_grafik->grafik_triwulan_tanggal($data['bulan1'],$data['bulan2'],$data['bulan3'],$data['tahun'])->result_array() as $row)
				{
					 $data['grafik2'][]=(string)$row['1'];
					 $data['grafik2'][]=(string)$row['2'];
					 $data['grafik2'][]=(string)$row['3'];
				}
			$data['tempat'] = $this->m_kabupaten->get($id_kabupaten);
			$data['tempat2'] = $data['tempat'][0];
		}else{
			foreach($this->m_grafik->grafik_triwulan_kabupaten($data['bulan1'],$data['bulan2'],$data['bulan3'],$data['tahun'],$id_kabupaten,$data['jenis_komoditi'])->result_array() as $row)
				{
					 $data['grafik'][]=(double)$row['1'];
					 $data['grafik'][]=(double)$row['2'];
					 $data['grafik'][]=(double)$row['3'];
				}
				
				foreach($this->m_grafik->grafik_triwulan_tanggal($data['bulan1'],$data['bulan2'],$data['bulan3'],$data['tahun'])->result_array() as $row)
				{
					 $data['grafik2'][]=(string)$row['1'];
					 $data['grafik2'][]=(string)$row['2'];
					 $data['grafik2'][]=(string)$row['3'];
				}
			$data['tempat'] = $this->m_kabupaten->get($id_kabupaten);
			$data['tempat2'] = $data['tempat'][0];
		}
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
		
		$data['pokok']= $this->m_pokok->getpokok();
		$data['strategis']= $this->m_strategis->getStrategis();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('grafik/grafik_triwulan/grafik_triwulan_result', $data);
        $this->load->view('footer');
	}
	
	public function tahunan(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
	
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
		
		$data['pokok']= $this->m_pokok->getpokok();
		$data['strategis']= $this->m_strategis->getStrategis();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('grafik/grafik_tahunan/grafik_tahunan', $data);
        $this->load->view('footer');
	}
	
	public function tahunan_result(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
			
		$data['tahun1']= $this->input->post('tahun1');
		$data['tahun2']= $this->input->post('tahun2');
		
		/*for($i = $data['tahun1']; $i <= $data['tahun2']; $i++){
			echo $i."<br>";
			
		}*/
		
		$id_pasar = $this->input->post("id_pasar");
		$id_kabupaten = $this->input->post("id_kabupaten");
		
		if($this->input->post("bahan_pokok")==""){
			$data['jenis_komoditi'] = $this->input->post("bahan_strategis");
			$data['jenis_komoditi2']= $this->m_komoditi->get($data['jenis_komoditi']);
			$data['jenis_komoditi2']= $data['jenis_komoditi2'][0];
			
		}else if($this->input->post("bahan_strategis")==""){
			$data['jenis_komoditi'] = $this->input->post("bahan_pokok");
			$data['jenis_komoditi2']= $this->m_komoditi->get($data['jenis_komoditi']);
			$data['jenis_komoditi2']= $data['jenis_komoditi2'][0];
		}else{
			$data['jenis_komoditi'] = $this->input->post("bahan_pokok");
			$data['jenis_komoditi2']= $this->m_komoditi->get($data['jenis_komoditi']);
			$data['jenis_komoditi2']= $data['jenis_komoditi2'][0];
		}
		
		if($this->input->post("id_kabupaten")==""){
				
				$data['grafik'] = $this->m_grafik->grafik_tahunan_pasar($data['tahun1'],$data['tahun2'],$id_pasar,$data['jenis_komoditi']);
				$data['tempat'] = $this->m_pasar->get($id_pasar);
				$data['tempat2'] = $data['tempat'][0];
		} 
		else if($this->input->post("id_pasar")==""){
			$data['grafik'] = $this->m_grafik->grafik_tahunan_kabupaten($data['tahun1'],$data['tahun2'],$id_kabupaten,$data['jenis_komoditi']);
			$data['tempat'] = $this->m_kabupaten->get($id_kabupaten);
			$data['tempat2'] = $data['tempat'][0];
		} else{
			$data['grafik'] = $this->m_grafik->grafik_tahunan_kabupaten($data['tahun1'],$data['tahun2'],$id_kabupaten,$data['jenis_komoditi']);
			$data['tempat'] = $this->m_kabupaten->get($id_kabupaten);
			$data['tempat2'] = $data['tempat'][0];
		}
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
		
		$data['pokok']= $this->m_pokok->getpokok();
		$data['strategis']= $this->m_strategis->getStrategis();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('grafik/grafik_tahunan/grafik_tahunan_result', $data);
        $this->load->view('footer');
	}
	
}
