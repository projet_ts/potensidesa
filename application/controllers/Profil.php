<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
        
        $this->load->model('m_front');
        $this->load->model('m_log');
        $this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_login');
		$this->load->model('m_profil');
		$this->load->model('m_berita');
		$as=$this->session->userdata('userid');
		
		$this->load->helper(array('url', 'html', 'text', 'ckeditor', 'date'));
        $this->load->library(array('form_validation','session','encrypt'));
        
        $this->data['ckeditor1'] = array(		
	    //ID of the textarea that will be replaced
	    'id' 	=> 	'content1',
	    'path'	=>	'asset/ckeditor',
            'config'    =>      array(
		'toolbar' => 	array(	//Setting a custom toolbar
		    array('Source','Maximize','ShowBlocks'),
                    array('Styles','Format','FontSize','Font'),
                    array('Cut','Copy','Paste'),
                    array('Save','Preview','Print','Templates'),
                    array('Bold','Italic','Underline','Strike','Subscript','Superscript'),
                    array('Undo','Redo'),
                    array('Find','Replace'),
                    array('NumberedList','BulletedList','Outdent','Indent','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'),                    
                    array('Link','Unlink','Anchor','Table','Image','Smiley'),
		    '/'
		)
	    )
        );
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Industri")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('forbidden');
			$this->load->view('footer');
		}
	}
	
   
	public function data(){
	
		//config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/profil/data";
		$config["total_rows"] = $this->m_profil->record_count();
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		
		$data["profil"] = $this->m_profil->fetch_profil($config["per_page"], $page);	
		$data["links"] = $this->pagination->create_links();
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('module_frontpage/profil/profil', $data);
        $this->load->view('footer');
		
	}
	
	public function result(){
	
		if(!$this->input->post('key')){
				redirect('berita/data');
		}else{
		
				$data["berita"] = $this->m_berita->search_berita($this->input->post('key'));
				$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
				$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
				$data["links"] = "";
				$this->load->view('header');
				$this->load->view('notification');
				$this->load->view('menu', $data);
				$this->load->view('module_frontpage/berita/berita', $data);
				$this->load->view('footer');
		}
    }
	
	
	public function update(){
	
		$data['editor']=$this->data;
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_profil->get($this->input->get('id'));
			
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('profil/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('module_frontpage/profil/update', $data);
            $this->load->view('footer');
        }
	}
	
	public function detail(){
	
		$data['editor']=$this->data;
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_berita->get($this->input->get('id'));
			
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('berita/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('module_frontpage/berita/detail', $data);
            $this->load->view('footer');
        }
	}
	
	public function update_data(){
	
		$filename = $this->input->post('userfile');
        $config['upload_path'] = "./files/profil_image/";
        $config['allowed_types'] = "png|jpg";
        $config['overwrite']="true";
        $config['max_size']="20000000";
        $config['file_name'] = $this->input->post('judul');
        $this->load->library('upload', $config);
		
		if(!$this->upload->do_upload())
        {
				 //get data
				$data['id_profil'] = $this->input->post('id_profil');
				$data['judul'] = $this->input->post('judul');
				$data['content'] = $this->input->post('content');
				
				//call function
				$this->m_profil->updatenoimage($data);
				
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Update Data Profil dengan id_profil = ".$data['id_profil']);
				
		}else{
				 //get data
				$dat = $this->upload->data();
				$data['gambar'] = $dat['file_name'];
				
				$data['id_profil'] = $this->input->post('id_profil');
				$data['judul'] = $this->input->post('judul');
				$data['content'] = $this->input->post('content');
				
				//call function
				$this->m_profil->update($data);
				
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Update Data Profil dengan id_profil = ".$data['id_profil']);
        
		}
        //redirect to page
        redirect('profil/data');
	}
	
       
}