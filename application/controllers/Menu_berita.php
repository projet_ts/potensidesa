<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_berita extends CI_Controller {
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_berita');
        
		// Memanggil library
		$this->load->library('recaptcha');

		
				
	}
	
	public function berita() {
		
		$data['berita'] =  $this->m_berita->get($this->uri->segment(3));
		$data['berita'] = $data['berita'][0];
		// Mendapatkan input recaptcha dari user
		$captcha_answer = $this->input->post('g-recaptcha-response');

		// Verifikasi input recaptcha dari user
		$response = $this->recaptcha->verifyResponse($captcha_answer);

		// Proses
		if ($response['success']) {
			// Code jika sukses
		} else {
			// Code jika gagal
		}
		$this->load->view('frontpage/header');
        $this->load->view('frontpage/nav2');
        $this->load->view('frontpage/berita/berita', $data);
        $this->load->view('frontpage/footer');
		
    }
	
       
}