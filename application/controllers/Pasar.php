<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//controller for modul sistem perindustrian

class Pasar extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_pelabuhan');
		$this->load->model('m_pasar');
		$this->load->model('m_kabupaten');
		$this->load->model('m_login');
		$as=$this->session->userdata('userid');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Industri")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('forbidden');
			$this->load->view('footer');
		}
    }

    public function data(){
	
		$id_kabupaten = $this->session->userdata('id_kabupaten');
		
		 //config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/pasar/data";
		
		if($this->session->userdata('group_id')=="group1000"){
			$config["total_rows"] = $this->m_pasar->record_count();
		}else{
			$config["total_rows"] = $this->m_pasar->record_count2($this->session->userdata('id_kabupaten'));
		}
		
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		
		if($this->session->userdata('group_id')=="group1000"){
			$data["pasar"] = $this->m_pasar->fetch_pasar($config["per_page"], $page);
		}else{
			$data["pasar"] = $this->m_pasar->fetch_pasar2($config["per_page"], $page, $this->session->userdata('id_kabupaten'));
		}
		
		$data["links"] = $this->pagination->create_links();
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('industri/module_pasar/pasar', $data);
        $this->load->view('footer');
		
		
    }
	
	public function result(){
       
	   $id_kabupaten = $this->session->userdata('id_kabupaten');
	
		//config for pagination
		$search = ($this->input->post("key"))? $this->input->post("key") : "NIL";
		$search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;
				
		$config = array();
		$config["base_url"] = base_url()."pasar/result/".$search;
		
		if($this->session->userdata('group_id')=="group1000"){
			$config["total_rows"] = $this->m_pasar->record_count3($search);
		}else{
			$config["total_rows"] = $this->m_pasar->record_count4($this->session->userdata('id_kabupaten'), $search);
		}
				
		$config["per_page"] = 30;
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
	
		if($this->uri->segment(4)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(4);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4))?$this->uri->segment(4) : 0;
				
		if($this->session->userdata('group_id')=="group1000"){
			$data["pasar"] = $this->m_pasar->search_pasar($config["per_page"], $page, $search);
		}else{
			$data["pasar"] = $this->m_pasar->search_pasar2($config["per_page"], $page, $search, $this->session->userdata('id_kabupaten'));
		}
				
		$data["links"] = $this->pagination->create_links();
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('header');
		$this->load->view('notification');
		$this->load->view('menu', $data);
		$this->load->view('industri/module_pasar/pasar', $data);
		$this->load->view('footer');		
	
    }
	
    public function insert(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('industri/module_pasar/insert');
        $this->load->view('footer');
    }
    
    public function update(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_pasar->get($this->input->get('id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('pelabuhan/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('industri/module_pasar/update', $data);
            $this->load->view('footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_pelabuhan->get($this->input->get('id'));
		$data['kabupaten'] = $this->m_kabupaten->data();
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('pelabuhan/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('industri/module_pelabuhan/detail', $data);
            $this->load->view('footer');
        }
    }
    
    public function create_data() {
        //get data
		$data['nama_pasar'] = $this->input->post('nama_pasar');
		$data['keterangan'] = $this->input->post('keterangan');
		$data['id_kabupaten'] = $this->input->post('id_kabupaten');
        
        //call function
        $this->m_pasar->create($data);
        
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Insert Data Pasar");
		
        //redirect to page
        redirect('pasar/data');
    }
    
    public function update_data() {
        //get data
		$data['id_pasar'] = $this->input->post('id_pasar');
		$data['nama_pasar'] = $this->input->post('nama_pasar');
		$data['keterangan'] = $this->input->post('keterangan');
		$data['id_kabupaten'] = $this->input->post('id_kabupaten');
        
        //call function
        $this->m_pasar->update($data);
		
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Update Data Pasar dengan id_pasar = ".$data['id_pasar']);
        
        //redirect to page
        redirect('pasar/data');
        
    }
    
    public function delete() {
                
        if($this->input->get('id')!="") {
            $this->m_pasar->delete($this->input->get('id'));
			
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Hapus Data Pasar dengan id_pasar = ".$this->input->get('id'));
        }
        
        //redirect to page
        redirect('pasar/data');
        
    }
      
}
