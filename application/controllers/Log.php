<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
        error_reporting(0);
        $this->load->model('m_log');
		$this->load->model('m_menu');
		
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
    }

    public function data(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data["log"]=$this->m_log->data();
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('module_log/group', $data);
        $this->load->view('footer');
    }

    public function detail(){
    
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_log->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('group/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('module_log/detail', $data);
            $this->load->view('footer');
        }
    }
    
    
         
}
