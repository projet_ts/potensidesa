<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_profil extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_user_profil');
		$this->load->model('m_menu');
		$this->load->model('m_group');
		$this->load->model('m_instansi');
		$this->load->model('m_login');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Setting")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('forbidden');
			$this->load->view('footer');
		}
		
		
    }

    public function update(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_user_profil->get($this->session->userdata('userid'));
		$data['group'] = $this->m_group->data_active();
		//$data["instansi"]=$this->m_instansi->data();
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            //redirect('home/');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('module_user/user_profil', $data);
            $this->load->view('footer');
        }
    }
    
    public function update_data() {
        
        //get data
        $data['userid'] = $this->input->post('userid');
		$data['group_id'] = $this->input->post('group_id');
        $data['username'] = $this->input->post('username');
		$data['fullname'] = $this->input->post('fullname');
		$data['email'] = $this->input->post('email');
		
		//set up password field
		$oldpassword = $this->input->post('oldpassword');
		$checkoldpassword = $this->input->post('checkoldpassword');
		$password = md5($this->input->post('password'));
		$matchpassword = md5($this->input->post('matchpassword'));
		
		if($checkoldpassword=="") {
			//call function
			$this->m_user_profil->update_nopassword($data);
		} else {
			
			$checkoldpassword = md5($checkoldpassword);
			
			//call function
			$data['password'] = $password;
			$this->m_user_profil->update_withpassword($data);		
			
		}
		
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Update Data User dengan userid = ".$data['userid']);
		
		$session = [
                    'userid' => $this->input->post('userid'),
                    'username' => $this->input->post('username'),
					'fullname' => $this->input->post('fullname'),
                    'group_id' => $this->input->post('group_id'),
					'photo' => $this->input->post('photo')
                ];
        $this->session->set_userdata($session);
		
		
		$config['upload_path'] = "./asset/img/";
        $config['allowed_types'] = "gif|jpg|png";
        $config['overwrite']="true";
        $config['max_size']="20000";
        $config['file_name'] = $this->input->post('userid');
        $this->load->library('upload', $config);
		
		if(!$this->upload->do_upload()){
            redirect('home/');
        
        }else {
            
            $dat = $this->upload->data();
            $data['file'] = $dat['file_name'];
                        
            $this->m_user_profil->tambah_photo($data);
        }
		
		
		//redirect to page
		redirect('home/');
		
    }
      
}