<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//controller for modul sistem industri

class Layanan extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_layanan');
		$this->load->model('m_login');
		
		$this->load->helper(array('url', 'html', 'text', 'ckeditor', 'date'));
        $this->load->library(array('form_validation','session'));
		
		$this->data['ckeditor1'] = array(		
	    //ID of the textarea that will be replaced
	    'id' 	=> 	'deskripsi',
	    'path'	=>	'asset/ckeditor',
            'config'    =>      array(
		'toolbar' => 	array(	//Setting a custom toolbar
		    array('Source','Maximize','ShowBlocks'),
                    array('Styles','Format','FontSize','Font'),
                    array('Cut','Copy','Paste'),
                    array('Save','Preview','Print','Templates'),
                    array('Bold','Italic','Underline','Strike','Subscript','Superscript'),
                    array('Undo','Redo'),
                    array('Find','Replace'),
                    array('NumberedList','BulletedList','Outdent','Indent','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'),                    
                    array('Link','Unlink','Anchor','Table','Image','Smiley'),
		    '/'
		)
	    )
        );
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		
    }

    public function data(){
		 //config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/layanan/data";
		$config["total_rows"] = $this->m_layanan->record_count();
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		$data["layanan"] = $this->m_layanan->fetch_layanan($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('service/module_layanan/layanan', $data);
        $this->load->view('admin/footer');
    }
	
	public function result(){
        
		//config for pagination
		$search = ($this->input->post("key"))? $this->input->post("key") : "NIL";
		$search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;
				
		$config = array();
		$config["base_url"] = base_url()."layanan/result/".$search;
		$config["total_rows"] = $this->m_layanan->record_count_search($search);
		$config["per_page"] = 30;
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
	
		if($this->uri->segment(4)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(4);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4))?$this->uri->segment(4) : 0;
				
		$data["layanan"] = $this->m_layanan->search_layanan($config["per_page"], $page, $search);
		$data["links"] = $this->pagination->create_links();
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('admin/header');
		$this->load->view('admin/notification');
		$this->load->view('admin/menu', $data);
		$this->load->view('service/module_layanan/layanan', $data);
		$this->load->view('admin/footer');	
		
    }
	
    public function insert(){
	
		$data['editor']=$this->data;
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
				
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('service/module_layanan/insert');
        $this->load->view('admin/footer');
    }
    
    public function update(){
	
		$data['editor']=$this->data;
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_layanan->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('layanan/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('service/module_layanan/update', $data);
            $this->load->view('admin/footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_layanan->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('layanan/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('service/module_layanan/detail', $data);
            $this->load->view('admin/footer');
        }
    }
    
    public function create_data() {
        //get data
		$data['id_layanan'] = $this->input->post('id_layanan');
		$data['jenis_layanan'] = $this->input->post('jenis_layanan');
		$data['judul'] = $this->input->post('judul');
		$data['deskripsi'] = $this->input->post('deskripsi');
        
		//call function
        $this->m_layanan->create($data);
        
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Insert Data layanan");
		
        //redirect to page
        redirect('layanan/data');
        
    }
    
    public function update_data() {
        
        //get data
		$data['id_layanan'] = $this->input->post('id_layanan');
		$data['jenis_layanan'] = $this->input->post('jenis_layanan');
		$data['judul'] = $this->input->post('judul');
		$data['deskripsi'] = $this->input->post('deskripsi');
        
        //call function
        $this->m_layanan->update($data);
		
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Update Data layanan dengan id_layanan = ".$data['id_layanan']);
        
        //redirect to page
        redirect('layanan/data');
        
    }
    
    public function delete() {
        if($this->input->get('id')!="") {
            $this->m_layanan->delete($this->input->get('id'));
			
			//log system
			//$this->m_log->create($this->session->userdata('userid'), 
				//"Hapus Data komoditi dengan id_komoditi = ".$this->input->get('id'));
        }
        
        //redirect to page
        redirect('layanan/data');
        
    }
      
}
