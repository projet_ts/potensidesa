<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_jasakip extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_jasakip");
        return $query->result();
    }		
	
	public function jasakip(){
        $query = $this->db->get('table_jasakip');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->download = $data['download'];
		$this->judul = $data['judul'];
		$this->id_jenisjasakip = $data['id_jenisjasakip'];	
        
        //insert data
        $this->db->insert('table_jasakip', $this);
    }

    //upload image
	public function createnoimage($data) {
        //get data
		
		$this->judul = $data['judul'];
		$this->id_jenisjasakip = $data['id_jenisjasakip'];
		
        //insert data
        $this->db->insert('table_jasakip', $this);
    }
    
    public function update($data) {
        //get data
        
        $this->download = $data['download'];
		$this->judul = $data['judul'];
		$this->id_jenisjasakip = $data['id_jenisjasakip'];		
		
        //update data
        $this->db->update('table_jasakip', $this, array('id_jasakip'=>$data['id_jasakip']));
    }
    
    public function delete($id) {
        $this->db->delete('table_jasakip', array('id_jasakip' => $id));
    }


    public function get($id){
        $this->db->where('id_jasakip', $id);
        $query = $this->db->get('table_jasakip');
        return $query->result();
    }
	
	public function get_jenis($id){
        $this->db->where('id_jenisjasakip', $id);
        $query = $this->db->get('table_jasakip');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_jasakip");
	}
	
	public function record_count_search($key) {
		$this->db->like("gambar", $key);
		$this->db->or_like("judul", $key);
		$this->db->or_like("content", $key);
		$this->db->or_like("tanggal", $key);		
		return $this->db->count_all_results("table_berita");
	}
	
	public function fetch_jasakip($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_jasakip');
		$this->db->order_by('id_jasakip DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_jasakip($limit, $start, $key) {
	
		$this->db->like("gambar", $key);
		$this->db->or_like("judul", $key);
		$this->db->or_like("content", $key);
		$this->db->or_like("tanggal", $key);		
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_berita");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}

	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_jasakip',$id);
		$query = $getData = $this->db->get('table_jasakip');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->judul = $data['judul'];
		$this->id_jenisjasakip = $data['id_jenisjasakip'];
        
        //update data
        $this->db->update('table_jasakip', $this, array('id_jasakip'=>$data['id_jasakip']));
    }
	
}
?>