<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_komoditi_tetap extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data() {
        $query  = $this->db->query("SELECT * FROM industri_table_komoditi_tetap ");
        return $query->result();
    }
	
	 public function data2($a) {
        $query  = $this->db->query("SELECT a.*, b.* FROM industri_table_komoditi_tetap a, industri_table_jenis_komoditi b 
									WHERE a.id_jenis_komoditi=b.id_jenis_komoditi and a.id_jenis_komoditi=".$a."");
        return $query->result();
    }
	
	public function komoditi_tetap(){
        $query = $this->db->get('industri_table_komoditi_tetap');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		$this->id_komoditi_tetap = $data['id_komoditi_tetap'];
        $this->nama_komoditi_tetap = $data['nama_komoditi_tetap'];
		$this->satuan = $data['satuan'];
		$this->id_jenis_komoditi = $data['id_jenis_komoditi'];
        
        //insert data
        $this->db->insert('industri_table_komoditi_tetap', $this);
    }
    
    public function update($data) {
        //get data
        $this->id_komoditi_tetap = $data['id_komoditi_tetap'];
        $this->nama_komoditi_tetap = $data['nama_komoditi_tetap'];
		$this->satuan = $data['satuan'];
		$this->id_jenis_komoditi = $data['id_jenis_komoditi'];
		
        //update data
        $this->db->update('industri_table_komoditi_tetap', $this, array('id_komoditi_tetap'=>$data['id_komoditi_tetap']));
    }
    
    public function delete($id) {
        $this->db->delete('industri_table_komoditi_tetap', array('id_komoditi_tetap' => $id));
    }


    public function get($id){
        $this->db->where('id_komoditi_tetap', $id);
		$this->db->where('a.id_jenis_komoditi=b.id_jenis_komoditi');
        $query = $this->db->get('industri_table_komoditi_tetap a, industri_table_jenis_komoditi b');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("industri_table_komoditi_tetap");
	}
	
	public function record_count_search($key) {
			return $this->db->count_all("industri_table_komoditi_tetap a, industri_table_jenis_komoditi b 
										 WHERE	a.id_jenis_komoditi=b.id_jenis_komoditi
										 AND  (	a.nama_komoditi_tetap LIKE '%".$key."%' ESCAPE '!' 
												OR a.satuan LIKE '%".$key."%' ESCAPE '!'
												OR b.nama_jenis_komoditi LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function fetch_komoditi_tetap($limit, $start) {
		$this->db->select('a.*, b.*');
		$this->db->from('industri_table_komoditi_tetap a, industri_table_jenis_komoditi b');
		$this->db->where('a.id_jenis_komoditi=b.id_jenis_komoditi');
		$this->db->order_by('a.id_komoditi_tetap DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_komoditi_tetap($limit, $start, $key) {
		
		$this->db->select('a.*, b.*');
		$this->db->from('industri_table_komoditi_tetap a, industri_table_jenis_komoditi b ');
		$this->db->where("a.id_jenis_komoditi=b.id_jenis_komoditi
										 AND  (	a.nama_komoditi_tetap LIKE '%".$key."%' ESCAPE '!' 
												OR a.satuan LIKE '%".$key."%' ESCAPE '!'
												OR b.nama_jenis_komoditi LIKE '%".$key."%' ESCAPE '!')
						");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>