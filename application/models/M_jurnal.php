<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_jurnal extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_jurnal");
        return $query->result();
    }		
	
	public function jurnal(){
        $query = $this->db->get('table_jurnal');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->download = $data['download'];
		$this->judul = $data['judul'];
		$this->edisi = $data['edisi'];
		$this->penyusun = $data['penyusun'];
		$this->abstrak = $data['abstrak'];
		$this->tahun = $data['tahun'];
		$this->kata_kunci = $data['katakunci'];
		$this->id_jenisjurnal = $data['id_jenisjurnal'];	
        
        //insert data
        $this->db->insert('table_jurnal', $this);
    }

    //upload image
	public function createnoimage($data) {
        //get data
		
		$this->judul = $data['judul'];
		$this->edisi = $data['edisi'];
		$this->penyusun = $data['penyusun'];
		$this->abstrak = $data['abstrak'];
		$this->tahun = $data['tahun'];
		$this->kata_kunci = $data['katakunci'];
		$this->id_jenisjurnal = $data['id_jenisjurnal'];
		
        //insert data
        $this->db->insert('table_jurnal', $this);
    }
    
    public function update($data) {
        //get data
        
        $this->download = $data['download'];
		$this->judul = $data['judul'];
		$this->edisi = $data['edisi'];
		$this->penyusun = $data['penyusun'];
		$this->abstrak = $data['abstrak'];
		$this->tahun = $data['tahun'];
		$this->kata_kunci = $data['katakunci'];
		$this->id_jenisjurnal = $data['id_jenisjurnal'];		
		
        //update data
        $this->db->update('table_jurnal', $this, array('id_jurnal'=>$data['id_jurnal']));
    }
    
    public function delete($id) {
        $this->db->delete('table_jurnal', array('id_jurnal' => $id));
    }


    public function get($id){
        $this->db->where('id_jurnal', $id);
        $query = $this->db->get('table_jurnal');
        return $query->result();
    }
	
	public function get_jenis($id){
        $this->db->where('id_jenisjurnal', $id);
        $query = $this->db->get('table_jurnal');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_jurnal");
	}
	
	public function record_count_search($key) {
		$this->db->like("gambar", $key);
		$this->db->or_like("judul", $key);
		$this->db->or_like("content", $key);
		$this->db->or_like("tanggal", $key);		
		return $this->db->count_all_results("table_berita");
	}
	
	public function fetch_jurnal($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_jurnal');
		$this->db->order_by('id_jurnal DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_jurnal($limit, $start, $key) {
	
		$this->db->like("gambar", $key);
		$this->db->or_like("judul", $key);
		$this->db->or_like("content", $key);
		$this->db->or_like("tanggal", $key);		
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_berita");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}

	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_jurnal',$id);
		$query = $getData = $this->db->get('table_jurnal');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->judul = $data['judul'];
		$this->edisi = $data['edisi'];
		$this->penyusun = $data['penyusun'];
		$this->abstrak = $data['abstrak'];
		$this->tahun = $data['tahun'];
		$this->kata_kunci = $data['katakunci'];
		$this->id_jenisjurnal = $data['id_jenisjurnal'];
        
        //update data
        $this->db->update('table_jurnal', $this, array('id_jurnal'=>$data['id_jurnal']));
    }
	
}
?>