<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kelembagaan extends CI_Model{

    function __construct() {
        parent::__construct();
    }

	public function data($id) {
    $query  = $this->db->query("SELECT * FROM table_desa d, table_kelembagaan g
			WHERE d.desa_id=g.desa_id AND d.desa_id='$id'");
        return $query->result();
	}

  public function create($data) {
        //get data
        $this->desa_id = $data['desa_id'];
        $this->kelembagaan_id = $data['kelembagaan_id'];
        $this->nama_kelembagaan = $data['nama_kelembagaan'];
        $this->jenis_kelembagaan = $data['jenis_kelembagaan'];
        $this->alamat = $data['alamat'];
        $this->tahun = $data['tahun'];

        //insert data
        $this->db->insert('table_kelembagaan', $this);
    }

    public function update($data) {
        //get data
        $this->desa_id = $data['desa_id'];
        $this->kelembagaan_id = $data['kelembagaan_id'];
        $this->nama_kelembagaan = $data['nama_kelembagaan'];
        $this->jenis_kelembagaan = $data['jenis_kelembagaan'];
        $this->alamat = $data['alamat'];
        $this->tahun = $data['tahun'];

        //update data
        $this->db->update('table_kelembagaan', $this, array('desa_id'=>$data['desa_id'], 'kelembagaan_id'=>$data['kelembagaan_id']));
    }

    public function delete($id1, $id2) {
        $this->db->delete('table_kelembagaan', array('desa_id'=>$id1, 'kelembagaan_id'=>$id2));
    }


    public function get($id1, $id2){
        $this->db->where('desa_id', $id1);
		    $this->db->where('kelembagaan_id', $id2);
        $query = $this->db->get('table_kelembagaan', 1);
        return $query->result();
    }

	public function record_count($id) {
		$this->db->where("desa_id", $id);
		return $this->db->count_all_results("table_kelembagaan");
	}

	public function record_count_search($key, $id) {
		$this->db->where("desa_id", $id);
		$this->db->like("kelembagaan_id", $key);
		$this->db->or_like("jumlah", $key);
		$this->db->or_like("kelamin", $key);
		$this->db->or_like("tahun", $key);
		return $this->db->count_all_results("table_kelembagaan");
	}

	public function fetch_kelembagaan($limit, $start, $id) {
		$this->db->select('*');
		$this->db->from('table_kelembagaan j');
		$this->db->join('table_kelamin p', 'p.id_kelamin=j.kelembagaan_id');
		$this->db->where("j.kelembagaan_id", $id);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function search_kelembagaan($limit, $start, $key, $id) {
		$this->db->where("kelembagaan_id", $id);
		$this->db->like("desa_id", $key);
		$this->db->or_like("jumlah", $key);
		$this->db->or_like("kelamin", $key);
		$this->db->or_like("tahun", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_kelembagaan");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
	}
}
?>
