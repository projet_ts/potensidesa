<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindutrian

class M_pasar extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data() {
        $query  = $this->db->query("SELECT * FROM industri_table_pasar a, industri_table_kabupaten b WHERE a.id_kabupaten = b.id_kabupaten");
        return $query->result();
    }
	
    public function data2($id_kabupaten) {
        $query  = $this->db->query("SELECT * FROM industri_table_pasar a, industri_table_kabupaten b WHERE a.id_kabupaten = b.id_kabupaten AND b.id_kabupaten='$id_kabupaten'");
        return $query->result();
    }
	
	public function pasar(){
        $query = $this->db->get('industri_table_pasar');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		$this->nama_pasar = $data['nama_pasar'];
		$this->keterangan = $data['keterangan'];
        $this->id_kabupaten = $data['id_kabupaten'];
		
        //insert data
        $this->db->insert('industri_table_pasar', $this);
    }
    
    public function update($data) {
        //get data
        $this->id_pasar = $data['id_pasar'];
		$this->nama_pasar = $data['nama_pasar'];
		$this->keterangan = $data['keterangan'];
        $this->id_kabupaten = $data['id_kabupaten'];
        
        //update data
        $this->db->update('industri_table_pasar', $this, array('id_pasar'=>$data['id_pasar']));
    }
    
    public function delete($id) {
        $this->db->delete('industri_table_pasar', array('id_pasar' => $id));
    }

    public function get($id){
        $this->db->where('id_pasar', $id);
        $query = $this->db->get('industri_table_pasar');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("industri_table_pasar a, industri_table_kabupaten b 
											WHERE a.id_kabupaten=b.id_kabupaten 
										AND b.id_kabupaten != '1'");
	}
	
	public function record_count2($id_kabupaten) {
			return $this->db->count_all("industri_table_pasar a, industri_table_kabupaten b 
										 WHERE a.id_kabupaten=b.id_kabupaten
										 AND b.id_kabupaten != '1' AND b.id_kabupaten='".$id_kabupaten."'");
	}
	
	public function record_count3($key) {
			return $this->db->count_all("industri_table_pasar a, industri_table_kabupaten b 
										 WHERE	a.id_kabupaten=b.id_kabupaten
										 AND b.id_kabupaten != '1'
										 AND  (	a.nama_pasar LIKE '%".$key."%' ESCAPE '!' 
												OR a.keterangan LIKE '%".$key."%' ESCAPE '!'
												OR b.nama_kabupaten LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function record_count4($id_kabupaten, $key) {
			return $this->db->count_all("industri_table_pasar a, industri_table_kabupaten b 
										 WHERE	a.id_kabupaten=b.id_kabupaten
										 AND b.id_kabupaten != '1'
										 AND b.id_kabupaten='".$id_kabupaten."'
										 AND  (	a.nama_pasar LIKE '%".$key."%' ESCAPE '!' 
												OR a.keterangan LIKE '%".$key."%' ESCAPE '!'
												OR b.nama_kabupaten LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function fetch_pasar($limit, $start) {
		$this->db->select('a.*, b.nama_kabupaten');
		$this->db->from('industri_table_pasar a, industri_table_kabupaten b');
		$this->db->where('a.id_kabupaten=b.id_kabupaten');
		$this->db->order_by("id_pasar DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetch_pasar2($limit, $start, $id_kabupaten) {
		$this->db->select('a.*, b.nama_kabupaten');
		$this->db->from('industri_table_pasar a, industri_table_kabupaten b');
		$this->db->where('a.id_kabupaten=b.id_kabupaten');
		$this->db->where("b.id_kabupaten='$id_kabupaten'");
		$this->db->order_by("id_pasar DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_pasar($limit, $start, $key) {
		$this->db->select('a.*, b.nama_kabupaten');
		$this->db->from('industri_table_pasar a, industri_table_kabupaten b');
		$this->db->where("b.id_kabupaten != '1'");
		$this->db->where("a.id_kabupaten=b.id_kabupaten
							AND  (	a.nama_pasar LIKE '%".$key."%' ESCAPE '!' 
									OR a.keterangan LIKE '%".$key."%' ESCAPE '!'
									OR b.nama_kabupaten LIKE '%".$key."%' ESCAPE '!')
						");
		$this->db->order_by("id_pasar DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
	public function search_pasar2($limit, $start, $key, $id_kabupaten) {
		$this->db->select('a.*, b.nama_kabupaten');
		$this->db->from('industri_table_pasar a, industri_table_kabupaten b');
		$this->db->where("b.id_kabupaten != '1'");
		$this->db->where("a.id_kabupaten=b.id_kabupaten
							AND b.id_kabupaten='".$id_kabupaten."'
							AND (	a.nama_pasar LIKE '%".$key."%' ESCAPE '!' 
									OR a.keterangan LIKE '%".$key."%' ESCAPE '!'
									OR b.nama_kabupaten LIKE '%".$key."%' ESCAPE '!')
						");
		$this->db->order_by("id_pasar DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>