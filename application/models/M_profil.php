<?php
class M_profil extends Ci_Model{
	
	 function __construct() {
        parent::__construct();
    }
	
	function profil(){
		$query = $this->db->query("SELECT * FROM table_profil where id_profil='1'");
		return $query->result();
	}
	
	function visi_misi(){
		$query = $this->db->query("SELECT * FROM table_profil where id_profil='2'");
		return $query->result();
	}
	
	 public function update($data) {
         //get data
		$this->id_profil = $data['id_profil'];
		$this->judul = $data['judul'];
		$this->content = $data['content'];
		$this->gambar = $data['gambar'];
        
        //update data
        $this->db->update('table_profil', $this, array('id_profil'=>$data['id_profil']));
    }
	
	 public function updatenoimage($data) {
         //get data
		$this->id_profil = $data['id_profil'];
		$this->judul = $data['judul'];
		$this->content = $data['content'];
        
        //update data
        $this->db->update('table_profil', $this, array('id_profil'=>$data['id_profil']));
    }
	
	
	
	public function get($id){
        $this->db->where('id_profil', $id);
        $query = $this->db->get('table_profil');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_profil");
	}
	
	public function fetch_profil($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_profil');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_profil($key) {
		$this->db->select('*');
		$this->db->from('table_profil');
		$this->db->where("judul LIKE '%".$key."%' ESCAPE '!' OR content LIKE '%".$key."%' ESCAPE '!'
						");
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}

?>