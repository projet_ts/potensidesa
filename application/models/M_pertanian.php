<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pertanian extends CI_Model{

    function __construct() {
        parent::__construct();
    }

	public function data($id) {
		$query  = $this->db->query("SELECT * FROM table_desa d, table_pertanian g
			WHERE d.desa_id=g.desa_id AND d.desa_id='$id'");
        return $query->result();
	}

    public function create($data) {
        //get data
        $this->desa_id = $data['desa_id'];
        $this->nama_tanaman = $data['nama_tanaman'];
        $this->jumlah = $data['jumlah'];
        $this->luas = $data['luas'];
        $this->tahun = $data['tahun'];


        //insert data
        $this->db->insert('table_pertanian', $this);
    }

    public function update($data) {
        //get data
        $this->desa_id = $data['desa_id'];
        $this->nama_tanaman = $data['nama_tanaman'];
        $this->jumlah = $data['jumlah'];
        $this->luas = $data['luas'];
        $this->tahun = $data['tahun'];


        //update data
        $this->db->update('table_pertanian', $this, array('desa_id'=>$data['desa_id'], 'pertanian_id'=>$data['pertanian_id']));
    }

    public function delete($id1, $id2) {
        $this->db->delete('table_pertanian', array('desa_id'=>$id1, 'pertanian_id'=>$id2));
    }



    public function get($id1, $id2){
        $this->db->where('desa_id', $id1);
		      $this->db->where('pertanian_id', $id2);
        $query = $this->db->get('table_pertanian', 1);
        return $query->result();
    }

	public function record_count($id) {
		$this->db->where("desa_id", $id);
		return $this->db->count_all_results("table_pertanian");
	}

	public function record_count_search($key, $id) {
		$this->db->where("desa_id", $id);
		$this->db->like("pertanian_id", $key);
		$this->db->or_like("nama_tanaman", $key);
		$this->db->or_like("jumlah", $key);
		$this->db->or_like("luas", $key);
    $this->db->or_like("tahun", $key);
    return $this->db->count_all_results("table_pertanian");
  }

	public function fetch_pertanian($limit, $start, $id) {
		$this->db->select('*');
		$this->db->from('table_desa d');
		$this->db->join('table_pertanian g', 'g.desa_id=d.desa_id');
		$this->db->where("d.desa_id", $id);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function search_pertanian($limit, $start, $key, $id) {
		$this->db->where("desa_id", $id);
		$this->db->like("pertanian_id", $key);
		$this->db->or_like("nama_tanaman", $key);
		$this->db->or_like("jumlah", $key);
		$this->db->or_like("luas", $key);
    $this->db->or_like("tahun", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_pertanian");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
	}
}
?>
