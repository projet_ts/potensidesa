<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindutrian

class M_antar_pulau extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	 public function data($id_jenis_komoditi, $id_kabupaten, $bulan, $tahun) {
        $query = $this->db->query("SELECT a.*, SUM(a.volume) as volume2, SUM(a.nilai) as nilai2, b.*,c.*, d.*
								FROM industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d 	
								WHERE a.id_komoditi_tetap=b.id_komoditi_tetap 
								AND a.id_kabupaten=c.id_kabupaten
								AND b.id_jenis_komoditi=d.id_jenis_komoditi 
								AND b.id_jenis_komoditi='".$id_jenis_komoditi."'
								AND c.nama_kabupaten='".$id_kabupaten."'
								AND MONTH(a.tanggal)='".$bulan."'
								AND YEAR(a.tanggal)='".$tahun."'
								GROUP BY a.id_komoditi_tetap");
        return $query->result();
    }
	
	 public function data2($nama_komoditi_tetap, $id_kabupaten, $tahun) {
        $query = $this->db->query("SELECT a.*, SUM(a.volume) as volume2, SUM(a.nilai) as nilai2, b.*,c.*, d.*
								FROM industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d 	
								WHERE a.id_komoditi_tetap=b.id_komoditi_tetap 
								AND a.id_kabupaten=c.id_kabupaten
								AND b.id_jenis_komoditi=d.id_jenis_komoditi 
								AND c.nama_kabupaten='".$id_kabupaten."'
								AND b.nama_komoditi_tetap='".$nama_komoditi_tetap."'
								AND YEAR(a.tanggal)='".$tahun."'
								GROUP BY a.id_komoditi_tetap");
        return $query->result();
    }
	
	
	public function jumlah($id_jenis_komoditi, $id_kabupaten, $bulan, $tahun) {
        $query = $this->db->query("SELECT SUM(a.volume) as volume, SUM(a.nilai) as nilai
								FROM industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d 	
								WHERE a.id_komoditi_tetap=b.id_komoditi_tetap 
								AND a.id_kabupaten=c.id_kabupaten
								AND b.id_jenis_komoditi=d.id_jenis_komoditi 
								AND b.id_jenis_komoditi='".$id_jenis_komoditi."'
								AND c.nama_kabupaten='".$id_kabupaten."'
								AND MONTH(a.tanggal)='".$bulan."'
								AND YEAR(a.tanggal)='".$tahun."'
								GROUP BY b.id_jenis_komoditi");
        return $query->result();
    }
	
	public function jumlah_keseluruhan($id_kabupaten, $bulan, $tahun) {
        $query = $this->db->query("SELECT SUM(a.volume) as volume, SUM(a.nilai) as nilai
								FROM industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d 	
								WHERE a.id_komoditi_tetap=b.id_komoditi_tetap 
								AND a.id_kabupaten=c.id_kabupaten
								AND b.id_jenis_komoditi=d.id_jenis_komoditi 
								AND c.nama_kabupaten='".$id_kabupaten."'
								AND MONTH(a.tanggal)='".$bulan."'
								AND YEAR(a.tanggal)='".$tahun."'
								GROUP BY a.id_kabupaten");
        return $query->result();
    }
	
	public function tanda_tangan($id_kabupaten) {
        $query = $this->db->query("SELECT *
								FROM industri_table_tanda_tangan a, industri_table_kabupaten b
								WHERE a.id_kabupaten=b.id_kabupaten
								AND b.nama_kabupaten='".$id_kabupaten."'");
        return $query->result();
    }
	
	public function cekAntarPulau($id_kabupaten, $id_pelabuhan, $tanggal) {
			return $this->db->count_all("industri_table_antar_pulau 
										WHERE id_kabupaten='$id_kabupaten' 
										AND id_pelabuhan = '$id_pelabuhan' 
										AND tanggal = '$tanggal' ");
	}
	
	
    public function create($data) {
        //get data
		//$this->id_harga_barang = $data['id_harga_barang'];
		$this->id_komoditi_tetap = $data['id_komoditi_tetap'];
		$this->id_kabupaten = $data['id_kabupaten'];
		$this->id_pelabuhan = $data['id_pelabuhan'];
        $this->tanggal =  $data['tanggal'];
		$this->volume = $data['volume'];
		$this->nilai_persatuan = $data['nilai_persatuan'];
		$this->nilai = $data['nilai'];
		
        //insert data
        $this->db->insert('industri_table_antar_pulau', $this);
    }
	
    
    public function update($data) {
        //get data
        $this->id_antar_pulau = $data['id_antar_pulau'];
		$this->volume = $data['volume'];
        $this->nilai_persatuan = $data['nilai_persatuan'];
		$this->nilai = $data['nilai'];
        
        //update data
        $this->db->update('industri_table_antar_pulau', $this, array('id_antar_pulau'=>$data['id_antar_pulau']));
    }
    
    public function delete($id) {
        $this->db->delete('industri_table_antar_pulau', array('id_antar_pulau' => $id));
    }

    public function get($id){
        $this->db->where('id_antar_pulau', $id);
        $this->db->where('a.id_kabupaten=b.id_kabupaten');
        $this->db->where('a.id_pelabuhan=c.id_pelabuhan');
        $this->db->where('a.id_komoditi_tetap=d.id_komoditi_tetap');
        $query = $this->db->get('industri_table_antar_pulau a, industri_table_kabupaten b, industri_table_pelabuhan c, industri_table_komoditi_tetap d');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d  
											WHERE a.id_komoditi_tetap=b.id_komoditi_tetap 
											AND a.id_kabupaten=c.id_kabupaten
											AND b.id_jenis_komoditi=d.id_jenis_komoditi
											AND c.id_kabupaten!='1'
										");
	}
	
	public function record_count2($id_kabupaten) {
			return $this->db->count_all("industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d  
											WHERE a.id_komoditi_tetap=b.id_komoditi_tetap 
											AND a.id_kabupaten=c.id_kabupaten
											AND b.id_jenis_komoditi=d.id_jenis_komoditi
											AND c.id_kabupaten='".$id_kabupaten."'");
	}
	
	public function record_count3($key) {
			return $this->db->count_all("industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d  
										 WHERE a.id_komoditi_tetap=b.id_komoditi_tetap 
										 AND a.id_kabupaten=c.id_kabupaten
										 AND b.id_jenis_komoditi=d.id_jenis_komoditi
										 AND c.id_kabupaten!='1'
										 AND  (	b.nama_komoditi_tetap LIKE '%".$key."%' ESCAPE '!' 
												OR d.nama_jenis_komoditi LIKE '%".$key."%' ESCAPE '!'
												OR c.nama_kabupaten LIKE '%".$key."%' ESCAPE '!'
												OR b.satuan LIKE '%".$key."%' ESCAPE '!'
												OR a.tanggal LIKE '%".$key."%' ESCAPE '!'
												OR a.volume LIKE '%".$key."%' ESCAPE '!'
												OR a.nilai_persatuan LIKE '%".$key."%' ESCAPE '!'
												OR a.nilai LIKE '%".$key."%' ESCAPE '!'
												)");
	}
	
	public function record_count4($id_kabupaten,$key) {
			return $this->db->count_all("industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d  
										 WHERE a.id_komoditi_tetap=b.id_komoditi_tetap 
										 AND a.id_kabupaten=c.id_kabupaten
										 AND b.id_jenis_komoditi=d.id_jenis_komoditi
										 AND c.id_kabupaten='".$id_kabupaten."'
										 AND  (	b.nama_komoditi_tetap LIKE '%".$key."%' ESCAPE '!' 
												OR d.nama_jenis_komoditi LIKE '%".$key."%' ESCAPE '!'
												OR c.nama_kabupaten LIKE '%".$key."%' ESCAPE '!'
												OR b.satuan LIKE '%".$key."%' ESCAPE '!'
												OR a.tanggal LIKE '%".$key."%' ESCAPE '!'
												OR a.volume LIKE '%".$key."%' ESCAPE '!'
												OR a.nilai_persatuan LIKE '%".$key."%' ESCAPE '!'
												OR a.nilai LIKE '%".$key."%' ESCAPE '!'
												)");
	}
	public function fetch_antar_pulau($limit, $start) {
		$this->db->select('a.*, b.*,c.*, d.*');
		$this->db->from('industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d ');
		$this->db->where('a.id_komoditi_tetap=b.id_komoditi_tetap');
		$this->db->where('a.id_kabupaten=c.id_kabupaten');
		$this->db->where('b.id_jenis_komoditi=d.id_jenis_komoditi');
		$this->db->where("c.id_kabupaten!='1'");
		$this->db->order_by('a.id_antar_pulau DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetch_antar_pulau2($limit, $start, $id_kabupaten) {
		$this->db->select('a.*, b.*,c.*, d.*');
		$this->db->from('industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d ');
		$this->db->where('a.id_komoditi_tetap=b.id_komoditi_tetap');
		$this->db->where('a.id_kabupaten=c.id_kabupaten');
		$this->db->where('b.id_jenis_komoditi=d.id_jenis_komoditi');
		$this->db->where("c.id_kabupaten='$id_kabupaten'");
		$this->db->order_by('a.id_antar_pulau DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_antar_pulau($id_kabupaten, $id_pelabuhan, $tanggal) {
		$this->db->select('a.*, b.*,c.*, d.*, e.*');
		$this->db->from('industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d, industri_table_pelabuhan e');
		$this->db->where("a.id_komoditi_tetap=b.id_komoditi_tetap
							AND a.id_kabupaten=c.id_kabupaten
							AND b.id_jenis_komoditi=d.id_jenis_komoditi
							AND a.id_pelabuhan=e.id_pelabuhan
							AND a.id_kabupaten='$id_kabupaten'
							AND a.id_pelabuhan='$id_pelabuhan'
							AND a.tanggal='$tanggal'
						");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
	public function search_antar_pulau2($limit, $start, $key, $id_kabupaten) {
	
		$this->db->select('a.*, b.*,c.*, d.*');
		$this->db->from('industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d');
		$this->db->where("a.id_komoditi_tetap=b.id_komoditi_tetap
							AND a.id_kabupaten=c.id_kabupaten
							AND b.id_jenis_komoditi=d.id_jenis_komoditi
							AND c.id_kabupaten='".$id_kabupaten."'
							AND  (	b.nama_komoditi_tetap LIKE '%".$key."%' ESCAPE '!' 
									OR d.nama_jenis_komoditi LIKE '%".$key."%' ESCAPE '!'
									OR c.nama_kabupaten LIKE '%".$key."%' ESCAPE '!'
									OR b.satuan LIKE '%".$key."%' ESCAPE '!'
									OR a.tanggal LIKE '%".$key."%' ESCAPE '!'
									OR a.volume LIKE '%".$key."%' ESCAPE '!'
									OR a.nilai_persatuan LIKE '%".$key."%' ESCAPE '!'
									OR a.nilai LIKE '%".$key."%' ESCAPE '!')
						");
		$this->db->order_by('a.id_antar_pulau DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
}
?>