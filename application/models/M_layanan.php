<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_layanan extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_layanan");
        return $query->result();
    }		
	
	public function layanan(){
        $query = $this->db->get('table_layanan');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		$this->id_layanan = $data['id_layanan'];
		$this->jenis_layanan = $data['jenis_layanan'];
        $this->judul = $data['judul'];
		$this->deskripsi = $data['deskripsi'];
		
        //insert data
        $this->db->insert('table_layanan', $this);
    }
    
    public function update($data) {
        //get data
        $this->id_layanan = $data['id_layanan'];
        $this->jenis_layanan = $data['jenis_layanan'];
        $this->judul = $data['judul'];
		$this->deskripsi = $data['deskripsi'];
		
        //update data
        $this->db->update('table_layanan', $this, array('id_layanan'=>$data['id_layanan']));
    }
    
    public function delete($id) {
        $this->db->delete('table_layanan', array('id_layanan' => $id));
    }


    public function get($id){
        $this->db->where('id_layanan', $id);
        $query = $this->db->get('table_layanan');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_layanan");
	}
	
	public function record_count_search($key) {
		$this->db->like("jenis_layanan", $key);
		$this->db->like("judul", $key);
		$this->db->or_like("deskripsi", $key);		
		return $this->db->count_all_results("table_layanan");
	}
	
	public function fetch_layanan($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_layanan');
		$this->db->order_by('id_layanan DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_layanan($limit, $start, $key) {
	
		$this->db->like("judul", $key);
		$this->db->like("jenis_layanan", $key);
		$this->db->or_like("deskripsi", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_layanan");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>