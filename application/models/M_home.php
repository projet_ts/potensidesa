<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perizinan

class M_home extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function kabupaten() {
        return $this->db->count_all("industri_table_kabupaten WHERE id_kabupaten != '1'");
    }
	
	public function pasar() {
        return $this->db->count_all("industri_table_pasar");
    }
	
	public function pasar2($id_kabupaten) {
        return $this->db->count_all("industri_table_pasar WHERE id_kabupaten = '$id_kabupaten'");
    }

	public function pelabuhan() {
        return $this->db->count_all("industri_table_pelabuhan");
    }
	
	public function pelabuhan2($id_kabupaten) {
        return $this->db->count_all("industri_table_pelabuhan WHERE id_kabupaten = '$id_kabupaten'");
    }
	
	public function komoditi() {
        return $this->db->count_all("industri_table_komoditi");
    }
	
	public function user() {
        return $this->db->count_all("table_user WHERE id_kabupaten!='1'");
    }
	
	public function user2($id_kabupaten) {
        return $this->db->count_all("table_user WHERE id_kabupaten='".$id_kabupaten."' AND group_id!='group1000'");
    }
	
	public function nomor(){
		$query= $this->db->query("select count(*) as total from izin_table_permohonan");
		return $query->result();
		
	}
	
}  
?>