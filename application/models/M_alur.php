<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perizinan

class M_alur extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	
	public function create($data) {
        //get data
        $this->nup = $data['nup'];
        $this->step_frontoffice = $data['step_frontoffice'];
        $this->step_kabid = $data['step_kabid'];
		$this->step_disposisi = $data['step_disposisi'];
		$this->step_survey = $data['step_survey'];
		$this->step_retribusi = $data['step_retribusi'];
        $this->step_backoffice = $data['step_backoffice'];
		$this->step_surat = $data['step_surat'];
		$this->step_pembayaran = $data['step_pembayaran'];
		$this->step_pengambilan = $data['step_pengambilan'];
        
        //insert data
        $this->db->insert('izin_table_alur', $this);
    }
    
    public function update_kabid($data) {
        //get data
        
        $this->step_kabid = $data['step_kabid'];
		
        
        //update data
        $this->db->update('izin_table_alur', $this, array('nup'=>$data['nup']));
    }
	
	public function update_disposisi($data) {
        //get data
        
		$this->step_disposisi = $data['step_disposisi'];
		
        
        //update data
        $this->db->update('izin_table_alur', $this, array('nup'=>$data['nup']));
    }
	
	public function update_survey($data) {
        //get data
        
		$this->step_survey = $data['step_survey'];
		
        
        //update data
        $this->db->update('izin_table_alur', $this, array('nup'=>$data['nup']));
    }
	
	public function update_retribusi($data) {
        //get data
        
		$this->step_retribusi = $data['step_retribusi'];
		
        
        //update data
        $this->db->update('izin_table_alur', $this, array('nup'=>$data['nup']));
    }
	
	public function update_surat($data) {
        //get data
        
		$this->step_surat = $data['step_surat'];
		
        
        //update data
        $this->db->update('izin_table_alur', $this, array('nup'=>$data['nup']));
    }
	
	public function update_pembayaran($data) {
        //get data
        if($data['izin_status']=="Registrasi Ulang"){
			$this->step_pembayaran = $data['step_pembayaran'];
			$this->step_surat = $data['step_surat'];
			
		}else{
			$this->step_pembayaran = $data['step_pembayaran'];
		}
		
        
        //update data
        $this->db->update('izin_table_alur', $this, array('nup'=>$data['nup']));
    }
	
	public function update_pengambilan($data) {
        //get data
        
		$this->step_pengambilan = $data['step_pengambilan'];
		
        
        //update data
        $this->db->update('izin_table_alur', $this, array('nup'=>$data['nup']));
    }
	
	public function check_position($nup) {
		$query  = $this->db->query("SELECT * FROM izin_table_alur s WHERE s.nup='$nup'");
        return $query->result();
	}
    
    public function get_alur(){
        $query = $this->db->get('izin_table_alur');
        return $query->result();
    }
	
}
?>