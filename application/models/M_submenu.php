<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//model for table_submenu
class M_submenu extends CI_Model{

    function __construct() {
        parent::__construct();
    }
    //select submenu by menu_id
	public function data($id) {
		$query  = $this->db->query("SELECT s.submenu_id, s.menu_id, s.submenu_name, s.attribute, s.link, s.active,
			m.menu_name FROM table_menu m, table_submenu s WHERE s.menu_id=m.menu_id AND s.menu_id='$id'");
        return $query->result();
	}
    //insert data
    public function create($data) {
        //init data
        $this->submenu_id = $data['submenu_id'];
        $this->menu_id = $data['menu_id'];
        $this->submenu_name = $data['submenu_name'];
        $this->attribute = $data['attribute'];
        $this->link = $data['link'];
        $this->active = $data['active'];

        //insert data
        $this->db->insert('table_submenu', $this);
    }
    //update data
    public function update($data) {
        //init data
        $this->menu_id = $data['menu_id'];
        $this->submenu_name = $data['submenu_name'];
        $this->attribute = $data['attribute'];
        $this->link = $data['link'];
        $this->active = $data['active'];

        //update data
        $this->db->update('table_submenu', $this, array('submenu_id'=>$data['submenu_id']));
    }
    //delete data
    public function delete($id) {
        $this->db->delete('table_submenu', array('submenu_id' => $id));
    }

    //get data by id
    public function get($id){
        $this->db->where('submenu_id', $id);
        $query = $this->db->get('table_submenu', 1);
        return $query->result();
    }
    //count all data
	public function record_count($id) {
		$this->db->where("menu_id", $id);
		return $this->db->count_all_results("table_submenu");
	}
  //count data by key
	public function record_count_search($key, $id) {
		$this->db->where("menu_id", $id);
		$this->db->like("submenu_name", $key);
		$this->db->or_like("attribute", $key);
		$this->db->or_like("link", $key);
		$this->db->or_like("active", $key);
		return $this->db->count_all_results("table_submenu");
	}
  //select data with limit for pagination
	public function fetch_submenu($limit, $start, $id) {
		$this->db->where("menu_id", $id);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_submenu");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
  //select data with limit and key for pagination
	public function search_submenu($key, $id) {
		$this->db->where("s.menu_id=m.menu_id
							AND s.menu_id = '".$id."'
							AND (s.submenu_name LIKE '%".$key."%' ESCAPE '!'
							OR s.attribute LIKE '%".$key."%' ESCAPE '!'
							OR s.link LIKE '%".$key."%' ESCAPE '!'
							OR s.active LIKE '%".$key."%' ESCAPE '!'
						 )");
		$query = $this->db->get("table_menu m, table_submenu s ");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
	}
}
?>
