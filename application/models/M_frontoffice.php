<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perizinan

class M_frontoffice extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data() {
        $query  = $this->db->query("SELECT * 
                                        FROM izin_table_permohonan");
        return $query->result();
    }
	
    public function create($data) {
        //get data
        $this->kode_izin = $data['kode_izin'];
        $this->nup = $data['nup'];
        $this->tanggal_masuk = $data['tanggal_masuk'];
        $this->jam_masuk = $data['jam_masuk'];
		$this->izin_status = $data['izin_status'];
		$this->id_perusahaan = $data['id_perusahaan'];
		$this->id_pemohon = $data['id_pemohon'];
		if($this->izin_status=="Baru") {
			$this->izin_tgl_terbit = $data['izin_tgl_terbit'];
			$this->izin_berlaku_sd = $data['izin_berlaku_sd'];
			$this->tgl_jatuh_tempo = $data['tgl_jatuh_tempo'];
		} else {
			$this->izin_tgl_terbit = $data['izin_tgl_terbit'];
			$this->tgl_dftr_ulg_trahir = $data['tgl_dftr_ulg_trahir'];
			$this->izin_berlaku_sd = $data['izin_berlaku_sd'];
			$this->tgl_jatuh_tempo = $data['tgl_jatuh_tempo'];
			$this->tgl_terbit_lama = $data['tgl_terbit_lama'];
			
			
		}
		
        //insert data
        $this->db->insert('izin_table_permohonan', $this);
    }
	
    
    public function update($data) {
        //get data
        $this->kode_instansi = $data['kode_instansi'];
        $this->nama_izin = $data['nama_izin'];
        $this->lama_urus = $data['lama_urus'];
		$this->masa_tahun = $data['masa_tahun'];
		$this->masa_bulan = $data['masa_bulan'];
		$this->keterangan = $data['keterangan'];
        
        //update data
        $this->db->update('izin_table_permohonan', $this, array('kode_izin'=>$data['kode_izin']));
    }
	
	public function update_ceklist($data) {
        //get data
        $this->cek_frontoffice = $data['status'];
        $this->keterangan_frontoffice = $data['keterangan'];
        
        //update data
        $this->db->update('izin_table_syarat_cek', $this, array('nup'=>$data['nup'], 
				'kode_izin'=>$data['kode_izin'], 'kode_syarat'=>$data['kode_syarat']));
    }
       public function update_terima($data) {
        //get data
        $this->cek_frontoffice = $data['status'];
        $this->nup = $data[nup];
        
        //update data
        $this->db->update('izin_table_syarat_cek', $this, array('nup'=>$data['nup']));
    }
	
	public function update_npwp($datax) {
        //get data
        $this->id_pemohon = $datax['id_pemohon'];
        $this->id_perusahaan = $datax['id_perusahaan'];
		$this->izin_status = $datax['izin_status'];
		$this->tgl_jatuh_tempo = $datax['tgl_jatuh_tempo'];
		$this->izin_tgl_terbit = $datax['izin_tgl_terbit'];
		$this->tgl_dftr_ulg_trahir = $datax['tgl_dftr_ulg_trahir'];
		//$this->tgl_jatuh_tempo = date('Y-m-d', strtotime('+1 month', strtotime( variabel_tgl_awal )));
		
        
        //update data
        $this->db->update('izin_table_permohonan', $this, array('nup'=>$datax['nup']));
    }
	
	public function update_npwpx($datax) {
        //get data
		$this->nup = $datax['nup'];
		$this->izin_status = $datax['izin_status'];
        
        //update data
        $this->db->update('izin_table_permohonan', $this, array('nup'=>$datax['nup']));
    }
	
	public function update_izin_terbit($dataj) {
        //get data
		$this->nup = $dataj['nup'];
		$this->izin_tgl_terbit = $dataj['izin_tgl_terbit'];
		$this->izin_berlaku_sd = $dataj['izin_berlaku_sd'];
        
        //update data
        $this->db->update('izin_table_permohonan', $this, array('nup'=>$dataj['nup']));
    }
    
    public function delete($id) {
        $this->db->delete('izin_table_alur', array('nup' => $id['nup']));
        $this->db->delete('izin_table_ceklist', array('nup' => $id['nup']));
        $this->db->delete('izin_table_disposisi', array('nup' => $id['nup']));
		$this->db->delete('izin_table_keterangan', array('nup' => $id['nup']));
		$this->db->delete('izin_table_nota', array('nup' => $id['nup']));
		$this->db->delete('izin_table_pembayaran', array('nup' => $id['nup']));
		$this->db->delete('izin_table_pemroses', array('nup' => $id['nup']));
		$this->db->delete('izin_table_pengambilan', array('nup' => $id['nup']));
		$this->db->delete('izin_table_permohonan', array('nup' => $id['nup']));
		$this->db->delete('izin_table_retribusi', array('nup' => $id['nup']));
		$this->db->delete('izin_table_review', array('nup' => $id['nup']));
		$this->db->delete('izin_table_survey', array('nup' => $id['nup']));
		$this->db->delete('izin_table_syarat_cek', array('nup' => $id['nup']));
		
		
		
		$this->db->delete('izin_table_pemohon', array('id_pemohon' => $id['pemohon']));
		$this->db->delete('izin_table_perusahaan', array('id_perusahaan' => $id['perusahaan']));
		
		
		
		
    }


    public function get_syarat($nup){
		$this->db->select('*');
		$this->db->from('izin_table_syarat_cek sc');
		$this->db->join('izin_table_syarat s', 'sc.kode_syarat=s.kode_syarat');
        $this->db->where('sc.nup', $nup);
        $query = $this->db->get();
        return $query->result();
    }
	
	public function get_kodeizin($nup) {
		$this->db->select('*');
		$this->db->from('izin_table_permohonan j');
		$this->db->join('izin_table_jenisizin js', 'j.kode_izin=js.kode_izin');
		$this->db->where('j.nup', $nup);
        $query = $this->db->get();
        return $query->result();
	}
	
	public function get($nup){
		$this->db->select('js.nama_izin, i.nama_instansi,j.izin_status, j.nup, j.tanggal_masuk, j.jam_masuk, j.tgl_jatuh_tempo, 
									p.npwp_perusahaan, p.nama_perusahaan, p.id_perusahaan, p.kec_perusahaan, p.kel_perusahaan, 
									s.no_identitas, s.nama_pemohon, s.id_pemohon, s.kecamatan, s.kelurahan, 
									al.step_frontoffice, al.step_kabid, al.step_backoffice, al.step_pembayaran,
									al.step_surat, al.step_pengambilan, p.alamat_perusahaan, s.alamat_pemohon,
									js.kode_izin, js.nama_izin, js.kategori, p.badan_usaha,
									fr.fullname as frontoffice, bc.fullname as backoffice, si.desain, j.desain_surat');
		$this->db->from('izin_table_permohonan j');
		$this->db->join('izin_table_perusahaan p', 'p.id_perusahaan=j.id_perusahaan');
		$this->db->join('izin_table_pemohon s', 's.id_pemohon=j.id_pemohon');
		$this->db->join('izin_table_jenisizin js', 'j.kode_izin=js.kode_izin');
		$this->db->join('izin_table_instansi i', 'i.kode_instansi=js.kode_instansi');
		$this->db->join('izin_table_pemroses pe', 'pe.nup=j.nup');
		$this->db->join('izin_table_alur al', 'al.nup=j.nup');
		$this->db->join('table_user fr', 'fr.userid=pe.frontoffice');
		$this->db->join('table_user bc', 'bc.userid=pe.backoffice');
		$this->db->join('izin_table_suratizin si', 'si.kode_izin=j.kode_izin');
        $this->db->where('j.nup', $nup);
        $query = $this->db->get();
        return $query->result();
    }
	
	public function data_timteknis(){
		$this->db->select('*');
		$this->db->from('table_user s');
		$this->db->join('izin_table_instansi i', 's.kode_instansi=i.kode_instansi');
		$this->db->join('table_group g', 'g.group_id=s.group_id');
		$this->db->where('g.group_id', 'group204');
        $query = $this->db->get();
        return $query->result();
    }
	
	public function record_count() {
		return $this->db->count_all("izin_table_permohonan");
	}
	
	public function record_count_search($key) {
		$this->db->like("j.nup", $key);
		$this->db->or_like("js.nama_izin", $key);
		$this->db->or_like("i.nama_instansi", $key);
		$this->db->or_like("s.no_identitas", $key);
		$this->db->or_like("s.nama_pemohon", $key);
		$this->db->or_like("p.npwp_perusahaan", $key);
		$this->db->or_like("p.nama_perusahaan", $key);
		$this->db->or_like("js.kategori", $key);
		$this->db->or_like("j.tanggal_masuk", $key);
		$this->db->or_like("j.jam_masuk", $key);
		$this->db->or_like("bc.fullname", $key);
		$this->db->or_like("fr.fullname", $key);
		$this->db->from('izin_table_permohonan j');
		$this->db->join('izin_table_perusahaan p', 'p.id_perusahaan=j.id_perusahaan');
		$this->db->join('izin_table_pemohon s', 's.id_pemohon=j.id_pemohon');
		$this->db->join('izin_table_jenisizin js', 'j.kode_izin=js.kode_izin');
		$this->db->join('izin_table_instansi i', 'i.kode_instansi=js.kode_instansi');
		$this->db->join('izin_table_pemroses pe', 'pe.nup=j.nup');
		$this->db->join('izin_table_alur al', 'al.nup=j.nup');
		$this->db->join('table_user fr', 'fr.userid=pe.frontoffice');
		$this->db->join('table_user bc', 'bc.userid=pe.backoffice');
		return $this->db->count_all_results();
	}
    
	public function fetch_frontoffice($limit, $start) {
		//first_query
		$this->db->select('js.nama_izin, i.nama_instansi,j.izin_status, j.nup, j.tanggal_masuk, j.jam_masuk, j.tgl_jatuh_tempo, 
									p.npwp_perusahaan, p.nama_perusahaan, p.id_perusahaan, p.kec_perusahaan, p.kel_perusahaan, 
									s.no_identitas, s.nama_pemohon, s.id_pemohon, s.kecamatan, s.kelurahan, 
									al.step_frontoffice, al.step_kabid, al.step_backoffice, al.step_pembayaran,
									al.step_surat, al.step_pengambilan, 
									js.kode_izin, js.nama_izin, js.kategori,
									fr.fullname as frontoffice, bc.fullname as backoffice');
		$this->db->from('izin_table_permohonan j');
		$this->db->join('izin_table_perusahaan p', 'p.id_perusahaan=j.id_perusahaan');
		$this->db->join('izin_table_pemohon s', 's.id_pemohon=j.id_pemohon');
		$this->db->join('izin_table_jenisizin js', 'j.kode_izin=js.kode_izin');
		$this->db->join('izin_table_instansi i', 'i.kode_instansi=js.kode_instansi');
		$this->db->join('izin_table_pemroses pe', 'pe.nup=j.nup');
		$this->db->join('izin_table_alur al', 'al.nup=j.nup');
		$this->db->join('table_user fr', 'fr.userid=pe.frontoffice');
		$this->db->join('table_user bc', 'bc.userid=pe.backoffice');
		$this->db->limit($limit, $start);
		$this->db->order_by("j.nup", "DESC");
		$this->db->where("js.kategori", "Izin Usaha");
		$this->db->or_where("js.kategori", "Penanaman Modal");
		$query1 = $this->db->get();
		$query1 = $query1->result();
		
		//second_query
		$this->db->select('js.nama_izin, i.nama_instansi, j.nup, j.tanggal_masuk, j.jam_masuk, j.tgl_jatuh_tempo, 
									s.no_identitas, s.nama_pemohon, s.id_pemohon, s.kecamatan, s.kelurahan, 
									al.step_frontoffice, al.step_kabid, al.step_backoffice, al.step_pembayaran,
									al.step_surat, al.step_pengambilan, 
									js.kode_izin, js.nama_izin, js.kategori,
									fr.fullname as frontoffice, bc.fullname as backoffice');
		$this->db->from('izin_table_permohonan j');
		$this->db->join('izin_table_pemohon s', 's.id_pemohon=j.id_pemohon');
		$this->db->join('izin_table_jenisizin js', 'j.kode_izin=js.kode_izin');
		$this->db->join('izin_table_instansi i', 'i.kode_instansi=js.kode_instansi');
		$this->db->join('izin_table_pemroses pe', 'pe.nup=j.nup');
		$this->db->join('izin_table_alur al', 'al.nup=j.nup');
		$this->db->join('table_user fr', 'fr.userid=pe.frontoffice');
		$this->db->join('table_user bc', 'bc.userid=pe.backoffice');
		$this->db->limit($limit, $start);
		$this->db->order_by("j.nup", "DESC");
		$this->db->where("js.kategori", "Izin Non Usaha");
		$query2 = $this->db->get();
		$query2 = $query2->result();
		
		return array_merge($query1, $query2);;
	}
	
	public function search_frontoffice($limit, $start, $key) {
		$this->db->select('js.nama_izin, i.nama_instansi,j.izin_status, j.nup, j.tanggal_masuk, j.jam_masuk, j.tgl_jatuh_tempo, 
									p.npwp_perusahaan, p.nama_perusahaan, p.id_perusahaan, p.kec_perusahaan, p.kel_perusahaan, 
									s.no_identitas, s.nama_pemohon, s.id_pemohon, s.kecamatan, s.kelurahan, 
									al.step_frontoffice, al.step_kabid, al.step_backoffice, al.step_pembayaran,
									al.step_surat, al.step_pengambilan, 
									js.kode_izin, js.nama_izin, js.kategori,
									fr.fullname as frontoffice, bc.fullname as backoffice');
		$this->db->like("j.nup", $key);
		$this->db->or_like("js.nama_izin", $key);
		$this->db->or_like("i.nama_instansi", $key);
		$this->db->or_like("s.no_identitas", $key);
		$this->db->or_like("s.nama_pemohon", $key);
		$this->db->or_like("p.npwp_perusahaan", $key);
		$this->db->or_like("p.nama_perusahaan", $key);
		$this->db->or_like("js.kategori", $key);
		$this->db->or_like("j.tanggal_masuk", $key);
		$this->db->or_like("j.jam_masuk", $key);
		$this->db->or_like("bc.fullname", $key);
		$this->db->or_like("fr.fullname", $key);
		$this->db->from('izin_table_permohonan j');
		$this->db->join('izin_table_perusahaan p', 'p.id_perusahaan=j.id_perusahaan');
		$this->db->join('izin_table_pemohon s', 's.id_pemohon=j.id_pemohon');
		$this->db->join('izin_table_jenisizin js', 'j.kode_izin=js.kode_izin');
		$this->db->join('izin_table_instansi i', 'i.kode_instansi=js.kode_instansi');
		$this->db->join('izin_table_pemroses pe', 'pe.nup=j.nup');
		$this->db->join('izin_table_alur al', 'al.nup=j.nup');
		$this->db->join('table_user fr', 'fr.userid=pe.frontoffice');
		$this->db->join('table_user bc', 'bc.userid=pe.backoffice');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
	
	
	public function status_permohonan($data){
		
		$this->nup = $data['nup'];
		$this->status_permohonan = $data['kelayakan_lingkungan'];
		
		//update
		$this->db->update('izin_table_permohonan', $this, array('nup'=>$data['nup']));
	}
		
		 public function get_survey($id){
                $query = $this->db->query("select * from izin_table_survey where nup='$id'",  array('nup' => $id));
                return $query->result();
            
        }
		
		public function get_retribusi($id){
                $query = $this->db->query("select * from izin_table_retribusi where nup='$id'",  array('nup' => $id));
                return $query->result();
            
        }
	
        public function get_template_nota(){
                $query = $this->db->query("select * from izin_table_template where keterangan='Nota'");
                return $query->result();
            
        }
        
        public function get_template_ceklis(){
                $query = $this->db->query("select * from izin_table_template where keterangan='Ceklis'");
                return $query->result();
            
        }
        
        public function get_template_validasi(){
                $query = $this->db->query("select * from izin_table_template where keterangan='Validasi'");
                return $query->result();
            
        }
        
        public function get_template_terima(){
                $query = $this->db->query("select * from izin_table_template where keterangan='Terima'");
                return $query->result();
            
        }
		
		public function get_template_rekomendasi(){
                $query = $this->db->query("select * from izin_table_template where keterangan='Rekomendasi'");
                return $query->result();
            
        }
		
		public function get_template_disposisi(){
                $query = $this->db->query("select * from izin_table_template where keterangan='Disposisi'");
                return $query->result();
            
        }
        
        public function get_template_review(){
                $query = $this->db->query("select * from izin_table_template where keterangan='Review'");
                return $query->result();
            
        }
		
		public function get_template_sts(){
                $query = $this->db->query("select * from izin_table_template where keterangan='STS'");
                return $query->result();
            
        }
        
        public function get_template_skr(){
                $query = $this->db->query("select * from izin_table_template where keterangan='Retribusi'");
                return $query->result();
            
        }
		
		public function get_template_skrd(){
                $query = $this->db->query("select * from izin_table_template where keterangan='SKRD'");
                return $query->result();
            
        }
		
		public function get_template_slip(){
                $query = $this->db->query("select * from izin_table_template where keterangan='slip'");
                return $query->result();
            
        }
		
		public function get_template_pengambilan(){
                $query = $this->db->query("select * from izin_table_template where keterangan='Pengambilan'");
                return $query->result();
            
        }
		
		public function get_template_notaperhitungan(){
                $query = $this->db->query("select * from izin_table_template where keterangan='NOTA PERHITUNGAN RETRIBUSI DAN PAJAK'");
                return $query->result();
            
        }
		
		public function get_template_teknis(){
                $query = $this->db->query("select * from izin_table_template where keterangan='teknis'");
                return $query->result();
            
        }
		
		public function get_ttd(){
                $query = $this->db->query("select * from izin_table_penandatangan where status_penandatangan='Aktif'");
                return $query->result();
            
        }
		
		 public function get_report($id){
                $query = $this->db->query("select * from izin_table_permohonan a,
                                                         izin_table_pemohon b,
                                                         izin_table_pemroses c,
                                                         izin_table_perusahaan d,
                                                         izin_table_suratizin e,
                                                         izin_table_jenisizin f,
                                                         izin_table_instansi g,
                                                         izin_table_syarat h,
                                                         izin_table_syarat_cek i,
														 table_user j
														 
                                                    where a.id_pemohon=b.id_pemohon and
                                                          a.nup=c.nup and
                                                          a.nup=i.nup and
                                                          a.id_perusahaan=d.id_perusahaan and
                                                          a.kode_izin=e.kode_izin and
                                                          a.kode_izin=f.kode_izin and
                                                          a.kode_izin=h.kode_izin and
                                                          a.kode_izin=i.kode_izin and
                                                          e.kode_izin=i.kode_izin and
                                                          e.kode_izin=f.kode_izin and
                                                          f.kode_izin=i.kode_izin and
                                                          f.kode_izin=h.kode_izin and
                                                          h.kode_izin=i.kode_izin and
														  c.backoffice=j.userid and
                                                          h.kode_syarat=i.kode_syarat and
                                                          f.kode_instansi=g.kode_instansi and
                                                        a.nup='$id'",  array('nup' => $id));
                                                        

            return $query->result();
            
        }
		
		
        
        
    
    
}
?>