<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_jenisjasakip extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_jenisjasakip");
        return $query->result();
    }		
	
	public function jenisjasakip(){
        $query = $this->db->get('table_jenisjasakip');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->nama_jenisjasakip = $data['nama_jenisjasakip'];
		$this->deskripsi = $data['deskripsi'];				
        
        //insert data
        $this->db->insert('table_jenisjasakip', $this);
    }

    //upload image
	public function createnoimage($data) {
        //get data
		
		
		$this->nama_jenisjasakip = $data['nama_jenisjasakip'];
		$this->deskripsi = $data['deskripsi'];
		
		
        //insert data
        $this->db->insert('table_jenisjasakip', $this);
    }
    
    public function update($data) {
        //get data
        
        
		$this->nama_jenisjasakip = $data['nama_jenisjasakip'];
		$this->deskripsi = $data['deskripsi'];
			
		
        //update data
        $this->db->update('table_jenisjasakip', $this, array('id_jenisjasakip'=>$data['id_jenisjasakip']));
    }
    
    public function delete($id) {
        $this->db->delete('table_jenisjasakip', array('id_jenisjasakip' => $id));
    }


    public function get($id){
        $this->db->where('id_jenisjasakip', $id);
        $query = $this->db->get('table_jenisjasakip');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_jenisjasakip");
	}
	
	public function record_count_search($key) {
		$this->db->like("gambar", $key);
		$this->db->or_like("nama_jenisjasakip", $key);
		$this->db->or_like("deskripsi", $key);		
		return $this->db->count_all_results("table_jenisjasakip");
	}
	
	public function fetch_jenisjasakip($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_jenisjasakip');
		$this->db->order_by('id_jenisjasakip DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_jenisjasakip($limit, $start, $key) {
	
		
		$this->db->like("nama_jenisjasakip", $key);
		$this->db->or_like("deskripsi", $key);			
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_jenisjasakip");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}

	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_jenisjasakip',$id);
		$query = $getData = $this->db->get('table_jenisjasakip');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->id_jenisjasakip = $data['id_jenisjasakip'];			
		$this->nama_jenisjasakip = $data['nama_jenisjasakip'];
		$this->deskripsi = $data['deskripsi'];
        
        //update data
        $this->db->update('table_jenisjasakip', $this, array('id_jenisjasakip'=>$data['id_jenisjasakip']));
    }
	
}
?>