<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_fasilitas extends CI_Model{

    function __construct() {
        parent::__construct();
    }

	public function data() {
        $query  = $this->db->query("SELECT * FROM table_fasilitas");
        return $query->result();
    }

	public function fasilitas(){
        $query = $this->db->get('table_fasilitas');
        return $query->result();
    }

    public function create($data) {
        //get data

    $this->nama_kecamatan = $data['nama_kecamatan'];
		$this->keterangan = $data['keterangan'];			

        //insert data
        $this->db->insert('table_fasilitas', $this);
    }

    public function update($data) {
        //get data

        $this->gambar = $data['gambar'];
		$this->judul = $data['judul'];
		$this->deskripsi = $data['deskripsi'];


        //update data
        $this->db->update('table_fasilitas', $this, array('id_fasilitas'=>$data['id_fasilitas']));
    }

    public function delete($id) {
        $this->db->delete('table_fasilitas', array('id_fasilitas' => $id));
    }


    public function get($id){
        $this->db->where('id_fasilitas', $id);
        $query = $this->db->get('table_fasilitas');
        return $query->result();
    }

	public function record_count() {
			return $this->db->count_all("table_fasilitas");
	}

	public function record_count_search($key) {
		$this->db->like("gambar", $key);
		$this->db->or_like("judul", $key);
		$this->db->or_like("deskripsi", $key);
		return $this->db->count_all_results("table_fasilitas");
	}

	public function fetch_fasilitas($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_fasilitas');
		$this->db->order_by('id_fasilitas DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function search_fasilitas($limit, $start, $key) {

		$this->db->like("gambar", $key);
		$this->db->or_like("judul", $key);
		$this->db->or_like("deskripsi", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_fasilitas");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;

	}

	//upload image
	public function link_gambar($id)
	{

		$this->db->where('id_fasilitas',$id);
		$query = $getData = $this->db->get('table_fasilitas');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;

	}

	//upload image
	public function updatenoimage($data) {
         //get data
		$this->id_fasilitas = $data['id_fasilitas'];
		$this->judul = $data['judul'];
		$this->deskripsi = $data['deskripsi'];

        //update data
        $this->db->update('table_fasilitas', $this, array('id_fasilitas'=>$data['id_fasilitas']));
    }

}
?>
