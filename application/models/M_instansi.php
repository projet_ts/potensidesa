<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perizinan

class M_instansi extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data() {
        $query  = $this->db->query("SELECT kode_instansi, nama_instansi, kementerian, alamat,
										telepon, fax, email, website 
                                        FROM izin_table_instansi WHERE kode_instansi!='000'");
        return $query->result();
    }
	
    public function create($data) {
        //get data
        $this->kode_instansi = $data['kode_instansi'];
        $this->nama_instansi = $data['nama_instansi'];
        $this->kementerian = $data['kementerian'];
        $this->alamat = $data['alamat'];
		$this->telepon = $data['telepon'];
		$this->fax = $data['fax'];
		$this->email = $data['email'];
		$this->website = $data['website'];
        
        //insert data
        $this->db->insert('izin_table_instansi', $this);
    }
    
    public function update($data) {
        //get data
        $this->nama_instansi = $data['nama_instansi'];
        $this->kementerian = $data['kementerian'];
        $this->alamat = $data['alamat'];
		$this->telepon = $data['telepon'];
		$this->fax = $data['fax'];
		$this->email = $data['email'];
		$this->website = $data['website'];
        
        //update data
        $this->db->update('izin_table_instansi', $this, array('kode_instansi'=>$data['kode_instansi']));
    }
    
    public function delete($id) {
        $this->db->delete('izin_table_instansi', array('kode_instansi' => $id));
    }


    public function get($id){
        $this->db->where('kode_instansi', $id);
        $query = $this->db->get('izin_table_instansi', 1);
        return $query->result();
    }
	
	public function record_count() {
		return $this->db->count_all("izin_table_instansi");
	}
	
	public function record_count_search($key) {
		$this->db->where_not_in("kode_instansi", "000");
		$this->db->like("kode_instansi", $key);
		$this->db->or_like("nama_instansi", $key);
		$this->db->or_like("alamat", $key);
		$this->db->or_like("email", $key);
		$this->db->or_like("kementerian", $key);
		$this->db->or_like("fax", $key);
		$this->db->or_like("telepon", $key);
		$this->db->or_like("website", $key);
		return $this->db->count_all_results("izin_table_instansi");
	}
    
	public function fetch_instansi($limit, $start) {
		$this->db->where_not_in("kode_instansi", "000");
		$this->db->limit($limit, $start);
		$query = $this->db->get("izin_table_instansi");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_instansi($limit, $start, $key) {
		$this->db->where_not_in("kode_instansi", "000");
		$this->db->like("kode_instansi", $key);
		$this->db->or_like("nama_instansi", $key);
		$this->db->or_like("alamat", $key);
		$this->db->or_like("email", $key);
		$this->db->or_like("kementerian", $key);
		$this->db->or_like("fax", $key);
		$this->db->or_like("telepon", $key);
		$this->db->or_like("website", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("izin_table_instansi");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
    
    
}
?>