<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_jenisagenda extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_jenisagenda");
        return $query->result();
    }		
	
	public function jenisagenda(){
        $query = $this->db->get('table_jenisagenda');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->nama_jenisagenda = $data['nama_jenisagenda'];
		$this->deskripsi = $data['deskripsi'];				
        
        //insert data
        $this->db->insert('table_jenisagenda', $this);
    }

    //upload image
	public function createnoimage($data) {
        //get data
		
		
		$this->nama_jenisagenda = $data['nama_jenisagenda'];
		$this->deskripsi = $data['deskripsi'];
		
		
        //insert data
        $this->db->insert('table_jenisagenda', $this);
    }
    
    public function update($data) {
        //get data
        
        
		$this->nama_jenisagenda = $data['nama_jenisagenda'];
		$this->deskripsi = $data['deskripsi'];
			
		
        //update data
        $this->db->update('table_jenisagenda', $this, array('id_jenisagenda'=>$data['id_jenisagenda']));
    }
    
    public function delete($id) {
        $this->db->delete('table_jenisagenda', array('id_jenisagenda' => $id));
    }


    public function get($id){
        $this->db->where('id_jenisagenda', $id);
        $query = $this->db->get('table_jenisagenda');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_jenisagenda");
	}
	
	public function record_count_search($key) {
		$this->db->like("gambar", $key);
		$this->db->or_like("nama_jenisagenda", $key);
		$this->db->or_like("deskripsi", $key);		
		return $this->db->count_all_results("table_jenisagenda");
	}
	
	public function fetch_jenisagenda($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_jenisagenda');
		$this->db->order_by('id_jenisagenda DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_jenisagenda($limit, $start, $key) {
	
		
		$this->db->like("nama_jenisagenda", $key);
		$this->db->or_like("deskripsi", $key);			
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_jenisagenda");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}

	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_jenisagenda',$id);
		$query = $getData = $this->db->get('table_jenisagenda');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->id_jenisagenda = $data['id_jenisagenda'];			
		$this->nama_jenisagenda = $data['nama_jenisagenda'];
		$this->deskripsi = $data['deskripsi'];
        
        //update data
        $this->db->update('table_jenisagenda', $this, array('id_jenisagenda'=>$data['id_jenisagenda']));
    }
	
}
?>