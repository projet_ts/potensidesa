<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perizinan

class M_disposisi extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data($nup) {
        $query  = $this->db->query("SELECT * 
                                        FROM izin_table_disposisi n, table_user u WHERE n.userid=u.userid AND n.nup = '$nup'");
        return $query->result();
    }
	
    public function create_disposisi($data) {
        //get data
        $this->disposisi_id = $data['disposisi_id'];
		$this->nup = $data['nup'];
        $this->perihal = $data['perihal'];
		$this->keterangan = $data['keterangan'];
        $this->tanggal_disposisi = $data['tanggal_disposisi'];
        $this->jam_disposisi = $data['jam_disposisi'];
		$this->userid = $data['userid'];
        
        //insert data
        $this->db->insert('izin_table_disposisi', $this);
    }
	
	  
    public function update($data) {
        //get data
        $this->kode_instansi = $data['kode_instansi'];
        $this->nama_izin = $data['nama_izin'];
        $this->lama_urus = $data['lama_urus'];
		$this->masa_tahun = $data['masa_tahun'];
		$this->masa_bulan = $data['masa_bulan'];
		$this->keterangan = $data['keterangan'];
        
        //update data
        $this->db->update('izin_table_disposisi', $this, array('kode_izin'=>$data['kode_izin']));
    }
	
	
    public function delete($id) {
        $this->db->delete('izin_table_disposisi', array('nup' => $id));
    }


	public function get($nup){
		$this->db->select('*');
		$this->db->from('izin_table_disposisi r');
		$this->db->join('table_user u', 'u.userid=r.userid');
		$this->db->where('r.nup', $nup);
        $query = $this->db->get();
        return $query->result();
    }
	
    
}
?>