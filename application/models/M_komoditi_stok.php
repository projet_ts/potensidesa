<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindutrian

class M_komoditi_stok extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data() {
        $query = $this->db->query("SELECT * FROM industri_table_komoditi_stok");
        return $query->result();
    }
	
	public function getkomoditi() {
        $query = $this->db->query("SELECT * FROM industri_table_komoditi_stok");
        return $query->result();
    }
	
    public function create($data) {
        //get data
		//$this->id_harga_barang = $data['id_harga_barang'];
		$this->id_komoditi = $data['id_komoditi'];
		$this->id_pasar = $data['id_pasar'];
        $this->tanggal = date('Y-m-d');
		$this->harga = $data['harga'];
		
        //insert data
        $this->db->insert('industri_table_harga_barang', $this);
    }
	
    
    public function update($data) {
        //get data
        $this->id_harga_barang = $data['id_harga_barang'];
		$this->id_komoditi = $data['id_komoditi'];
		$this->id_pasar = $data['id_pasar'];
        $this->tanggal = date('Y-m-d');
		$this->harga = $data['harga'];
        
        //update data
        $this->db->update('industri_table_harga_barang', $this, array('id_harga_barang'=>$data['id_harga_barang']));
    }
    
    public function delete($id) {
        $this->db->delete('industri_table_harga_barang', array('id_harga_barang' => $id));
    }

    public function get($id){
        $this->db->where('id_harga_barang', $id);
        $query = $this->db->get('industri_table_harga_barang');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
											WHERE a.id_komoditi=b.id_komoditi 
											AND a.id_pasar=c.id_pasar
											AND b.jenis_komoditi='Bahan pokok'
										");
	}
	
	public function record_count2($id_kabupaten) {
			return $this->db->count_all("industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
											WHERE a.id_komoditi=b.id_komoditi 
											AND a.id_pasar=c.id_pasar
											AND b.jenis_komoditi='Bahan pokok' 
											AND c.id_kabupaten='".$id_kabupaten."'");
	}
	
	public function record_count3($key) {
			return $this->db->count_all("industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
										 WHERE	a.id_komoditi=b.id_komoditi
										AND a.id_pasar=c.id_pasar
										AND b.jenis_komoditi='Bahan pokok'
										AND  (	b.nama_komoditi LIKE '%".$key."%' ESCAPE '!' 
												OR c.nama_pasar LIKE '%".$key."%' ESCAPE '!'
												OR a.tanggal LIKE '%".$key."%' ESCAPE '!'
												OR a.harga LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function record_count4($id_kabupaten, $key) {
			return $this->db->count_all("industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
										 WHERE	a.id_komoditi=b.id_komoditi
										AND a.id_pasar=c.id_pasar
										AND b.jenis_komoditi='Bahan pokok'
										AND c.id_kabupaten='".$id_kabupaten."'
										AND  (	b.nama_komoditi LIKE '%".$key."%' ESCAPE '!' 
												OR c.nama_pasar LIKE '%".$key."%' ESCAPE '!'
												OR a.tanggal LIKE '%".$key."%' ESCAPE '!'
												OR a.harga LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function fetch_pokok($limit, $start) {
		$this->db->select('a.*, b.*,c.*');
		$this->db->from('industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c');
		$this->db->where('a.id_komoditi=b.id_komoditi');
		$this->db->where('a.id_pasar=c.id_pasar');
		$this->db->where("b.jenis_komoditi='Bahan pokok'");
		$this->db->order_by("a.tanggal DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetch_pokok2($limit, $start, $id_kabupaten) {
		$this->db->select('a.*, b.*,c.*');
		$this->db->from('industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c');
		$this->db->where('a.id_komoditi=b.id_komoditi');
		$this->db->where('a.id_pasar=c.id_pasar');
		$this->db->where("b.jenis_komoditi='Bahan pokok'");
		$this->db->where("c.id_kabupaten='$id_kabupaten'");
		$this->db->order_by("a.tanggal DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_pokok($limit, $start, $key) {
		$this->db->select('a.*, b.*,c.*');
		$this->db->from('industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c');
		$this->db->where("a.id_komoditi=b.id_komoditi
							AND a.id_pasar=c.id_pasar
							AND b.jenis_komoditi='Bahan pokok'
							AND  (	b.nama_komoditi LIKE '%".$key."%' ESCAPE '!' 
									OR c.nama_pasar LIKE '%".$key."%' ESCAPE '!'
									OR a.tanggal LIKE '%".$key."%' ESCAPE '!'
									OR a.harga LIKE '%".$key."%' ESCAPE '!')
						");
		$this->db->order_by("a.tanggal DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
	public function search_pokok2($limit, $start, $key, $id_kabupaten) {
	
		$this->db->select('a.*, b.*,c.*');
		$this->db->from('industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c');
		$this->db->where("a.id_komoditi=b.id_komoditi
							AND a.id_pasar=c.id_pasar
							AND b.jenis_komoditi='Bahan pokok'
							AND c.id_kabupaten='".$id_kabupaten."'
							AND  (	b.nama_komoditi LIKE '%".$key."%' ESCAPE '!' 
									OR c.nama_pasar LIKE '%".$key."%' ESCAPE '!'
									OR a.tanggal LIKE '%".$key."%' ESCAPE '!'
									OR a.harga LIKE '%".$key."%' ESCAPE '!')
						");
		$this->db->order_by("a.tanggal DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>