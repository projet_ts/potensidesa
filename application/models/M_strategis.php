<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindutrian

class M_strategis extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data() {
        $query = $this->db->query("SELECT * FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c WHERE a.id_komoditi=b.id_komoditi AND a.id_pasar=c.id_pasar AND b.jenis_komoditi='Bahan Strategis' ORDER BY tanggal ASC");
        return $query->result();
    }
	
	 public function data2($id_pasar, $tanggal) {
        $query = $this->db->query("SELECT * FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
									WHERE a.id_komoditi=b.id_komoditi 
									AND a.id_pasar=c.id_pasar 
									AND a.id_pasar='".$id_pasar."' 
									AND b.jenis_komoditi='Bahan Strategis' 
									AND a.tanggal='".$tanggal."' 
									ORDER BY tanggal ASC");
        return $query->result();
    }
	
	 public function data3($id_pasar, $tanggal) {
        $query = $this->db->query("SELECT * FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
									WHERE a.id_komoditi=b.id_komoditi 
									AND a.id_pasar=c.id_pasar 
									AND a.id_pasar='".$id_pasar."' 
									AND b.jenis_komoditi='Bahan Strategis' 
									AND a.tanggal=DATE_SUB('".$tanggal."', INTERVAL 1 DAY) 
									ORDER BY tanggal ASC");
        return $query->result();
    }
	
	 public function data4($nama_komoditi, $id_pasar, $tanggal) {
        $query = $this->db->query("SELECT * FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
									WHERE a.id_komoditi=b.id_komoditi 
									AND a.id_pasar=c.id_pasar 
									AND a.id_pasar='".$id_pasar."' 
									AND b.nama_komoditi='".$nama_komoditi."' 
									AND b.jenis_komoditi='Bahan Strategis' 
									AND a.tanggal='".$tanggal."' 
									ORDER BY tanggal ASC");
        return $query->result();
    }
	 
	public function data5() {
       /* $query = $this->db->query("SELECT * FROM
									(SELECT c.id_pasar as id_pasar, nama_komoditi as nama_komoditi2,harga as harga_kemarin FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
																		WHERE a.id_komoditi=b.id_komoditi 
																		AND a.id_pasar=c.id_pasar 
																		AND b.jenis_komoditi='Bahan Strategis'
																		AND a.tanggal=DATE_SUB('2016-03-31', INTERVAL 1 DAY) 
																		ORDER BY tanggal) t1 
									INNER JOIN
									(SELECT c.id_pasar as id_pasar2, nama_komoditi as nama_komoditi,harga as harga_hari_ini FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
																		WHERE a.id_komoditi=b.id_komoditi 
																		AND a.id_pasar=c.id_pasar 
																		AND b.jenis_komoditi='Bahan Strategis'
																		AND a.tanggal='2016-03-31' 
																		ORDER BY tanggal) t2
									ON t1.nama_komoditi2 = t2.nama_komoditi");
		$query = $this->db->query("SELECT * FROM
									(SELECT c.id_pasar as id_pasar, nama_komoditi as nama_komoditi2,harga as harga_kemarin FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
																		WHERE a.id_komoditi=b.id_komoditi 
																		AND a.id_pasar=c.id_pasar 
																		AND b.jenis_komoditi='Bahan Strategis'
																		AND a.tanggal= DATE_SUB(CURDATE(), INTERVAL 1 DAY)
																		ORDER BY tanggal) t1 
									INNER JOIN
									(SELECT c.id_pasar as id_pasar2, nama_komoditi as nama_komoditi,harga as harga_hari_ini FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
																		WHERE a.id_komoditi=b.id_komoditi 
																		AND a.id_pasar=c.id_pasar 
																		AND b.jenis_komoditi='Bahan Strategis'
																		AND a.tanggal = CURDATE()
																		ORDER BY tanggal) t2
									ON t1.nama_komoditi2 = t2.nama_komoditi");*/
		/*$query = $this->db->query("SELECT * FROM
									(SELECT c.id_kabupaten as id_kabupaten, c.id_pasar as id_pasar, nama_komoditi as nama_komoditi2,
									round(AVG(IF(a.tanggal=DATE_SUB('2016-04-01', INTERVAL 1 DAY) ,harga,0)), 0) as  harga_kemarin FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
																		WHERE a.id_komoditi=b.id_komoditi 
																		AND a.id_pasar=c.id_pasar 
																		AND b.jenis_komoditi='Bahan Pokok'
																		GROUP BY nama_komoditi
																		ORDER BY b.id_komoditi ASC) t1 
									INNER JOIN
									(SELECT c.id_kabupaten as id_kabupaten2, c.id_pasar as id_pasar2, nama_komoditi as nama_komoditi,
									round(AVG(IF(a.tanggal='2016-04-01',harga,0)), 0)  as harga_hari_ini FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
																		WHERE a.id_komoditi=b.id_komoditi 
																		AND a.id_pasar=c.id_pasar 
																		AND b.jenis_komoditi='Bahan Pokok'
																		GROUP BY nama_komoditi
																		ORDER BY b.id_komoditi ASC) t2
									ON t1.nama_komoditi2 = t2.nama_komoditi");*/
        $query = $this->db->query("SELECT * FROM
									(SELECT c.id_kabupaten as id_kabupaten, b.satuan as satuan, c.id_pasar as id_pasar, nama_komoditi as nama_komoditi,round(avg(harga) , 0) as  harga_kemarin FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
																		WHERE a.id_komoditi=b.id_komoditi 
																		AND a.id_pasar=c.id_pasar 
																		AND b.jenis_komoditi='Bahan Strategis'
																		AND a.tanggal=DATE_SUB(CURDATE(), INTERVAL 1 DAY)
																		GROUP BY id_kabupaten, nama_komoditi 
																		ORDER BY tanggal
								) t1 
									INNER JOIN
									(SELECT  c.id_kabupaten as id_kabupaten2, c.id_pasar as id_pasar2, nama_komoditi as nama_komoditi2,round(avg(harga) , 0) as  harga_hari_ini FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
																		WHERE a.id_komoditi=b.id_komoditi 
																		AND a.id_pasar=c.id_pasar 
																		AND b.jenis_komoditi='Bahan Strategis'
																		AND a.tanggal=CURDATE()
																		GROUP BY id_kabupaten2, nama_komoditi 
																		ORDER BY tanggal
								) t2
									ON t1.nama_komoditi = t2.nama_komoditi2 and t1.id_kabupaten = t2.id_kabupaten2");
        return $query->result();
    }
	
	 
	public function data10() {
      
        $query = $this->db->query("SELECT * FROM
									(SELECT c.id_kabupaten as id_kabupaten, b.satuan as satuan, c.id_pasar as id_pasar, nama_komoditi as nama_komoditi,round(avg(harga) , 0) as  harga_kemarin FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
																		WHERE a.id_komoditi=b.id_komoditi 
																		AND a.id_pasar=c.id_pasar 
																		AND b.jenis_komoditi='Bahan Pokok'
																		AND a.tanggal=DATE_SUB('2016-05-08', INTERVAL 1 DAY)
																		GROUP BY id_kabupaten, nama_komoditi 
																		ORDER BY tanggal
								) t1 
									INNER JOIN
									(SELECT  c.id_kabupaten as id_kabupaten2, c.id_pasar as id_pasar2, nama_komoditi as nama_komoditi2,round(avg(harga) , 0) as  harga_hari_ini FROM industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
																		WHERE a.id_komoditi=b.id_komoditi 
																		AND a.id_pasar=c.id_pasar 
																		AND b.jenis_komoditi='Bahan Pokok'
																		AND a.tanggal='2016-05-08'
																		GROUP BY id_kabupaten2, nama_komoditi 
																		ORDER BY tanggal
								) t2
									ON t1.nama_komoditi = t2.nama_komoditi2 and t1.id_kabupaten = t2.id_kabupaten2");
        return $query->result();
    }
	
	public function getStrategis() {
        $query = $this->db->query("SELECT * FROM industri_table_komoditi WHERE jenis_komoditi='Bahan Strategis'");
        return $query->result();
    }
	
	public function strategis(){
        $query = $this->db->get('industri_table_harga_barang');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		//$this->id_harga_barang = $data['id_harga_barang'];
		$this->id_komoditi = $data['id_komoditi'];
		$this->id_pasar = $data['id_pasar'];
        $this->tanggal =  $data['tanggal'];
		$this->harga = $data['harga'];
		
        //insert data
        $this->db->insert('industri_table_harga_barang', $this);
    }
	
    
    public function update($data) {
        //get data
        $this->id_harga_barang = $data['id_harga_barang'];
		$this->harga = $data['harga'];
        
        //update data
        $this->db->update('industri_table_harga_barang', $this, array('id_harga_barang'=>$data['id_harga_barang']));
    }
    
    public function delete($id) {
        $this->db->delete('industri_table_harga_barang', array('id_harga_barang' => $id));
    }

     public function get($id){
        $this->db->where('id_harga_barang', $id);
        $this->db->where('a.id_pasar=b.id_pasar');
        $this->db->where('a.id_komoditi=c.id_komoditi');
        $query = $this->db->get('industri_table_harga_barang a, industri_table_pasar b, industri_table_komoditi c');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
											WHERE a.id_komoditi=b.id_komoditi 
											AND a.id_pasar=c.id_pasar
											AND b.jenis_komoditi='Bahan Strategis'
										");
	}
	
	public function record_count2($id_kabupaten) {
			return $this->db->count_all("industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
											WHERE a.id_komoditi=b.id_komoditi 
											AND a.id_pasar=c.id_pasar
											AND b.jenis_komoditi='Bahan Strategis' 
											AND c.id_kabupaten='".$id_kabupaten."'");
	}
	
	public function record_count3($key) {
			return $this->db->count_all("industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
										 WHERE	a.id_komoditi=b.id_komoditi
										AND a.id_pasar=c.id_pasar
										AND b.jenis_komoditi='Bahan Strategis'
										AND  (	b.nama_komoditi LIKE '%".$key."%' ESCAPE '!' 
												OR c.nama_pasar LIKE '%".$key."%' ESCAPE '!'
												OR a.tanggal LIKE '%".$key."%' ESCAPE '!'
												OR a.harga LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function record_count4($id_kabupaten, $key) {
			return $this->db->count_all("industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c 
										 WHERE	a.id_komoditi=b.id_komoditi
										AND a.id_pasar=c.id_pasar
										AND b.jenis_komoditi='Bahan Strategis'
										AND c.id_kabupaten='".$id_kabupaten."'
										AND  (	b.nama_komoditi LIKE '%".$key."%' ESCAPE '!' 
												OR c.nama_pasar LIKE '%".$key."%' ESCAPE '!'
												OR a.tanggal LIKE '%".$key."%' ESCAPE '!'
												OR a.harga LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function cekHargaBarang($id_pasar, $tanggal) {
			return $this->db->count_all("industri_table_harga_barang a, industri_table_komoditi b 
										WHERE a.id_komoditi=b.id_komoditi
										AND a.id_pasar='$id_pasar' 
										AND a.tanggal = '$tanggal' 
										AND b.jenis_komoditi='Bahan Strategis'");
	}
	
	public function fetch_strategis($limit, $start) {
		$this->db->select('a.*, b.*,c.*');
		$this->db->from('industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c');
		$this->db->where('a.id_komoditi=b.id_komoditi');
		$this->db->where('a.id_pasar=c.id_pasar');
		$this->db->where("b.jenis_komoditi='Bahan Strategis'");
		$this->db->order_by("a.tanggal DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetch_strategis2($limit, $start, $id_kabupaten) {
		$this->db->select('a.*, b.*,c.*');
		$this->db->from('industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c');
		$this->db->where('a.id_komoditi=b.id_komoditi');
		$this->db->where('a.id_pasar=c.id_pasar');
		$this->db->where("b.jenis_komoditi='Bahan Strategis'");
		$this->db->where("c.id_kabupaten='$id_kabupaten'");
		$this->db->order_by("a.tanggal DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_strategis($id_kabupaten, $id_pasar, $tanggal) {
		$this->db->select('a.*, b.*,c.*');
		$this->db->from('industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c');
		$this->db->where("a.id_komoditi=b.id_komoditi
							AND a.id_pasar=c.id_pasar
							AND b.jenis_komoditi='Bahan Strategis'
							AND  c.id_kabupaten = '$id_kabupaten' 
							AND  a.id_pasar = '$id_pasar' 
							AND  a.tanggal = '$tanggal' 
									
						");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
	public function search_strategis2($limit, $start, $key, $id_kabupaten) {
	
		$this->db->select('a.*, b.*,c.*');
		$this->db->from('industri_table_harga_barang a, industri_table_komoditi b, industri_table_pasar c');
		$this->db->where("a.id_komoditi=b.id_komoditi
							AND a.id_pasar=c.id_pasar
							AND b.jenis_komoditi='Bahan Strategis'
							AND c.id_kabupaten='".$id_kabupaten."'
							AND  (	b.nama_komoditi LIKE '%".$key."%' ESCAPE '!' 
									OR c.nama_pasar LIKE '%".$key."%' ESCAPE '!'
									OR a.tanggal LIKE '%".$key."%' ESCAPE '!'
									OR a.harga LIKE '%".$key."%' ESCAPE '!')
						");
		$this->db->order_by("a.tanggal DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
}
?>