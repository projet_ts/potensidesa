<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function login($data) {
        $this->db->where('username', $data['username']);
        $this->db->where('password', $data['password']);
        $this->db->where('active', "Y");
        return $this->db->get('table_user')->row();
    }
	
	function login2($username, $password) {
        //create query to connect user login database
        $this->db->select('*');
        $this->db->from('table_user');
        $this->db->where('username', $username);
        $this->db->where('password', MD5($password));
      //  $this->db->where('password', $password);
		$this->db->limit(1);

        //get query and processing
        $query = $this->db->get();
        if($query->num_rows() == 1) {
            return $query->result(); //if data is true
        } else {
            return false; //if data is wrong
        }
    }
    
    function __destruct() {
        $this->db->close();
    }
	
	public function access($group_id, $menu) {
		$query  = $this->db->query("SELECT * FROM table_groupaccess s, table_menu m 
			WHERE s.menu_id=m.menu_id AND s.group_id='$group_id' AND menu_name='$menu'");
        return $query->result();
	}
}
?>    