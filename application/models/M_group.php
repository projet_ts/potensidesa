<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_group extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data() {
        $query  = $this->db->query("SELECT group_id, group_name, description, active 
                                        FROM table_group");
        return $query->result();
    }
	
	public function data2() {
        $query  = $this->db->query("SELECT group_id, group_name, description, active 
                                        FROM table_group WHERE group_id!='group1000'");
        return $query->result();
    }
	
	public function data_active() {
        $query  = $this->db->query("SELECT group_id, group_name, description, active 
                                        FROM table_group WHERE active='Y'");
        return $query->result();
    }
	
    public function data_active2() {
        $query  = $this->db->query("SELECT group_id, group_name, description, active 
                                        FROM table_group WHERE active='Y' AND group_id!='group1000'");
        return $query->result();
    }
    
    public function create($data) {
        //get data
        $this->group_id = $data['group_id'];
        $this->group_name = $data['group_name'];
        $this->description = $data['description'];
        $this->active = $data['active'];
        
        //insert data
        $this->db->insert('table_group', $this);
    }
    
    public function update($data) {
        //get data
        $this->group_name = $data['group_name'];
        $this->description = $data['description'];
        $this->active = $data['active'];
        
        //update data
        $this->db->update('table_group', $this, array('group_id'=>$data['group_id']));
    }
    
    public function delete($id) {
        $this->db->delete('table_group', array('group_id' => $id));
    }


    public function get($id){
        $this->db->where('group_id', $id);
        $query = $this->db->get('table_group', 1);
        return $query->result();
    }
	
	public function record_count() {
		return $this->db->count_all("table_group");
	}
	
/*	public function record_count_search($key) {
		$this->db->like("group_name", $key);
		$this->db->or_like("description", $key);
		$this->db->or_like("active", $key);
		return $this->db->count_all_results("table_group");
	}*/
	
    public function record_count_search($key) {
		$this->db->like("group_name", $key);
		$this->db->or_like("description", $key);
		$this->db->or_like("active", $key);
		return $this->db->count_all_results("table_group");
	}
    
	public function fetch_group($limit, $start) {
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_group");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_group($limit, $start, $key) {
		
		$this->db->like("group_name", $key);
		$this->db->or_like("description", $key);
		$this->db->or_like("active", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_group");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
    
    
}
?>