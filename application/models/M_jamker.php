<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_jamker extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_jamker");
        return $query->result();
    }		
	
	public function jamker(){
        $query = $this->db->get('table_jamker');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		$this->id_jamker = $data['id_jamker'];
        $this->senin = $data['senin'];
		$this->selasa = $data['selasa'];
		$this->rabu = $data['rabu'];
		$this->kamis = $data['kamis'];
		$this->jumat = $data['jumat'];
		$this->sabtu = $data['sabtu'];
		$this->minggu = $data['minggu'];
        
        //insert data
        $this->db->insert('table_jamker', $this);
    }
    
    public function update($data) {
        //get data
        $this->id_jamker = $data['id_jamker'];
        $this->senin = $data['senin'];
		$this->selasa = $data['selasa'];
		$this->rabu = $data['rabu'];
		$this->kamis = $data['kamis'];
		$this->jumat = $data['jumat'];
		$this->sabtu = $data['sabtu'];
		$this->minggu = $data['minggu'];
		
        //update data
        $this->db->update('table_jamker', $this, array('id_jamker'=>$data['id_jamker']));
    }
    
    public function delete($id) {
        $this->db->delete('table_jamker', array('id_jamker' => $id));
    }


    public function get($id){
        $this->db->where('id_jamker', $id);
        $query = $this->db->get('table_jamker');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_jamker");
	}
	
	public function record_count_search($key) {
		$this->db->like("senin", $key);
		$this->db->or_like("selasa", $key);
		$this->db->or_like("rabu", $key);
		$this->db->or_like("kamis", $key);
		$this->db->or_like("jumat", $key);
		$this->db->or_like("sabtu", $key);
		$this->db->or_like("minggu", $key);
		return $this->db->count_all_results("table_jamker");
	}
	
	public function fetch_jamker($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_jamker');
		$this->db->order_by('id_jamker DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_jamker($limit, $start, $key) {
	
		$this->db->like("senin", $key);
		$this->db->or_like("selasa", $key);
		$this->db->or_like("rabu", $key);
		$this->db->or_like("kamis", $key);
		$this->db->or_like("jumat", $key);
		$this->db->or_like("sabtu", $key);
		$this->db->or_like("minggu", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_jamker");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>