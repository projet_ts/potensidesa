<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_video extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_video");
        return $query->result();
    }		
	
	public function video(){
        $query = $this->db->get('table_video');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->judul = $data['judul'];		
		$this->deskripsi = $data['deskripsi'];	
		$this->gambar = $data['gambar'];
		$this->link = $data['link'];
        
        //insert data
        $this->db->insert('table_video', $this);
    }

    //upload image
	public function createnoimage($data) {
        //get data
		
		
		$this->judul = $data['judul'];
		$this->deskripsi = $data['deskripsi'];
		$this->link = $data['link'];
		
		
        //insert data
        $this->db->insert('table_video', $this);
    }
    
    public function update($data) {
        //get data        
        
		$this->judul = $data['judul'];
		$this->deskripsi = $data['deskripsi'];
		$this->gambar = $data['gambar'];	
		$this->link = $data['link'];	
		
        //update data
        $this->db->update('table_video', $this, array('id_video'=>$data['id_video']));
    }
    
    public function delete($id) {
        $this->db->delete('table_video', array('id_video' => $id));
    }


    public function get($id){
        $this->db->where('id_video', $id);
        $query = $this->db->get('table_video');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_video");
	}
	
	public function record_count_search($key) {		
		$this->db->or_like("judul", $key);
		$this->db->or_like("deskripsi", $key);		
		$this->db->like("gambar", $key);
		$this->db->like("link", $key);
		return $this->db->count_all_results("table_video");
	}
	
	public function fetch_video($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_video');
		$this->db->order_by('id_video DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_video($limit, $start, $key) {	
		
		$this->db->or_like("judul", $key);
		$this->db->or_like("deskripsi", $key);		
		$this->db->like("gambar", $key);
		$this->db->like("link", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_video");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}

	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_video',$id);
		$query = $getData = $this->db->get('table_video');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->id_video = $data['id_video'];			
		$this->judul = $data['judul'];
		$this->deskripsi = $data['deskripsi'];
		$this->link = $data['link'];
        
        //update data
        $this->db->update('table_video', $this, array('id_video'=>$data['id_video']));
    }
	
}
?>