<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model table_kecamatan

class M_kecamatan extends CI_Model{

    function __construct() {
        parent::__construct();
    }

	public function data() {
        $query  = $this->db->query("SELECT * FROM table_kecamatan");
        return $query->result();
    }

	public function kecamatan(){
        $query = $this->db->get('table_kecamatan');
        return $query->result();
    }

    public function create($data) {
        //get data

        $this->nama_kecamatan = $data['nama_kecamatan'];
		    $this->keterangan = $data['keterangan'];

        //insert data
        $this->db->insert('table_kecamatan', $this);
    }

    public function update($data) {

        //get data
        $this->nama_kecamatan = $data['nama_kecamatan'];
        $this->keterangan = $data['keterangan'];

        //update data
        $this->db->update('table_kecamatan', $this, array('kec_id'=>$data['kec_id']));
    }

    public function delete($id) {
        $this->db->delete('table_kecamatan', array('kec_id' => $id));
    }

    public function get($id){
        $this->db->where('kec_id', $id);
        $query = $this->db->get('table_kecamatan');
        return $query->result();
    }

	public function record_count() {
			return $this->db->count_all("table_kecamatan");
	}

	public function record_count_search($key) {
		$this->db->like("nama_kecamatan", $key);
		return $this->db->count_all_results("table_kecamatan");
	}

	public function fetch_kecamatan($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_kecamatan');
		$this->db->order_by('kec_id DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function search_kecamatan($limit, $start, $key) {

		$this->db->like("nama_kecamatan", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_kecamatan");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
	}

	//upload image
	public function link_gambar($id)
	{

		$this->db->where('kec_id',$id);
		$query = $getData = $this->db->get('table_kecamatan');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;

	}

}
?>
